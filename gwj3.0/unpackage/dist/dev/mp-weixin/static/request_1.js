//发起网络请求
const header = {
	'custom-type':'application/x-www-form-urlencoded',
	'openid':'',
	'sessionKey':''
}
const request = (url,data) => {
	if(url != '/mgr/question/mhm_get_question'){
		uni.showLoading({
			mask:true,
		    title: '加载中'
		});
	}
	var value = wx.getStorageSync('openid');
	var value2 = wx.getStorageSync('sessionKey');
	if (value) {
		header.openid = value
		header.sessionKey = value2
	}

	let promise = new Promise((resolve, reject)=>{
		uni.request({
			url:'https://gwj.maohemao.com' + url,
			method:'POST',
			data:data,
			header:header,
			success: (res) => {
				if (res.statusCode == 200) {
					resolve(res);
				}else {
					reject(res.data);
				}
			},
			fail: (res) => {
				uni.showToast({
					title: '网络异常',
					icon: 'none',
					duration: 5000
				})
				reject(res.data);
			},
			complete: (res) => {
				if(url != '/mgr/question/mhm_get_question'){
					uni.hideLoading();
				}
			}
		})
	})
	return promise
}


//小程序登录
function login(){
	uni.login({
		provider: 'weixin',
		success: (res) => {
			request('/mgr/login/mhm_get_code',{code:res.code}).then(e => {
				wx.setStorageSync('openid', e.data.data.openid);
				wx.setStorageSync('sessionKey', e.data.data.sessionkey);
			})
		},
		fail: (res) => {
			console.log(res);
		}
	})
}
module.exports = {
	request:request,
	login:login
}