(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],{

/***/ 1:
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.createApp = createApp;exports.createComponent = createComponent;exports.createPage = createPage;exports.default = void 0;var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 2));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}

var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isFn(fn) {
  return typeof fn === 'function';
}

function isStr(str) {
  return typeof str === 'string';
}

function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}

function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

function noop() {}

/**
                    * Create a cached version of a pure function.
                    */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
   * Camelize a hyphen-delimited string.
   */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {return c ? c.toUpperCase() : '';});
});

var HOOKS = [
'invoke',
'success',
'fail',
'complete',
'returnValue'];


var globalInterceptors = {};
var scopedInterceptors = {};

function mergeHook(parentVal, childVal) {
  var res = childVal ?
  parentVal ?
  parentVal.concat(childVal) :
  Array.isArray(childVal) ?
  childVal : [childVal] :
  parentVal;
  return res ?
  dedupeHooks(res) :
  res;
}

function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}

function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}

function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}

function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}

function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}

function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}

function wrapperHook(hook) {
  return function (data) {
    return hook(data) || data;
  };
}

function isPromise(obj) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

function queue(hooks, data) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook));
    } else {
      var res = hook(data);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {} };

      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    } };

}

function wrapperOptions(interceptor) {var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}

function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}

function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}

function invokeApi(method, api, options) {for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {params[_key - 3] = arguments[_key];}
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}

var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return res.then(function (res) {
      return res[1];
    }).catch(function (res) {
      return res[0];
    });
  } };


var SYNC_API_RE =
/^\$|sendNativeEvent|restoreGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64/;

var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection'];

var CALLBACK_API_RE = /^on|^off/;

function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}

function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}

function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).
  catch(function (err) {return [err];});
}

function shouldPromise(name) {
  if (
  isContextApi(name) ||
  isSyncApi(name) ||
  isCallbackApi(name))
  {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(
    function (value) {return promise.resolve(callback()).then(function () {return value;});},
    function (reason) {return promise.resolve(callback()).then(function () {
        throw reason;
      });});

  };
}

function promisify(name, api) {
  if (!shouldPromise(name)) {
    return api;
  }
  return function promiseApi() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {params[_key2 - 1] = arguments[_key2];}
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject })].concat(
      params));
    })));
  };
}

var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;

function checkDeviceWidth() {var _wx$getSystemInfoSync =




  wx.getSystemInfoSync(),platform = _wx$getSystemInfoSync.platform,pixelRatio = _wx$getSystemInfoSync.pixelRatio,windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}

function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }

  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}

var interceptors = {
  promiseInterceptor: promiseInterceptor };


var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors });var


EventChannel = /*#__PURE__*/function () {
  function EventChannel(id, events) {var _this = this;_classCallCheck(this, EventChannel);
    this.id = id;
    this.listener = {};
    this.emitCache = {};
    if (events) {
      Object.keys(events).forEach(function (name) {
        _this.on(name, events[name]);
      });
    }
  }_createClass(EventChannel, [{ key: "emit", value: function emit(

    eventName) {for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {args[_key3 - 1] = arguments[_key3];}
      var fns = this.listener[eventName];
      if (!fns) {
        return (this.emitCache[eventName] || (this.emitCache[eventName] = [])).push(args);
      }
      fns.forEach(function (opt) {
        opt.fn.apply(opt.fn, args);
      });
      this.listener[eventName] = fns.filter(function (opt) {return opt.type !== 'once';});
    } }, { key: "on", value: function on(

    eventName, fn) {
      this._addListener(eventName, 'on', fn);
      this._clearCache(eventName);
    } }, { key: "once", value: function once(

    eventName, fn) {
      this._addListener(eventName, 'once', fn);
      this._clearCache(eventName);
    } }, { key: "off", value: function off(

    eventName, fn) {
      var fns = this.listener[eventName];
      if (!fns) {
        return;
      }
      if (fn) {
        for (var i = 0; i < fns.length;) {
          if (fns[i].fn === fn) {
            fns.splice(i, 1);
            i--;
          }
          i++;
        }
      } else {
        delete this.listener[eventName];
      }
    } }, { key: "_clearCache", value: function _clearCache(

    eventName) {
      var cacheArgs = this.emitCache[eventName];
      if (cacheArgs) {
        for (; cacheArgs.length > 0;) {
          this.emit.apply(this, [eventName].concat(cacheArgs.shift()));
        }
      }
    } }, { key: "_addListener", value: function _addListener(

    eventName, type, fn) {
      (this.listener[eventName] || (this.listener[eventName] = [])).push({
        fn: fn,
        type: type });

    } }]);return EventChannel;}();


var eventChannels = {};

var eventChannelStack = [];

var id = 0;

function initEventChannel(events) {var cache = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  id++;
  var eventChannel = new EventChannel(id, events);
  if (cache) {
    eventChannels[id] = eventChannel;
    eventChannelStack.push(eventChannel);
  }
  return eventChannel;
}

function getEventChannel(id) {
  if (id) {
    var eventChannel = eventChannels[id];
    delete eventChannels[id];
    return eventChannel;
  }
  return eventChannelStack.shift();
}

var navigateTo = {
  args: function args(fromArgs, toArgs) {
    var id = initEventChannel(fromArgs.events).id;
    if (fromArgs.url) {
      fromArgs.url = fromArgs.url + (fromArgs.url.indexOf('?') === -1 ? '?' : '&') + '__id__=' + id;
    }
  },
  returnValue: function returnValue(fromRes, toRes) {
    fromRes.eventChannel = getEventChannel();
  } };


function findExistsPageIndex(url) {
  var pages = getCurrentPages();
  var len = pages.length;
  while (len--) {
    var page = pages[len];
    if (page.$page && page.$page.fullPath === url) {
      return len;
    }
  }
  return -1;
}

var redirectTo = {
  name: function name(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.delta) {
      return 'navigateBack';
    }
    return 'redirectTo';
  },
  args: function args(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.url) {
      var existsPageIndex = findExistsPageIndex(fromArgs.url);
      if (existsPageIndex !== -1) {
        var delta = getCurrentPages().length - 1 - existsPageIndex;
        if (delta > 0) {
          fromArgs.delta = delta;
        }
      }
    }
  } };


var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(
      function (item, index) {return index < currentIndex ? item !== urls[currentIndex] : true;});

    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false };

  } };


function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.windowHeight - safeArea.bottom };

  }
}
var protocols = {
  redirectTo: redirectTo,
  navigateTo: navigateTo,
  previewImage: previewImage,
  getSystemInfo: {
    returnValue: addSafeAreaInsets },

  getSystemInfoSync: {
    returnValue: addSafeAreaInsets } };


var todos = [
'vibrate',
'preloadPage',
'unPreloadPage',
'loadSubPackage'];

var canIUses = [];

var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];

function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}

function processArgs(methodName, fromArgs) {var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {// 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {// 不支持的参数
          console.warn("\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F ".concat(methodName, "\u6682\u4E0D\u652F\u6301").concat(key));
        } else if (isStr(keyOption)) {// 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {// {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        if (isFn(fromArgs[key])) {
          toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
        }
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}

function processReturnValue(methodName, res, returnValue) {var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {// 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}

function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {// 暂不支持的 api
      return function () {
        console.error("\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F \u6682\u4E0D\u652F\u6301".concat(methodName));
      };
    }
    return function (arg1, arg2) {// 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }

      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);

      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      if (isFn(options.name)) {
        methodName = options.name(arg1);
      } else if (isStr(options.name)) {
        methodName = options.name;
      }
      var returnValue = wx[methodName].apply(wx, args);
      if (isSyncApi(methodName)) {// 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}

var todoApis = Object.create(null);

var TODOS = [
'onTabBarMidButtonTap',
'subscribePush',
'unsubscribePush',
'onPush',
'offPush',
'share'];


function createTodoApi(name) {
  return function todoApi(_ref)


  {var fail = _ref.fail,complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail:\u6682\u4E0D\u652F\u6301 ").concat(name, " \u65B9\u6CD5") };

    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}

TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});

var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin'] };


function getProvider(_ref2)




{var service = _ref2.service,success = _ref2.success,fail = _ref2.fail,complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service] };

    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail:服务[' + service + ']不存在' };

    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}

var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider });


var getEmitter = function () {
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();

function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}

function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}

var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit });


var api = /*#__PURE__*/Object.freeze({
  __proto__: null });


var MPPage = Page;
var MPComponent = Component;

var customizeRE = /:/g;

var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});

function initTriggerEvent(mpInstance) {
  {
    if (!wx.canIUse('nextTick')) {
      return;
    }
  }
  var oldTriggerEvent = mpInstance.triggerEvent;
  mpInstance.triggerEvent = function (event) {for (var _len4 = arguments.length, args = new Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {args[_key4 - 1] = arguments[_key4];}
    return oldTriggerEvent.apply(mpInstance, [customize(event)].concat(args));
  };
}

function initHook(name, options) {
  var oldHook = options[name];
  if (!oldHook) {
    options[name] = function () {
      initTriggerEvent(this);
    };
  } else {
    options[name] = function () {
      initTriggerEvent(this);for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {args[_key5] = arguments[_key5];}
      return oldHook.apply(this, args);
    };
  }
}

Page = function Page() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  initHook('onLoad', options);
  return MPPage(options);
};

Component = function Component() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  initHook('created', options);
  return MPComponent(options);
};

var PAGE_EVENT_HOOKS = [
'onPullDownRefresh',
'onReachBottom',
'onAddToFavorites',
'onShareTimeline',
'onShareAppMessage',
'onPageScroll',
'onResize',
'onTabItemTap'];


function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}

function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }

  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }

  vueOptions = vueOptions.default || vueOptions;

  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super &&
    vueOptions.super.options &&
    Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }

  if (isFn(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {return hasHook(hook, mixin);});
  }
}

function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}

function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}

function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}

function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;

  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}

function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};

  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }

  if (!isPlainObject(data)) {
    data = {};
  }

  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });

  return data;
}

var PROP_TYPES = [String, Number, Boolean, Object, Array, null];

function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;

  var vueProps = vueOptions.props;

  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }

  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: '' };

          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: '' };

        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(
    initBehavior({
      properties: initProperties(vueExtends.props, true) }));


  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(
        initBehavior({
          properties: initProperties(vueMixin.props, true) }));


      }
    });
  }
  return behaviors;
}

function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}

function initProperties(props) {var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: '' };

    // 用于字节跳动小程序模拟抽象节点
    properties.generic = {
      type: Object,
      value: null };

    properties.vueSlots = { // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots });

      } };

  }
  if (Array.isArray(props)) {// ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key) };

    });
  } else if (isPlainObject(props)) {// {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {// title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }

        opts.type = parsePropType(key, opts.type);

        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key) };

      } else {// content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key) };

      }
    });
  }
  return properties;
}

function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}

  event.stopPropagation = noop;
  event.preventDefault = noop;

  event.target = event.target || {};

  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }

  if (hasOwn(event, 'markerId')) {
    event.detail = typeof event.detail === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }

  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }

  return event;
}

function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {// ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];

      var vFor;
      if (Number.isInteger(dataPath)) {
        vFor = dataPath;
      } else if (!dataPath) {
        vFor = context;
      } else if (typeof dataPath === 'string' && dataPath) {
        if (dataPath.indexOf('#s#') === 0) {
          vFor = dataPath.substr(3);
        } else {
          vFor = vm.__get_value(dataPath, context);
        }
      }

      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }

      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}

function processEventExtra(vm, extra, event) {
  var extraObj = {};

  if (Array.isArray(extra) && extra.length) {
    /**
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *'test'
                                              */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {// model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {// $event
            extraObj['$' + index] = event;
          } else if (dataPath === 'arguments') {
            if (event.detail && event.detail.__args__) {
              extraObj['$' + index] = event.detail.__args__;
            } else {
              extraObj['$' + index] = [event];
            }
          } else if (dataPath.indexOf('$event.') === 0) {// $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }

  return extraObj;
}

function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}

function processEventArgs(vm, event) {var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];var isCustom = arguments.length > 4 ? arguments[4] : undefined;var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象
  if (isCustom) {// 自定义事件
    isCustomMPEvent = event.currentTarget &&
    event.currentTarget.dataset &&
    event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {// 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return event.detail.__args__ || event.detail;
    }
  }

  var extraObj = processEventExtra(vm, extra, event);

  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {// input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(event.detail.__args__[0]);
        } else {// wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });

  return ret;
}

var ONCE = '~';
var CUSTOM = '^';

function isMatchEventType(eventType, optType) {
  return eventType === optType ||

  optType === 'regionchange' && (

  eventType === 'begin' ||
  eventType === 'end');


}

function getContextVm(vm) {
  var $parent = vm.$parent;
  // 父组件是 scoped slots 或者其他自定义组件时继续查找
  while ($parent && $parent.$parent && ($parent.$options.generic || $parent.$parent.$options.generic || $parent.$scope._$vuePid)) {
    $parent = $parent.$parent;
  }
  return $parent && $parent.$parent;
}

function handleEvent(event) {var _this2 = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;

  var ret = [];

  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];

    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;

    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this2.$vm;
          if (handlerCtx.$options.generic) {// mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = getContextVm(handlerCtx) || handlerCtx;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx,
            processEventArgs(
            _this2.$vm,
            event,
            eventArray[1],
            eventArray[2],
            isCustom,
            methodName));

            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            throw new Error(" _vm.".concat(methodName, " is not a function"));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          var params = processEventArgs(
          _this2.$vm,
          event,
          eventArray[1],
          eventArray[2],
          isCustom,
          methodName);

          // 参数尾部增加原始事件对象用于复杂表达式内获取额外数据
          // eslint-disable-next-line no-sparse-arrays
          ret.push(handler.apply(handlerCtx, (Array.isArray(params) ? params : []).concat([,,,,,,,,,, event])));
        }
      });
    }
  });

  if (
  eventType === 'input' &&
  ret.length === 1 &&
  typeof ret[0] !== 'undefined')
  {
    return ret[0];
  }
}

var hooks = [
'onShow',
'onHide',
'onError',
'onPageNotFound',
'onThemeChange',
'onUnhandledRejection'];


function parseBaseApp(vm, _ref3)


{var mocks = _ref3.mocks,initRefs = _ref3.initRefs;
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }

  _vue.default.prototype.mpHost = "mp-weixin";

  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }

      this.mpType = this.$options.mpType;

      this.$mp = _defineProperty({
        data: {} },
      this.mpType, this.$options.mpInstance);


      this.$scope = this.$options.mpInstance;

      delete this.$options.mpType;
      delete this.$options.mpInstance;

      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    } });


  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {// 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (!wx.canIUse('nextTick')) {// 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }

      this.$vm = vm;

      this.$vm.$mp = {
        app: this };


      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;

      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);

      this.$vm.__call_hook('onLaunch', args);
    } };


  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }

  initHooks(appOptions, hooks);

  return appOptions;
}

var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];

function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}

function initBehavior(options) {
  return Behavior(options);
}

function isPage() {
  return !!this.route;
}

function initRelation(detail) {
  this.triggerEvent('__l', detail);
}

function initRefs(vm) {
  var mpInstance = vm.$scope;
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      var components = mpInstance.selectAllComponents('.vue-ref');
      components.forEach(function (component) {
        var ref = component.dataset.ref;
        $refs[ref] = component.$vm || component;
      });
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for');
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || component);
      });
      return $refs;
    } });

}

function handleLink(event) {var _ref4 =



  event.detail || event.value,vuePid = _ref4.vuePid,vueOptions = _ref4.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;

  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }

  if (!parentVm) {
    parentVm = this.$vm;
  }

  vueOptions.parent = parentVm;
}

function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs });

}

function createApp(vm) {
  _vue.default.prototype.getOpenerEventChannel = function () {
    if (!this.__eventChannel__) {
      this.__eventChannel__ = new EventChannel();
    }
    return this.__eventChannel__;
  };
  var callHook = _vue.default.prototype.__call_hook;
  _vue.default.prototype.__call_hook = function (hook, args) {
    if (hook === 'onLoad' && args && args.__id__) {
      this.__eventChannel__ = getEventChannel(args.__id__);
      delete args.__id__;
    }
    return callHook.call(this, hook, args);
  };
  App(parseApp(vm));
  return vm;
}

var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function encodeReserveReplacer(c) {return '%' + c.charCodeAt(0).toString(16);};
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function encode(str) {return encodeURIComponent(str).
  replace(encodeReserveRE, encodeReserveReplacer).
  replace(commaRE, ',');};

function stringifyQuery(obj) {var encodeStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : encode;
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];

    if (val === undefined) {
      return '';
    }

    if (val === null) {
      return encodeStr(key);
    }

    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return;
        }
        if (val2 === null) {
          result.push(encodeStr(key));
        } else {
          result.push(encodeStr(key) + '=' + encodeStr(val2));
        }
      });
      return result.join('&');
    }

    return encodeStr(key) + '=' + encodeStr(val);
  }).filter(function (x) {return x.length > 0;}).join('&') : null;
  return res ? "?".concat(res) : '';
}

function parseBaseComponent(vueComponentOptions)


{var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},isPage = _ref5.isPage,initRelation = _ref5.initRelation;var _initVueComponent =
  initVueComponent(_vue.default, vueComponentOptions),_initVueComponent2 = _slicedToArray(_initVueComponent, 2),VueComponent = _initVueComponent2[0],vueOptions = _initVueComponent2[1];

  var options = _objectSpread({
    multipleSlots: true,
    addGlobalClass: true },
  vueOptions.options || {});


  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }

  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;

        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties };


        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options });


        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      } },

    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      } },

    methods: {
      __l: handleLink,
      __e: handleEvent } };


  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }

  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }

  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}

function parseComponent(vueComponentOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

var hooks$1 = [
'onShow',
'onHide',
'onUnload'];


hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);

function parseBasePage(vuePageOptions, _ref6)


{var isPage = _ref6.isPage,initRelation = _ref6.initRelation;
  var pageOptions = parseComponent(vuePageOptions);

  initHooks(pageOptions.methods, hooks$1, vuePageOptions);

  pageOptions.methods.onLoad = function (query) {
    this.options = query;
    var copyQuery = Object.assign({}, query);
    delete copyQuery.__id__;
    this.$page = {
      fullPath: '/' + (this.route || this.is) + stringifyQuery(copyQuery) };

    this.$vm.$mp.query = query; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', query);
  };

  return pageOptions;
}

function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}

function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}

todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});

canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name :
  canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});

var uni = {};

if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (hasOwn(target, name)) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      if (!hasOwn(wx, name) && !hasOwn(protocols, name)) {
        return;
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    } });

} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });

  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
  }

  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });

  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });

  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}

wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;

var uni$1 = uni;var _default =

uni$1;exports.default = _default;

/***/ }),

/***/ 10:
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 11:
/*!********************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/request.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(uni) {//发起网络请求
var header = {
  'custom-type': 'application/x-www-form-urlencoded',
  'openid': '',
  'sessionKey': '' };

var request = function request(url, data) {
  if (url != '/mgr/question/mhm_get_question') {
    uni.showLoading({
      mask: true,
      title: '加载中' });

  }
  var value = wx.getStorageSync('openid');
  var value2 = wx.getStorageSync('sessionKey');
  if (value) {
    header.openid = value;
    header.sessionKey = value2;
  }

  var promise = new Promise(function (resolve, reject) {
    uni.request({
      url: 'https://gwj.maohemao.com' + url,
      method: 'POST',
      data: data,
      header: header,
      success: function success(res) {
        if (res.statusCode == 200) {
          resolve(res);
        } else {
          reject(res.data);
        }
      },
      fail: function fail(res) {
        uni.showToast({
          title: '网络异常',
          icon: 'none',
          duration: 5000 });

        reject(res.data);
      },
      complete: function complete(res) {
        if (url != '/mgr/question/mhm_get_question') {
          uni.hideLoading();
        }
      } });

  });
  return promise;
};


//小程序登录
function login() {
  uni.login({
    provider: 'weixin',
    success: function success(res) {
      request('/mgr/login/mhm_get_code', { code: res.code }).then(function (e) {
        wx.setStorageSync('openid', e.data.data.openid);
        wx.setStorageSync('sessionKey', e.data.data.sessionkey);
      });
    },
    fail: function fail(res) {
      console.log(res);
    } });

}
module.exports = {
  request: request,
  login: login };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),

/***/ 126:
/*!*********************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/address-list.png ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADDUlEQVRYR62WSWgVQRCGq/qQk1cXPCnejMtJFCEXBUMEl6PiAsrr6iFugcSDghgP6iFxDzJdMyi4H13AaEAvURA9GdSToic3xIvX0CUtk8d7caZnXt5rGHjM+6vqm7+7qxuhhVGr1XqUUhsAYBMALMwen+FH9kw4556naTpZNS1WEWqt+5RSx0Skp4oeESedc2eTJBkv05cCGGNGRGSoLFHe/4g4aq09GooNAhDR08zuudSfiZlg5t6iBIUAxpj7IrKtncozsYj4wFq7PdelvJdEdAMA9nSieEOOm8y8d3bO/xzQWh9BxIsdLv4vnYgMJElyqTF3E0AURQucc68AYGkIQESmEPEZIr7JEq8RkY2IuKoE/LNSal0cxz/r09MYYIw5JCKXS5LkWuljqkwdIh621l7JBSCixwDQVwQgIruTJLkdAtRa70LEWwHNODNvLgKQQOAjZt5aZW0Q0UMA2FKkZeb61Nd/1Gq1hUqp74ECQ8x8riLAIACMFmmdc4vSNPXtGxoBViml3gbs35kkyb0qAFrrHYh4NwCwOk3TqSYArfV6RHwZKNDLzBNVAIjIH1a+i+YOpdTaOI5fNwEQ0TIA+FgUhIj7rbXXqwAYY/aJyLWAdj4z/2oC6O/vnzc9Pf0nEDTMzKeqABDRSQAYLtB+Y+bFRbvgHQB0B1zottZ+CEEYY5aLyPuA5gUz14/1pk5IRKcB4Hgg+D0zrwgBEFHZRwxaa8/nOhBFUa9z7kkFm41SaiKO4y9eG0XREuecX3i2JPZ3V1fXyrGxsa+5AP6l1voOIu6sAOElnzKdX8ClAxGvWmsPNAr/Ow2JaC0A+AOpo8MfYCKyaaYBFTqQuTCEiCMdJQDI7SOFN6KSrdQSm4gcTZIktzWX3Qn9jvA7o50R7B+lt2IiCh4sJWSlzasUwBcgooMAUL9EVLSjtLjPUwkggzAAEHeyeEsAGYTfw2Pt2h7sA2VfSEQDAHChQFfJ9rYAfLAxZkhEZveJlou3PAWN5FrrY4h4Jns3p+JtAWRr4gQAqKr3hLxp+wtOFikwO/8hdwAAAABJRU5ErkJggg=="

/***/ }),

/***/ 127:
/*!***************************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/address-list-hover.png ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADRElEQVRYR62XTWjUQBTH/2+rN/GmFU9KLza71pMogiAKuyhsUm+KH6Bs0uJnoe3BgqgH9VC/FWk7tYLfN5usqF3QiwqiJ0t3WkXRk1oRLx518yS7benWzCTbNrCwJP/3/r+8mXkzIdRwGVZuIxibGZQmoB6VX3CNMzBO4AIIz6Xb/yJuWoojNLL2VhCOAdgYRw/gBRhnZV48idJHAhiW3Q1GR1Si0OeEc9IVnbpYLUDSsoeYkZ6V+UQQEQpFV2RUOZQAhmkPArDmYj4t1pWeaA7LFQqQtOxbzNgzT+blNES4XXTF3pk5/wNoNO2jBFyaT/PJXAy0jXri8vTcVQBN21uX/imVXhOwUgfAwDAYzxLgt4HOB60FYQsBTRFxnxfW1a0fftjzY1JXBWBk7cMgXNHOWkUpg5hYQ8c4IvPiajiAaT8GsFUDsFt64q4O0DDtXQDuaDRPpCe2qQBYGUiUl26fGWduGJbjgTmr0kpPTFV+6k/KzNX7oO9qA+qQXt/5WACm0w7wOZU2AV424vWPl1fHpChl5pp80DtVEAM7Rz3xIA5Ao2nvIOC+BmDNiNc/XA1g2Rt8xitlEFNmJN9XiAOQyjppn3hIqfX9dfLRjTfVAM1Og+/zR/UcSOyXbu/NOACG1bIP7A+otH+ZlnzI9/2sAjA2HViExX9+qw34pPT6T8UCMHMnADqp0H6TnliuWgUjwXJWmRAlkkW3V+ogklaLwewXNZqX0hNT23p1I7Jyp8HUpQkuSk+kIvqA9iUAtEtPXAitQNKyM8x4Gllm5ha/bkFhbLDnS6Bd1dy6IlH6mwZRrzaW8KvEpdXvvYGvoQDBzUbTvkfAzkiIiuDThK4hjp4I14uuODhd+99uaFjOOjC/jpOwFk2wgdWB05MNSFmB4IGRtTtA6K7FIErrM2XGQvqI5kSkXUpRftXPGZ0yL0Jbs/ZMaGRzXSA6XZvbTLW+f0SfiiM2Fj1cdPOKBCjPCdM5BPDUISJeRaLNgzyxACoT02kBcc98mtcEEIiTlnOQma/NtezaPhD1ho3ZXBsRXQzXxSv7nADKw2HZHeCZfaJ285qHYDq5YdrBx+qZyr3Zmc8JoDwnTOc4w0/EPSeEDds/4WQ1MLrINQkAAAAASUVORK5CYII="

/***/ }),

/***/ 144:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/jq01.jpg ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ4RDgyODQ1MUEyNDExRUE4ODVDQUY4MUQ3QTZBRDhBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ4RDgyODQ2MUEyNDExRUE4ODVDQUY4MUQ3QTZBRDhBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RDhEODI4NDMxQTI0MTFFQTg4NUNBRjgxRDdBNkFEOEEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RDhEODI4NDQxQTI0MTFFQTg4NUNBRjgxRDdBNkFEOEEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABXAIEDAREAAhEBAxEB/8QArgAAAAcBAQEBAQAAAAAAAAAABAUGBwgJCgMLAQACAQABBAMBAQAAAAAAAAAAAAAAAQMEBQIGBwgJEAACAQMDAgUCAwUFBwUAAAABAgMRBAUAEgYhBzFBIhMIURRhIwlxkaEyQoHR4VIVscFiojNTFoLiJCUXEQABAwIEAwQGCQQDAAAAAAABAAIDEQQhMRIFUTITQWFxBoGRsSJSFPChweEjMxU1B9HxQghygiT/2gAMAwEAAhEDEQA/AIx/q7fHmay/8e5ZYYSOCDMZ2Lj0+bkUxvjMveq6477mboWscnM4ikLelXYNWoGuNeWLv9P3hhPxD2hel/NFj+t7W+/biGgn6is191kZbcy+5DIjxyGDIWlRSCa2kMdwygk7p7PYwY/1qOnjrtEs/wA1Ibgf54rz3KzpyOZwNEo8Rlre6tL7j2SUXnHsxGS0e874rgwlIr60KkNFOoYVH9W0A6k2/J6VCm5/QnL+JXfTlPw874Yi9lyRHb7kV3aLkZ1dhbmORxBb5aPYWFIU3pcJ0BZV3DoKYXHZ6UyvQF+IfyEt+R4Lj/JcRkxNbX8FrIkkbrRTIElNTuPQhtRkK2HlEGO7ocJS9jlR723gEirEqsyybDVlZeqsWWtfroQijsD3L2zXnb7lEwYIJ7RBcswLIGKxhTJ9B00IVOX6yHFOCfHTj9130yF3FjYpZVgsMWkscV5yaWdkMVvjYShe6nt4XEshAKxRMDIUqNVpzSKoLgPcSx5zxvF8twdxP9jlIt/2blI7qxmRQLizuoQ7+3c2z1Ei1IXxqQQdKocnOU6VjkZnaju7bhSrHwB6fw0hT0PKfFfmAtpmCyAgyV3KZHDV8qV6U89Ink6/H4Wlx+wI8zuSwEYkkd6AN0iG+n8KaVNy8hStGHvce8b3drPaKAwUTxPGZPdUFSvQnrXz0qiLjaWU8DPM8cgTeGLNGyL/AOlm6P4eWhCUFoyG6hkkO2Ij25SfBY5F9tnP4KGroOaiX2Q+nBU98CSTm36lOZvneaVOH8EubrfCpMcc9zxpoWikPUVb/wAicazt/wA8eKptk/dJPEexq2o+2P8AK37j/fq1ot2xVT36lHYODvL2l5HxmFBA17bLmLWSCJB7Nzi5kvLVYiANjSPAFBHWhprhE1u6DfGXgwtw4EjszHZkvT21XJn2F+3kkyOaRj3rz/8APcRueJNlMFczvd5THZnK2l5M775lu7W9mt7oSyMzEO8kRLEn1N16112a0mZcWzJowAxzQRRcA3i3Nruc9u7NkhCb3E5CSxyBsrpdqMxmtmfrErg/mQHxUe8Oo8gQfr1trf8AL9KpJuf0J28dcYLO2T8b5HH/APXXbmW0vo4Ummxt2yPCrwFkrGlJiHUOgdR4VVXjeLWuzAKaV5/6avy0y/x2y2M7Id8Ly8scbKtpedu+XXkzz4nN4XJBDZ7Lqcn3FjjKhdwqi+npQ6Yma0MwAqkWyL4+d5rO5xlrLDkYbjFXlrG0sguIjaPHISFuFkdgkcezqfD8NRmCrwDlVChd+or+oJ2k+HvHrnuTxe/tec8xUSwY7Ccev7JsBHlraN2lTPcjjZ7CL25FpJaWzT3z/wDaUVcTtDOA9SVZl4f1Aee/qb8g7hcS+QucxkGa5xgkwvELyMNbWPE8fjmM1hxPjVk5ks8Dip5Aj3VwvuX2SmBa5meijWPQh+BvqCRNb8Ps3zXth3f5P8e+W219dQ77+SC59sG3gkw8BZMxamm37XIQqsMrAAmQKfx1T3IDZnBuAqoUnOVbti7ETKGRtwpT6UP06kHz1HKeh5fSlZj8dFA0RuLcyq0ihqqrKOoNWahWlPx0ImJAFCrGOyEmEg4TlGlixkHtxkxyyLarIAAOiu3qLdPAGulUcuccyULiynH8pkLjaMfegJ0a4gjYEqKEAzxipBGhImf5VBZXYYW5gDJI0ckSKsYQ7z6dtAKU/s0ITaZm3tsVgM7kZZG247FZK9lChiUhtrWWWRwEBZ/bjUkKtSSKAE00iacA60e5wqRXH0qrH4Rcdny/yr+R3NLidnmx2NtcFHWTdES91b2rtvLFVlSG1IPWtIgPIadt/wA4eKrfLjWOldIQNdTjTH1rZj7J/wA3/N/jq0wW14KIndj2OSdvrmdPt5HWwljgjWUrK0gikeNGY0KgkdT5a4RfX8c0ZZCQV6R2CwvIJQ+YY14YLzofkZaQYX5KfIHAW6qLePuFkcrbKvVIYsvBFk3to167VjuLiTr4VUjXUfLBc7a7aN2XSH2rkfnJjW79eS/5GY/YouZa3WaVh4MBviatFV6ttevmAw663BrGsFG5LR9ZealKfiOWix2Wx99kMbFnsdYfdR5bjN1fXGKgzEklo9vGxv7WGa6tJLU3PuRUGxj/ADeWmpnuZTT2oTnXvcrleX4tx7hubzuQyHGeKXN7Lxi2mKXQ4097It1eWuHyRhW8sLKWZxK1tHIkHusZPbDuzNHdK94o7JIl/gfkL3fgs4uNnuvzi845ABBDx+75TlrnBSQMTvtZsPNdvYXNtKGIkjeNo5KkMCCRrFnOPFCU/P8AmnNu9NhbRc+5TnuUPiMRb4TBDOZK5uMfgsJBEsFvhsRYyOLDEYmMKAttbxxQf8OrFKk/8W+wPfDuH30wnCuzXHL7Lcnmu7a5SW3aa3xmJtfuEiOU5HkQhjw+Jhc+uR6sQKRpI9EaEZ31SLdVj/0reOdsOyI5vl89/wDpXf8A5TgLWPmfca3smt7KzlsbX7m143wvGGMyY7i1rdOwcuRdXp/NnpQKKyZxdKSc026JjjU5qlm176NxXvxyDsFzPj543nLO2trrj2UuchbSWua+/wARBlrWN4ljD2zywO/tVqHkUJWrAabLHmMvYsmtDRQZKQkGXnn2xyzSKzbdyb3jJDepD7aAKu4dR0FRqHbvndI5suQGGCanyCc7Az74o98t0EjboFnnWJWagaoDKtWHQ1+upijJe2lxNR/t1cKobaql460Fd3uA0av7dKMSB3oRjaGaWcbmlp+WxT1FVbrX1/1V89TOhH3oRB3bnmxna/mV4jlZHw81otOjf/NKW7L+BZJCNR7trYmVZmq/dJ5LW2LYaUI7e9Qo/T045PcYbvN3BeJo25j3Jk9gxlX3WuOsJbt3RvFoZZMmzVHQlf8Ah0toNUbZTz/eoewSOZE2RvM4mvrWtb1fQ/u1O1uW21Kz79+PkVPL8bbTutxbMS2XDs5wqz5RaXwyFvb2j4e8sRevdzuzN9sbdCyutTIGRgVB15ptbSWy3Jm2bg8su3EAClc++q9xD9Nn2N+92jgY2NJ0gcATn9yxO5/OXfKOdc157c24yn/l3IrzJJcXJlEsti0rJYNCUkV4iLUKwqW8aEU16H2XaNwtLSEvbH0QwUOsCo8OzwqvHXmLzLtm679dwwC4+Y6zqtETnAHgHYA+KLbnj+Ovm+4gmNnNO9Hgvggjd6DcReARopoQDvVQAKlj5XE0zmSiNvTIpnrAx4ZKrdZXLPcbBdmQ4j8E0p/yr9SSk1he4rOSQ3Fu6pPGGWVaNAWCn8wTIWjlWWNKKVLA7TpudlwA10zWtYa6SHaq8ewU7E1Jb3EAHzDCwnsKdaz5JLi+0l3wuXiHHr625lyLH88wXPbiwU8rsbWwtr3AXmAsMosjmPC3F3FJJPalYysyKxVt2/TUbA92kmiaSb4nxfkXIMlBj+P4q+yd/cTxxwR21tI0Qlll2xiW6p7EFWb+tl0/0Ws97VWmOSFfF8Lf0k+43dfJYbPd7eY2PbjiCtFdTYbj01hyDnGRtpApNtA4afDYYXERJM7fdyRjwjVuoT5ju+tKtVXav4ldmezPba24f2D4LY8flgjjORykSG+5PnrtIz7mTzvIZ0fJZG6LMzn3H9tAaKFGj5euNUigF8zv1oOLfErjuZ7B8OkxncPvRFj5LfJW0VycjxfiF21vJDEvIsjaS+3f3FiAZGx0EhMzARztGGKMw6w1OLtefd96KLILynu3zjmHczOd0+T52+yvN8xyBuV5LOO6mafJNOszm0lRo91rBFIHtl8EtwY6Uj1IigbDA5p97Gv0zRRaDfjnzK87sdquLcwv7D2MhdQPZ3U2+No7i6xsz2k17alPG3vWjEleo3EgHp1pHU1mgomJ8h4qUmExGUqsaohiLl2q4B2kVqVp0K+PnoUZOVaYe9B9En5YTqy0Kr0AXwalWP7tKDQ1QjzG4q6ilLSSrJGNpZkoWXxpRNx3V/aNSPmO76/uRkmo+TF3b43tdcxTSKYb/L4lHMssdoixW0n303uzTSLFGipAASxp11DubnrDTSlO/wC5UW+zUhpTs4pCfCjGRWnx/wAJeqjI+Y5DyPJqJUQERyX0WNtwoicqNsNrIR1I8tS7QUgaPH2lP+XrfXt8dxWhOrCnBxWmX2/x/h/jqStmXmqcqzvyxwNzhPh7NddwOf8AbS+5XDjbXgmNtDbW2Z41d30k0ktheSW8k+MxF7uMrwvd+1GW2nwWuhXW1WW/bBJ5gka6PzJG0lhd7rAQDSpxOdOxd28leZWbJ/Jdrsm7udN5FfKwStjo6YsLmh1GktbXTX/IYqwjFfp58hwf28Ddg5JWWFJBHkcjgbt40SMu4cS54JbsqA1qtF15p3Hf/Ppu5Le6uxRryKRkkDwq0L7E+WbT/Uu32OHc9p2a6fcCFrnOnjia9xIxJAndjn2qTc/wQ4DxDtZZcz5Td8UsM7koEkteI4zAWGUniuy21sTJdpM0UuQsypE5AMCkEB2I1nt1r5u3N7Db3NwPfxLgQOzI0K5b59/2L/iTa7C42rYvLloLAMI1yNaJtZBBoBIRopShrWtcFXx3M+DvFeVTJfcbyJ4fmfdeS6hWxabCXMu8FJzYQzQGxuNpKsYmKt06CnX1p5bt5bXZYIrmV8t2G+9XIeHbj21HYF8h/wCQfNNt5q803N5ZQ9GzDyWtoBzeBPDik7xD4AWcmStJuacvfLYq1lRTiuOY+SwklhRjVJL+8luXRZD47EB6nrXVvMXBlW4FaY1peaDNT44r2E4rhsCnGsJxnH4/DRBFijs7MNdIVYsJ5pNm+ecOS3qLbz4+OozJJC4AnCoTnQf3Ih4B+qNxPsBFmOKZfi/PcrmOG5jNcZihGFMLZBcTkLiytrhbua7hiW3uIYVZWlX3dpFa6nJOi/uTS/Ir9c75U8zweU7e9nWg7N8Oz1tcYPK5zDhZuZ3dtcxRLDEMrIFXF2WXsSylrePf7goWVgdWIyS9B/cqPc6cpnMvLkScjfZO+neW6mmlkubua9mkLS/cTyymSa4eQkszMWJ6mp1lROG1kazqEjSVOf4k/DfuX8jMjjchksJmeM9tsRdxpmuTZCELHeQxSyI+J49R0OQyU0q7Q4DxQKzbutV1DuryK3/DfXUR2f3UB8zGGhqtJvD+2Vtwfj2H43xXHNjcDgcba46xsgiVjgtYyGeopV5WJeQn+ZiT501Q9ZhcTjisW/8AqOmPAjj9CnP4uI3mKmSJqehnYUVmb0lR6TUg+OshI1xp2rP5Gbi36/6Je3kVvZMYxMgR7WpCkijBxXptHQDw04lFhMTQFuPf9yDWdxC88UccqEM4RAp6kkFvUPpRfx0lUk1lNBz6fR/ZRA+dj3Vz2mixVtIduRy08Ug9oSD25YDA0koLKVEQuKACoJH4ar55Gwy9N/McVpfmCRrSIDzn1fT0J7Pjbxs8b7KdusQVCm24vYXEodShaa8Rr2QhDX1GS4J/adXkDS2IA5q/2CJ0O1RRupqGrLvcStDenlcqhJOF8Oxv+nX2PS7fP2c0Vwt3Lj7C2ghkSVHaKJg814IZCoJWoQ066iX1o68tjYwx9OFwphh3Kfa3ku2byzdGuMrmuBzrkQfsSW7h2/JecZNb27zuTxjQCSOODETC1jIIYFw25i+8Egg7RQ9BrRYP4zs4Lp10/wDELjWlB/VdZd/OnmW2t/l7XqdIilATgPUmvPa2O3tQqzXNw5FxKxlZ5JnuZ3DSSyyyOxLzMaufBiOutztNvsLCL5ZlsAa11U44fYua7r5k3ffLg3dxM5oIoW1zzx+tEJ7XsdwZEkJpuUxBnXxpueoDV/DU6OFsWLcj2cFQnMnt48UosJ2tihAAgRd1S1VNSaE+JYkfv0SgllBmnYSGvqck5PE+3BtY7m5VVZrm6VUG0VKIuxdheqp6l6Hp11GbG/UKjCqlh7XHSDVxVQn6nXxnuMWT314Xx1Ghtnhx3cW0xauZPahQx4/lfsRwbGdqpDdnr1pIT1NZic6EvwlVU8T7Nc87wM3GeG8Lz3Jr+4HsRy4rGtLLAksouDtyTG3sY3guH91fdnjjDV9ShidTxIymYTGtnEVV4HwX/RkzZ5NxzI/KKSwae/CZSz4Fx/I/cWs7RKgW35Nm0qFkuVbc1tbGSNuo99wSNV809wJCI8WVwUiSWI2waHDVwWizv/8AGg8T7Zcdue2WPtcJbcAw8Nhe8fwdhZ2OPXjDIzQtY2kEZSuPO0uNqtIrMx6iuq+4bcTO1uacBRQre0E7S4itDRV2QXGUIaP7t6kAItFLMK0faKVPT6ag9tO1SBai3xApVKvj1qBM+1AsjBJGOzpRW/MfYRtWnmQNORc6VLzIWkblxOgZjaERsV213yBR4ADx6alrJpAcCcqpM2eLMWTtPcHtlZgy16ejYakgHrSo8dImb+Rkh9w1UV/mlirzN47BYfHg/cPG1zMil03ItyJ3balBuEUBFaeWqi/IfdsDcTQLmXmEh96yNmLzkPUpdYe2jxmFwmPAWNbLFY2zWNfR7fsWUMQQk1DlCtDTqf262hoLWgHA0C3PbopILNkcoLZAMQe819iu9qPqP3jSqaqXWEdy59yKOlK0VQOooP8AfqzSrjNYWhG8wRkrSlR0NfT1/sOhCBPj7R2De0i0oKBRTp1qa/t1DuOf0JEGbAR1LGFQWNRRUAKf0Ho7k1qfpphCM7HEWqAen1AlaUFNxBFOgrSo0ISstMdb2GNgWWMAFfddgKlV3M9Vp5gHQlaHFwDOcnDxRKg4tfTtFLa2cklx7ltKt3apKssEhAljZLhHDLKAAw/lI8tLgpfyu5/EfWUbHjXHsZhrm3tcbjrOwJX7i0xOPsbOKPedokMMEUELUDGpYGldIohBBxzTg9tOdW+LnhwuSrFibedY8NmykZnxM0SCWMybD+ZZqwXch6KK7aaEitV4Lz/i3KOPiG9tbee6ljFtkoljhuILqCSN7aS5Sb3FWexvI23KoFUrtJJB0P8AyiU/FDdyAm3NGqpz5B9tsf287k3FpiMaIsBlbi4zeFu3t1Ht213J+djY22hVOOm9JUDoGXWvH8wp5sV1Ga3Bq05JpoLVLfJQXkVXjaKUSyUUj1PSj9OpOnYucJwI3zF7BJN7kqho2t6ulKFds6vGYAKfykda16alJHcp8ERwzJNk7SRVDLI5QbvEBVLfxK6RVyYDu7ILnunxnFbZGiu7ZbG4dwjx2lvdyWolQs8chHui+ag6fyih8dUsv7k1c+3f99i8SpBT26bgpcqsZB3FdgoOtdpZzt69OvUa3GTm9A9i6RPz/wDVvsCuq2R/9zTaZVIkOQbeFBLM4oqqCST0P06CnnqzSoZHfFS++ZnRj/JSgA8hQ9fSf9mhC4LkEWUxKf5qP9abulAPEnpqHcc/oSIVb3ZuJ0hEsUO8uvuTSCJVMYqA+6hVn/pB6mh0whdEu3FGDihHiN3QE13N09KjzOhCPZcpBfWz2b3SKPbVVJVjQ7ACelCwr4aFnG7RIH8DVALLH4eFvcvbqWeSN1kgW33woGA8ZSS5kWv9PQaRWn6ixHPIr1riyeKxFTJ7SufIoKMwAWvWqjQo+eKD4iSL/S5bX7Ocs0kjoGT0tM4XaVbwG0jofLSqHJzlPr2X7oZHgE9piMzaw5LEl2Ed3NIkE9gZZmlfrX8yL1UcN0P83joWNSMlMfu9wyz709tZ8jg8ZbXuZsYP9WwV5bzQt7VxGjGWyVkVQUuYaoRWtaajXAFAkJPFViYKRneG1lx8cNxYCa0uLadgH90uyMpUGoMbChB6imoUnKgEpRZHFxwgzJiVMioqPGpBBHXqB40NdMDMJUkGx8xuo5VsBaou6tN1G8KVr0FNSlio4Zb7nI9/MWovLuO3s4pr27tI3lS3u1sQWiEoiZK+3JGCKmnTVNL+4sXL8Tvjq/G72lP208iv7ddxXagZgAW2gLuag8WIrrb5Ob0D2LqRyHgPYrqqv/wfu/w1gkVCxl9obqVr08SPxrUEHy1ZpV9e6MpR9+wqPUor6zSnqrWtP92hCDmd/uAwkbaFUVB6Vq39+sSxrjUjFCEC8aPwO+rbuvWjDpXr4nrpOnHwCRGMF5LEYpIJGDrtKgBGC9P61lBVkHmKHSdNnAJUKTIsCzyCsrO7OxVV3EsTUBAEC/SgFBrF8bA0kAZIw7UorG/iYAsikGlennT9moSx0hGUd2pZUXaiFqmngQfI9T00LPW/iUYWc5QlXkK0JKgkgVr0p5eGhISSanNHEUyxh/cRZI3U7iw3ij9CPBiAaDQkUhex/em97Z3ht83LIOI3kkcc0YCzmwLyhPuYDRTWPoxUijDxpp6GNkhIeAQAhC/kZ2yscNlLXuBw/dd4Pm+Rjye+1EbQWl80KvdLbtEdpiyKSLIqrWrh/wBmqqZrRKW4aU3ISGEjNMpcm4aOOQe41UQy0PWi06PX+U9CKHrpvQ3gFG6j+JXaCGzmSr+6NyMSppQHpt8ennrJHUfxKjFaY6xm7kX2QDhxHZ5j3ZY5KSBXeztbeIR7abC3uMeviTqphAfdFzhVwcaFazttvBLfvkkY0v6hx9JS2SNjMjdZELijEVqK+f7NbKwlzQTmt2fg6ncPYrtNi/T+J/v1ksFQ2LD3YSzyqhHh+J6ilOurrpNSVQI2Z8FLMfANSimnn4aOk1LVfDYSDxcJ0rtKMSR9agU66be0NNAgLpFYnqJQ1em3b5+Nfr+Gm0qGxWcoZdo6UoK+O38fOtNCEcJji6gUDAjqQRWh8fPy0EVFChdre0nSOiCoqa9K+BoPp5aZ6DO9CFPD7ahwzggjxPT/AGaOgzvSLrHPPOGiI90gERr/ACNHQV9wOehFRSmjoM70q5DIZWCNo2ikMT+k7SQSa0B3eXQaOgzvSIytsplLYKgxzZCJgyPCS0ijctDG4YlTQees2RtZWnalUl+1/eePD8UvuEc+wWWy/C5baa4xbQRi7vcJfwszx2FoAQwtmlO6OQEGNmp4eEWWziJMhrqWTWRvOmWoYumVwuG5Hi5uY9u558hYXNwkWUw8jwJd2177RDWk8Ac/b3qe2WUGiyr/AC9fGlneY5A1uVVBuIi134OLE3M08EtnP7D+tY5I0CEiRJ1oNksZAkikVwQykVFNOJpRw4diZ0z/ADW5mi/MS4tLOEykxAgveM2xmFCxEALU8zqJaQsM5JrzKLtFpEXSTY69Z9pT2YLjssjDfBVZoz7avKFAJP8A1Ek2ksF+hA1eaQ33RkFcNeZBqOf9Fcz/AKU30/5x/doWSoXa2tEahltytOjLPG3X6EKxpq71n4Skovj20ZUbLiDYaGpkUbaGoBqRWpGjWfhKKISsMICiR4SadD7qj0/sr9dMvNTUiiULqtrZs6lZ7f016GaNCQfpvda+HWldMveW0oCUq6m2thIdlxBu3DdSRWIVj6waEj0+flpGvc40LSEI1jjs1ZgjQsCNtVkBFadWJ8AK/wANOoQlESMEQ7HB8dksbAHyrRzQ6EIJLHEwIlkjVd1erU9XXpUgDQhdYba3YIqSwCvg7TxADzrtLbv4aadI4GmklIv6mhCrtSWOQeJcMqqD9Bubr01j1XfAUq6WrTxSD7b1wMNs4QM+1v8AMzKpA6fXR1XfAUiVEE88NqirC8tjI0iO2yR4lah9xpfbBZI+vU9P7NGsv90gjvKbla57NLTpPFF3HL7k3Hss+e4FDcZm2LW9vyDDR2M+Us0VpGMAvLC1SRmjlp+TP/1IWP01U3NrG5+sysBBrTCvhmpELgyItcNRpmneyDcd5cs2ZjZ+G5j2Uku8ZmIp4pTcVpNaXNiIorgMjKxjlC1YGjdaHUdQOm34gmF4jj5jmsjb3mQx9zjJr6PIm+d/sDGZmnX/AE+8hlaiXdvTorbZCvUjrpbKNpmPvDNLt8bYGPDHB4Ljl2YnPNPVNDaCNI7aRCYyfzIZECutfT6q+rpq0kFHkZp9go2gNcT7Vazt/H/n/wDdrBZr/9k="

/***/ }),

/***/ 145:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/jq02.jpg ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjhDNjM2OEREMUEyNjExRUFCRkFBODE5MDlGMTJGOTUzIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjhDNjM2OERFMUEyNjExRUFCRkFBODE5MDlGMTJGOTUzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OEM2MzY4REIxQTI2MTFFQUJGQUE4MTkwOUYxMkY5NTMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OEM2MzY4REMxQTI2MTFFQUJGQUE4MTkwOUYxMkY5NTMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABXAIEDAREAAhEBAxEB/8QAsQAAAgMBAQEBAAAAAAAAAAAACAoGBwkFCwQCAQABBAMBAQAAAAAAAAAAAAAGBAUHCAECAwAJEAABBAEDAwIEBAMGBQUAAAADAQIEBQYREgcAEwghFDEiFQlBUSMWYTIKkdHhQqIkcaFSJReBYuJTJhEAAQIEBAIHBQYEBAcBAAAAAQIDABEEBSExEgZBUWFxkSIyEwehscHRFPCBQlIjFeFiMzTxcoIIkqLC0iQ1Fhf/2gAMAwEAAhEDEQA/AMuamxn3njn5O8TAFSWrsx4xyDPlBIRs65x+8wJ0ORGLTyWypC1yyqxHveJE2qwTvX4dDFF44JaT/wBceoxjJj6DJFx2U4T3KOO6LKa1EV7idmRFGP8AD17qNXre4ZmJL9J6jz90UtMM0tKH/KqOXlVa9aJI8qu9iUsqNOcAAlSPFXSVGT0R2m0oZqLqnxejk6SWucj/AJj8IKvVexmT1RLCf/STAyzqawgOc9RNKJ5HK0iPRvyI9Wormqiq1V0106MPwJPRFXWSApbY/CfnH0VsRoyPKRdDOYuxunwRE9W66+uqdYhRH1SWhMMg2yG73Ii7dFRd7UTVmuumurdNesHIxsjxjrh33+mgWxsvDTlkVMGDYFDzTMjGS3ubisjxlPjNWjAiHWT4aKhnNX1ejtVT0VE6G6/OF8NHYxxtBjvEldXwI1g6IMlrP/dd+R8kOrXbI8abaTGhb3nOVyO2K/00T0Veh8qXPMeyMTgleJJGOU2WFgihSgzewBsuehDvjleq6Ba4rJjxnY1VXRrm/Lr12piS4Z5wjcP6hgzGPa7/ADJuRFVG/irUVfX1RPj0aUX9H740JmY/bi7U12/xX1XXb8FVNGrq7VU9PT/j0rjEJ7ff5+8ULDZEzw/8XeT7Gmy6JIX/AM153h9kMDK1hWPQPH1VdwxPsXWm1riWSxSjYMZBh7m7uNZ0aDRWA9/Tjm74DCZNlmdtk0o8qRkFlZz5UskidOl2O7QpSq/dLNJdKeiq52qpu3OVfmX16K6Nu2BucxMDnCBfgPUYv7x/8p+Q/GfO6rPOOORrPEcvr7evOM1NYSv29bQI42jWvuYkUwWWcEysVHteNzEc7+VdNemS5Wm2XDIjH7c42txMofA+y/8Adfi+dlVlHG3Ksumgc74cH6sCthV7Yg77F/RpZ0Qo5RBSywpB2oou2wiC0e746IMmlFuUdM9AwHUMIcjnG7l7UV2SUdvQ2gosqJZwJtfOAVn6aRpkckY7dd2rV7ZF9UX49c6pYr2ZDjHssDnGW3bT/pd/Yv8Af0+yjOMJNeO0TDqTOG0OV2D6mmyWqzjFbfIZNLW1dRIj5Zi06jjQrUqucNYoSS1R+mrEbq5Ph0EJUpPhJHVD/TuiqT5TGCTwGAjBtlYSjfkFO9o2nx7MLSI5qo5E219m4SK1WpuVWdtVb+KO0XpZXY0CV/jKc+PbBl6WuKtu6lqdJ16yEmeKRI4JPAdUo6OTQLUzbU5jJ7RGEm1A2KN3ZBCcsosdQb2DRr2N19XblRf5fh0OWt53ztM1S1cz0RYr1Mp0ixFbgBWtGqZEyZpUM8+EB1c3oGJLrjjQcpko/eV2j2ruI4jO2ujkTRj01/w6kdP9FB64pCtKRUuSAGPziPts3K3fFexSR1b/AD6aqxv4aL6qionXozH4WdGfAlvKNyzXHR41jsGFok11c1XoiOfu/h8OsKBKSBnKPZYjOHkv6WjNqem8XfISukRio93MFHJYdlPBte57jGoykBIdMRXDCpGL6J8finQtcKZ+eCldsba18z2wzz9WjTJlpIxjIsfNQwBwxSayTh1dIkRZB0OVWodgpDjNR7Ht2tRU2on8OkXkUnFYn1xt51R+T2RXOTcqSsUjmjoSpfMs8fs5GIlgYLjvuoM9TFix5rIs41UZxYs+OrkYjXuexdERUXrrTMUvm4Lw64TruDiFlJaSSOgQW3ht5ITs2oHY5yFfOPlUE7Gx593j0PEjWMJRKN8eMANkVkubENqj1QbNfy/FX/zmqZxLaFTSR/CMtvl8aikJ6os3z95pd48eGnkfzNGnlrbTBeJMytMenBcjTxskPUnrccKBznjTusvJ0dU1XRdOl66htKQrnHThHkA397lGaW9xbTZtjYTbGVMsZ9hMeeXOmzJpnSZcyXJKQhXSZJV3lc5yq5V9VXriqsbIkQM41kDgcojSXEtjwVykeNI6DUzUVwxmc9yo579FRpBsYiorfXcvr080VUwUYhMpchGNCMiB2R3v3ECAR2g5BykYZj3vK8rvkRzBK16q5WsR6atT8NEX8ulIqKdOSUDqAjyUJb8AA6hKNxv6frlwmMfc88fo7bA4YebysrxKx2TyR07Vlidm5EOvqhWhdGY5Gu9NzE/FE6YbkoqBP4TGwzj05hJKM1hIseuLEMqKQjyEaYo3L+ojmsG9hPXVERXaKnTcxg0JfbGMmMy+yv8A1f6v8en7CN8ISA5W4pn0/N+YcRMsoFRj9VyaGqnylgy1jVYJGUfQHSGSiIxGRQxbdxSK1E1CxVVdqL0KVbLbQ7ghys5LFellvwTHvEYw8k0R8e5S5axiWD286ryckl403bGNlGO2QH5vRfbSBbNf83x6wg/U0xbdxSkYcPdBxYlil3VqcyLhl2RFJlSSZOeds7u/VKhjQ1KgOuxEq/ad3vtb2txDsciMVd25Ph8OhKjUpusKU+HV8os7vxl6ssSPPxHkYcMJKgJshhBBkVi0wu80rxF+CrsQgmuamqa6K5ioqfmnUmU61LYTqijdawae4vN5J1Ye2ObIhwhNcQCbXkDqrEaqJ/Lrpr/DrsISOKKUzGccJWLte1EVGvXV3oumv4+v4aL1ujxDrjil1ZUAcpw8V/Sa2MGXxR5VY1PknjNq8546s2IKeCAivlVN+AmrpIyCe4ga9mm5q/yKqfj0O3uqcZPc90Koaqy9JxAjl4UmcSmvnAZJqaHL4NT2xMSQH3kdphkLLVqgRyNaxUej9U+PqCm3lR1HVj1wSprRpGWXIRbxLy+piVchK3MbmyZVT2xI0e/a4sVxXOYxSFUZCzJmxyq9GhRrd+umqIvStm2hKdQ1aj0mGipqVl9REpdQiR0eV5RPSBMfiOU0eRxUAOPJsYsqySINxx9/umk0zP8AblE5+qsemrtU/Dpc0FtOBJhIpalnUrOLE8nuIMN8oPGLlDh7PpRXYryFhNrU3EyIB0eZBUQffBsQMI5RoWumwmF2OarXqzavx6IkJLtMVqzSJxvTsmofS0DmZR51XFn25INAfIIOTXMS0ej1ix5DIUuP3e0eWwkmSpm9sb5oWN1G1+o3tcn4L1Fd53v9C+aZoELnnIHrixNh9GKa42s17xB7pPiUMeGR5xmF5AeJvIPHWdZQxuMWRcTiTFWtu40WQ2vk15Svc0pJKq0UdQNejHJr8U/Nei7b28aSqZ8p8/rrEgcsTl7YizcGwbvZqpYCVGmE5SBOAyxiV+NXhk7l6BKyTKrO6p8YgzqmoeKpBHLdWM6w2MZHqpE5pIQkjMIxSneKQjdV1Y5etb3ulyzf1VpPUBD3tDYCr9/cNKH3qHugsfD7kTC/tZ/cdxnkvI8XvuUqTAqi2sqPFIs6ph5QWRf0ljXxQr3YkUFjaVyGc39IAlMrkc1iL8nRDbLsncFkbqqbCqK5T4S6soFN37Rr9tbiVRKI+hCJgSme048RHpC+HXmtxr5t8VUHJ3EE9YcedGiT7fH76Nrc1cWXvQDWpHfHCdClGrWla3tq1FXRFVqdF14sNRaGUYYlIM8eInANR17FS1MkebqUM+SiIor5vyX+zrjrVC6ZhSf7k/FK0Xlvy0KBGlCsstHX2FXGjRZxV+o3FUN7nEWTIiQyx0lDIriNem12i6p0Jv1JfEiJQ6Wz/wBojrHvEL2cx0kz/wA22Q7IzVk5DgopZTOeiMPNp9x5BSla96KVzY6t13O3OX4+vXqFeoONSwEGdxb+kvFJVBUtRnKK/wDfjqj4zZvjpNdFZYMGF+qQ3fSXfUWx7JjdHvbMdH2M2ruTVV6GWUJbryCeM/bFxLklF82s08kaQljThjiAT0fm9kA7yGP/APVSJiMSOWQIZWgGn6LUVXfpNRyq/tjRdGornaIunUkUhR5AmZYRRDczZZvTzIGCVZ84hpZjXIqEGbvIxWqjBqqOXTRzk+CI3pRNHAzMD6klYkcI+OOTvkYFrdznK9zk1+CbtU/9fXr2rT3uUaBkAgzh0D+lDvGR5HmVRd4YmLF4ks3gLGHLGV5iZlCVO2oSkYo2u9XpoiJ8ehi7j6g8o7Q4FkFXf26Qj0H7RrbYZYjye9W8jxyQGDMhBIGEOKRkl71YrVVVZoi/w6avPaGEvt2Qo8gn8ZlHZjisqiNHLeUWJS5EmFLZNtC3lyGLGA+UMThOaQz5bAo5EchGI75lRNelFPVtrVoUkBI4z/hHhQXFXfSlBYOSioTP3cIkFZPDbwK6t2VogisytIytyKXDDsAIh0UcmwdHsZKKBm7b6M69XBpL6Vtnu6fjHJbamjpVIr6MRF7cW2P0mRJx6XJjSqixjvMLfZRZgxHJ8pIHtnPcVXF+KqqKzTVNF1670lwYWRTEkasDhHRlD6SXvCEYjp+ULk/cn8dY3F3P+WX3C2U2+L1sijqcqt8FiV1JNx19hevsnWoIqTK6TLi+4ZDGZgWFawaOcjGpr1Fm+EWujrxStthby8dUpSljy+MWo9NDdr5Z5rrFtNJOQBM5K/zCM/D0cC4xWfHy18awbaxnilRUQbACCcfzjYN8cLEd6+u34L8OhOnq/IILaZEGYxiYXLTQVTCWKuTihmogY/cZ++KBxqmxPioU2rrADr8O2OnyBsYF0l8mI2KoljkMitRxjR2bkTRUaq/l04OCou/9YlR+3OGp5FrsX9rJA6pe6M4OZOKy8/cwYrmGNIT64XJwBv0GRhHzaGOPcGHWIrmgLMMcOg2qisRNXu+Vq9T56e2P6pli0MkoRMErAxBlOUsPfFWfVOs824ru3na8CAiXDLOfwhh77XfkA3w28gMNqJz7K/4o5Up8R4giUNDZDsX0Gaz8glQxXVjFeRkZulsdBKgERzgaK1ummtnN47KrDZkOIWhbaEDvmQJkDOYmZcorHbnqoV5U2n9AqMseZ+cMOdv+P/L/AB6gKJHjAX70GKXlRzPhOXYwJ8ZtlgMcUlrYMqaYx6m6kwdyS5OyNDIyPJVzmheV6s+Gvw6Bmaht4yRDrawTXJfIIQCOvMQqP5HVBKfP+NLCYNBkmizjEpCl3MRDIhyx2ER25zXPSUNW/FfX8OlVtWlT7zYzBMGO7ELcaoqpqQSUg458IpaOgS1s6JIGI5FZ32q4Tnlg+4AjnkG5qbRNe/czVddddOhSuaqEXXuCaZDLrMW59OKtq8bJVSZVCVL8UgJaEdJOZ5QHOf1KOySiQsNZDZDUiR2qTsNIZbAUeOrir8w9XyNdVTRWov4oiKa29FS+1JGaZZxT7eqE0u6aumW2tS0rzSJjjkSR7o5Fvl2RtgS4NeqVnsYAglh2EGBMkPOGdKgzxR5j4XuI5ANEzRFX09enVlmoaXrcxTLhApUyUz3ELCp8QPnEFxrHZ0icIx3RglIV7nnkodwmkeqlapCtGV/zIvxRqon5dLCqYlJXZDRqd1BJadlPOWA6Tjlzhs3+mAJJxLm3ysgmn18kd3xfh1k0USzWA5rKnJ5McxEkzQQ9Cf8Ac2oxrXLqm7XT8WGvTIzmIUeX/Mnt/hDoYrhK+HHkBfe2pTh0E6NfwrP2L1AFXDkBKrid0ZB7RNR7lcrl1RNOh7R/l7TCpxSW3AyCFrP5cflAjc3+bGP8VZHCwKlkxcj5FvBHDIxK4mQBAANV7IffNO2XAbPRV1HHXajmfMqtdonTZV3FpollIV5qcDgJe/4RJW1fSqr3MlNW/Upap1iekkggcvCR7YD7OvuaZhxO4T+UcNwS7FHhLMraeFCFUTUYbSFHeCU7fDRrjbQmcQTkTXVE26uRZtpuouVclDpHk6gMc84LL56XWnbdierKd3zCicyomcwJ4cx2RQGQ/fAsaLDw2NTEwCXlKSpUR1NXSJjZ31JkGIkakq4sKVFjWEoM4oxlKv6Llbrquqp1aS3+nu2E0lPVuhXmHxS6usRUC5X26O1r9LR6Q2gnxYcZcAYDjJ/PjlDlHIA5lyjIbbjyanJIvWU54MH6JBrCmh1sa7HFgx11ZHYQiKJjkaFiqvUXervpZSO25y+WUAKaKfGTPEgHABQicvSD1Pbs7qLLdS4VuA+AJKcJnElST7I+rkHkaJFxigmpQwGku0fZQJCWQfYtiRY6SRHPKQu327t6fOiK1V9F0XqpbttrG3VMlPfE+cvdFp292ULjIfCyEHnp/wC6M3uceUMhzo8yqxmQOtwmsRsjIsx10iqAQXEI2u26LPfKeqMExFRjkVHO/LqS9rWJTVL9VWFsI5EmfZL4xEO9N3GsqTSUCiV88JdoJPsihsAzR9FCyrkOSt7UR8PxkycVBcRWFPk9qdYH7kyE/q8ZkAbugGqIzaNWo3T42b9ILSJrqHpadU055d6XslFdd411UWyH1AnjLrgouP8AOsrvyYbzPd5vc4tjfHmSUlhi0MMVx/3FlFEYdhDso0etIOZOsTThikNRGdqO5qOK9rVVerKvbYprxbxUV1QpLCwe4nxCUxkZZynnxiHWNwuIeNIylQcBzIEscc/v5Q+51S+JsjMD7lkWHnuM8eZPIjRiSses5dXq53uojxzhrYMm93VFXtPhuRGPb/mVdfTRYYtVep5cmTqg2e/bWv7VaVK6P8ITm+4LRPW8hzoMVpJ1XyrR2II8AD1RYV7XwJWgIkZSvRilrFG5URWp66qia9FdueTRuOv1pDaFYzMPdZT1F7oKKmtaS9UIQApKcwfvlFTUPAd/Y3KJPmmqKSW9obKS1Gd08EJe7GQENytMd710cm5wmpr8V9U6aqy/UKVqXTlLkuPw+3OJ22RsDfLtMH6d1yjpCNJSfzYEqwVxBA+6OxyV4P4plQq+fjPJJKW0qmFfFJkuKV1hHerlG9AuYK/RyKM7Gva5zX+qf2t1NvhylWoKRJBlxzzhfXf7dLhcqp2vqbmnz3JZjr/mij8n8Hsxsriun1mTYnbwHNnxrdFfPgHlGnj3vlsHFFLCx7ZiK/Ryquq6Kq9LmfUNtJJqO6nnP34QIOf7X9zVlT9PSVpUJE4AcP8AVEFrvCvleibuRlZMmj/kdHt0kBX5drjPDJSKxV09UbtRfw6Uj1Ft6yEF1AScOPygZuf+3b1Dtqiyylx9GUgE49A7/GN/PsA4bmfDXkZyjZ8nMFj+PW/DNlENZOq41hFgyoOX4xKryz5Yne3gAlCU7WFJsZu+XduVqL529sV/9m4HB0T+MAdT6U7ro3vp6qleQ9yOmfL80NQr5P4Vra02CQo1hk8ivkJV30jF3Dqp+SOc76cppcQwZA4Ipaor1a7dtRdVROkBeqPyqg9Z9H1W99FYvUtoAEnDD2xhhV+I3DnKHLHK3IHI3J1n5DZvTWuQHmFDIPjVXhGQMa+fLDBPSW7LRZcJ50Gwqq5UHomiqnWiW2n1nVLzeIg9cRV2uhQbQgqQBjpkJZwrB5F+SvI6WeZ4vPurvIaaqyu/xCOO+nGtVFDx/I5TkQdlMeMzksoVWJ/+V36z2tTaqopVYGRR1KSe6qYMRtvfdD7tpXRaiXCDMY4EgjGI/wAYcuU1jmGAFrqeD2H3cibeR58pYXt454ynlpBsUGdtaJ7goIbNjlQysf8ABF1sLbry4pllhSszIDnhFU6mlQ2iorVYBAmsy8IJwn98Gzk1HPyzG5cOiLJ49t53uiuBAlkMOGj4H0+HDsyDSMs4Y4TEQjhoi79XIiq5UWXajbarxt5ymdbJKwJDDGRnEXI3I/b7s1XsK/8AHmZK4fOBursGzTG8lgy8ypspyWkgEr4ke2x2I2+g2oGbHSgyIsuwjmr2Odq5UaFiaoiqqr1X+u9In2atx9ylUGxqM5Jyx6Ym2k9UjUUSKRuoHnKATKapzMhygoOSs9w4uFR2YfCLcuhlYBKhsQkaFGs97GHdkkhB+0E2mcuihAQqqRGjXb69NdH6bO3F/wAtYU1Rc8Je+NazeIt6ZIcDtw/LjP3RStJUzOdcwqcBm2jaQtbD77oYIrnxjSoYXmFaZIwkgIrA0Jy6iGjUYN3o1NOpe2zbmLdUIorWfMS2AlRHNIkZ9hgWutzqa1rzakFOrEz6cYMepyjC+ApQeP8AIKKBydPSPAnkyK+9lFkQAS2uR9dFEaNPIMaxYrntRHMTV+i+mmklv1ibfWFt52SEgd08JgHqhipbcy4kVSMVK49R/hD92qfmn9qdU7iYoVS5v+4TA5MwVuHY5SlcwcmHMHkVu4sVUeB7mbQUgWENo9ptFUrxrt1+X8UrfR1SqEzRiYt3tv0fRROBVyEx0z59QgJqrAOTecMhEDGcWsb6WY0KFJsY9eKPXwu0jnPfPnmaMLRRYju8u17niG1yq3TpZUuXG4omCdComS0UWy9uKSFNJLzYkcs+2LisPEmtxW4rafkfl2DTz5TYhJUPFcUv8pl10KWqoCbMLMbQV6C19EUZzblTVE0XpPQbVoapei5PqbfJyCpd3CR9/ZBRX73rRTeZtdlBoQJHCZ1jPKfDTBN0f2/+BjADLsOVuQLMElgXx5oZmBY8yaKQMqhlxa8z8llkjKUat0eol3fKqoqdFLvp/talbQt2pWorn+LlL5xGTnqHv2qqFoZYA0H8h4z+USSV9uvx7dQVVtA5j5RW7jHSRe4LNqMSIU0JASJnsYlzCta9zJM0UVwhPEGV2jEFvaqaoqN7Zu3kt6re6pdTPImeHGO9LvjfTb2qtbDdNLxBJBn2fGA+5k8TJXD0EOUVPKGO5LTy90WNWGcUF6W1DMnR59NHCwRoJZUEKxHo10n3JkcVe0iN1RC7tJKGlOSwSCfthBBT+qqKRxArz3pjOXxIiu8D5e5TwGrtq7jvNf2x+4CQm3tZYUsG3ob6NVyVkQ49vAlsFLE+LJ1VHx5Md/x1VU9Ok9KTbx3Y536nG5m/3SjISTiPvmennBseLHPPI9te59Gzb9tHs7HHrWdh48EpbeqH9aDUy4LvdAm2twkYUhzwPR6K1rFV7ldpoiPgr1yygIrqWuFsWnXMgHjFDeDvDnKnjtkHLl/y/kUwn/k3Hczxy2oMce7LCYvlyuaeNfZK6AfZAO0gmI1j0RSCcqo/XROsqfLSRUjMwK2ex3csqqHF/wDjqnIY5dkd6vsfGfjSnk0bvFmFEtpNb9Bsc5w6BQ5ZJzAM0gyWJrcGXWlRfU0yxjqV0oYXmGpF1RdPlRxtl4U8C8c0ql8YTUzrVGl2lVSJdKlzmpM5YAQLebcTeAVzx3f1/Hfi5l2I8hNj2EqoymVTvqI1fesOyWJB3Jc2NHkDeMTtQuCZNF02onUrbJ3K0/dEtVOKUylEaeqFNQDZ9atylSypSMCkSJMxAu4faU+R2EomrAWESUQcqokr7eUBRucNVZGeo1PGVV0R40c1fw6vxYrjQ1tI3SokHFJ5jhjzj5wX+31bFGzWon9Ok4iXPARfLYCNjI8DWIxzFaglaxRNXVFVyNVEXVE9PV3w661tsQ4qSx+mc+rjDfQXJxtIUkycGXDEZQJWW4hJzbJpdTgR4kW5xOYSyvAkc0USCtuPuypbguT2rprBmaHVu5VeR7v5l16gLdDim7z+223utxMm1WfNZ/c7l3lRYmG+RPHdA3IKzEgnp82SpkAj3D6KomFs7IM6Ow7HxjGZJltlKJyO2qiojlVETran3Fb6T9KmTKpQNKiOKhgTnznBGm3lDhqKnFhRJSOgmY9hiZw52QeVdvfZLdfSOP8AHkO2JX3Nlj0IA5UuOJAf7KFHkIVI41RdCvcMbXKquXT16WU9Uq71HnPHFR92EaO+SHD9Pg1wHv8AbD++wf8A9nVcokOPP8nSIuIz640wMfLK0stEs62HarVF7Gx6pMbOjBOYYRq1HERiI5RbtFReoLsNuOsTxi/u7NyXC3tFRBH+EbY8X+bfHFXhuM1eJ4he5nksmri5W+IyqYLuyLYrIDMaUkgZqqYE8ALgseF8lY6uQkh6E2ic+16PLCkgSkYF7ZcaOvSmrqFfrOCas8zOCobyZxODAqTHMwqQzc9yS5fjcKqx7Hrm1lzHxHvDe2kyPSTJcSopwSlk7TzXRh7mOQTdNrVQsKFNQGtVkXCnsAMO3mNJuQdplfohoZc9RgKeYudfDTh6fg+EZW+Xb5pPslp8Ypsdt8gxwWH1sw5kiWVwavlSrNmN2EuQM4ANTvCYMiN2NerenyibqX6dNTTpmF9XDLPrhlud7KqvQ3UpY0nHHxf4fGI7xvz5iGYVeTYxyBxyfHKqty0s6FNsb9kmfew4L/aR7armV02RkOPY2x0BXoQyxmqMjnKjN7k6VimuSsNB9kKxeGxTaqisS60MSmfLj90Cn5Y+dfG91juOcF4/ZUV3FqclDkzv2pHjspscngHJr0gtnyzHW50gyGscdrldvRVZ6adZdr66mpnGHAdJQQeoiI5vtdatw1SWUKEkqE5dY6Byi1ft3+KVd5vZ/mkWTyHHwrD8CxRM2zWVo19lIgGnjhhiUgTuZDfKlzzK1XnVwwt+bY9dG9MtHbf3ATlBFvjdJtFLb7ZbDM6UDDoBHLogzcvw5vD9gXHvGXHgY7cspL5sjLS5OG8yPIoVcg4liK6NYRIbYMF8lj0CGFXgYvq07ddnWSkJOmWUEtssjrqQpc5kT7cecUVgGVZPS8qWdVlXINdjKchVIBsv7xO9HpcrjNZKkQzvBHj2EkNnHiKLduVyORHp66p03V3gMs4fxQKozqyAg1sG4KzgN1V3mT5lxxkeFyHTT3IwGdkDpjkcjIYRDn4/WdkshmhWm7zng0cxUejl6aaSrDCSlWZMeeSK5vzABhh8Y7HOnhjwzzXjMypxya/jvJJEeYGHd46iJBa+X2u8+ZUMc2McZWhRpFbsLovyuTom27uOltV1bqaggNz+3CI83Rtj94trtJKa5d0dPbCynOHiXzdwLy8LHc/xi6khiK6wxPkPGxyfpFjSCkbnHDJiDMJRoqbTxpOhWP1REcnzdXRsm99mXQMVX1SGalMu9NQlw5cYohuT0z9RLeX6Wno3XqBRnpATjLEfi4RJanOapfqrnWSyj1UEj5FaXX3qOjgI9XLEYNCt9w9mn8ujlXTqwSb3TuWIuUNwbW2GiZA593piuN+s15t91aprlbnG1qeSJkZTUBPDlES4fDgcC9ya8HfADk19jRssyOmv8jHXNfJ95BsZNSIQoj5IZAAyTDCNyeiIzd8F6rwuvFxumoc4sHbLebda8emAlGeuxu4zDkSuZS0lrPt7Ct43PemsrA8Wt96VJmSBG1jhkSeNytAVQO9UXRET1QdQJVbvPzVe8w4oM0A9EFHB4u5oz3HsYx+ZzAHHy2dUuQDPOA+l+o4wIiKn0aOMbrG5mkI54gqBNHkbtI4bNH9P6aNbiQ4J4iEg/uj93wj0XtX/APs/s/w6g2DuPN/sZsivFsR5kezY1rmuVpm7dUGxj/VWP9dETX+HUVpYcNwS3Q9xMxgnCLu1D7dbblLvLkzI4rMz7Y0D8Pft9+S3KtpjvKOW2OKcE8LAsI1gDOOXSz66+nNgSGTmNwfE6MgcjyQiIPcxzyADquvo3VejJnaz9YqdTOR4nj2iK47w3hZ7VUqpbbUPpdQoiTeWEspGNbuTs84/4KLjdHxTj3J3kTcRap1eS+zq6ucboJD/AJ3yZwuOsTnSJJobSImyTZ2aEaxURy6InT0vadot9Joq1trZnq0EgicpTlLojFl3LuW7MAW5yq0EyKiVTnhhPqIj58M5W5ftL6Hmec8ScO4VDtggKGfA4/wKytpMKIT2UZsiwCGfclYITnbHSZCv+X0cuvTQ7umyWxAo6JhpKBPwpHwPXD2r093Ne323q595eZGpSjKcp8OqNGJf3MfBfjJmI8Z8h8ZTM65EyMwaOQPE+H8YvQRCzAONES29vIHNiRijK5zVe3aiKqKuqL0ha3SFP6lDSzLlh74H9xbF3TYHPPonXlDAaQpWSpjLTGc3kDxR4z+UGZ21bb+I3HGHYuRsuS3kamp6rjbJKYAwrIizVl004Uwhyuam5pE2KvxXb69dXLxbKs+U4lCirDEA54c4S23aG6W0m6LU8hDY1qE1CYTiQcOIEZ0cEVdt4T8xA5J4NzivzDAMkwnLAUlhYS4NvHOYZIUseLWMJimr8gbJjw3McErd7JIXo1HOairwpqB+kqPqWjppuQwEINybtddQluR+tb8KvxCXIznBr1vld4/eSRhR8xxex4ryGMdYwrzhi0kUbrdvuyTZIrfHEhWwKpPekUqihypQCuKvyi1ci7obprgrT5SG+mUoeLV6s7+s5C30F9GGZJw7RE/5X4S4V5nizbN1xk0U8hta+pL2J1RJqywCHQsmIeNS1ipItY0hRHepNdNe2rV+LO/akprVsyC2gesZCCir9cK6spAX21N1ZHeSBgD/AMUB/wCRHOMjxuxBXx7e1Nj1ejnR2BmMj74NZHGwomOFJlsUYFagVX3hjFe9e6g3LsRFeNpLdZTVU0m2x3TLDHOeUKNp+siqdLiLghS0lc5KE8JDmqKh4f8AuL5XlsrApsCis6v90XLq4NJMkOBahUcggWnekhqjeAzAKRio9dzV/j05/wD5BV1u3/3d5cmkjUDM4zA/l+MLXPXy2L3FTW2npUqLzhSQEDDA/wA3wg2slva3lrI54+Q0lZBCxuRHK6vRgTvL34LTRxNUjmDixoyq9RojVQjkVV9UXqO7U7SMrVSOhK2UHGeIwy9sWyqLO7VUbNcwssKWAdIww4jLlnAHeVHiZSZdQi5M4stpFNlVXKFDbFkQmseaNMJ/t62ZIqhh91DIQaorCNerV9UVPj1Ldm3e9bmglh5aadIHdBwIHCU8pRC/qj6YW6/0ocZZZVWgeKQ1T5zkTOcYf8z45kHG+WzL3M5i4fyBdVx/bQkA6VU3j2R3wpv02z7ojejxtcrChR7HaLqqJr1Jth3TT3hXm06/JcPEHSfeYp7unZd62+jynGVOI5SJGXVBEcWR5h7sLI3ED78eB4lCDElXMgT7Ksslie7dHrY1hJWJKV5EVBaIqieq6J1I1pUzTMLfeaS+4TOZGok4xGrwfpO87NI5cB0fdlFycV+QmdZ3eXWJTeNpdRRAfNtollq6ys6iwis/7gKwniYM0UJYYWoohkazcmjmu6d7Zd1V0m00oQicvDLjCdFQXP1Wxnx6o9CnY38v+a/39V8iRYTbD4+UXixC5A5H5oqMIvM/wPJp2O4ZRsKOTjku/rpJ44bqQN0g0S1ij7LJiOUPaZHIxHJvcidMlFaWrKj6y6z+sTlIyEx0YxLW799tborBa7OopoFmRl4pHDOQ58ouLwozjmfyltMy5duoV3mNZEcWqq8kizptfS1k+AdWnoKqCeoljSuBWjVrViNAFXFRm9y/IvHcG+ls0TbdMEpVKWUa7Y2pSpr0isZS4wg4KUJk55njF4c7XPMFHTWgato4dLPMki0DQshxZZmghkICNLDZ2FUl6x6iTYJqoilTV2qeiR5WXK4XEhwrXpKZS7fnFlqJuy0zSEUzDSCAMh7Y+2nx7CrPjvGT53n5+PqWxBDO2rjOuY9qkyTFdY29pKBFNc3trZnmnUTBxUa0Tv0hKjdNW6nollzUZnnPGFldX1TDQdpggaJy7vVA48W8XAyDyByOfxY7KQ4Y+JdY2B15Jl3ZcuDSyq1W31VTyqzHs9xgkmZLOR4ZoJEFGE2pPZt0I9V3lC3+WMHNQhsoLhWVjn1lUhtTgmMU4dk84LvlvjC1ncc5jjGU8m22CQbzHLHH3yMcpFu7eJBuQNr32cZY1jQAq3DRyIzuFmMY5dXoTVUVFTUg0h9oqL4xSCcJjKY5QrrrlTu0rtLUJbS24hSTISwIkecowl5F4gh8T+Nmd8VByfLC1knl5abiq4tzNj5FJhYfPca0yuvbAK10OAkhiMaRF1eZFci6qusrioB28PPl9XIe6KnP7XXcd3FloA0Wo9ctXOcZ81GbeUPHk4ha82C8hQHO9wn7xxhs2dJK17V2Pl1hochhn/iRqo52nqqr69MTlal1ICgE9WEO1RsOua/tlOE9JnF9V33CvNfH60sOFgvD2HjSKZXW48Lu7SeOMMb0I4JLzJbIMXsDRVR7UaqKnw66MvUoloJLvGZhE5s5+mb8+sbJVxMAXyp5Dc8eR8uwZnefTsg99LhrKknEKPAraepdvFDq6yKwMeIGbJcjydtu9yM1e53T5RUtbd3kUB0/RlQOAkZnDOfLogMvbtPb0k06Uhek5wYXB9rnlje4/bjGKxtaVY9jVG7RQgUkWGRr6mK0Cqo5hGiY+NuXY5XOYq6uRUsS9se612xv2O2GSygaeJnIZmYnh1RE9l3rb9u7zprveEpVRpdOrhLPLAy7I2N475jnxb4eVLTDPT5PDrfq1ccqiGyQ1jhteNS/rskxjMMio70RFVOvnTfrLePT28O2HcSSqsdVNKgJAS73NXDpj7B7f3VtT1HsDO4dqqWaZlElN68ST3cJpTkccjBM5PncI1fBrMTaX6nkljHIUE9Gjq6eLW6mbIYKO5DyTuUjl9dEVUTpM1uBbY0JBkY509ufU8pVUw6G8dJJEhyJwxHOKt58wTHsrp8DoLqmx60a2xdbTciNj1ElvEhDhkSb7eeSO6SKNIfJ+ZpCOa5UTd6a9FNn3TVUw/R7o6oGb5tNF08YaUeofOAbwHh+u4My4MG8be55xpm2NCnUN2UzYNpU5TBfMhwkvGifaPBDso6he5gnNdoxyI1FRUSxfp/6qU9IQzfQlVNhkJHI9B6IqX6mehNe2hyptX9RaiqRMxiQcBMYYxY3MNrjnGeIS8+wfEa2HWS5HYs6fD5f1ANbNYV0OVKt74seMZ7J0hjlI7st7Lnta7/MqWYF2pblZWbxtcNCjdmUhQ1KwUoKx7v4geEVOXR1W269VjvqT9S0ZEjAd6Shz4HnDz30p35f60/u6qvEmx58PlK7mXN5GKi5RHkGL0Nk+7PKLlOzH1gGeyDMkRbNMhkQGRZ5EEwB1MqOEqt3abukm4vMfqPNqu4ifhVh7/lDxtJNA3WJaRpUuY72oHiI1Y4nscv4h4rgW2G47y5yxxzX47CHAHxtZQ8ihTY01iRpQafHsVkLFKaOdGbDxe+UW9hd25qqyPboigdeHmaENg4TUMe2LN0H0SaNtsGbwQJkCc+yIpxxmvIuR8s0t/BxjyBx6mmWVlImYzzzUZIHHKGvdVzwxQYtGHj9aefbmlFf2m3U6WIAt5E3bfTo63RKZS4wUJlhpmCcOOHPLLhCyjKzXBGpflSBnoMs8oLKJNyyFmNfkNcOjmU0aLJFaYZLp6yUeY5ZMczCzbOLPhSHR++FrdIyBVrn6Oc7VuiumpqUsFZcb1nhMTHtgsqq4oY8hLLikcVaSB7vjFkePEmgNGyLITTJFbytlGQgTLsfwaijSJ9HVhkT0x0ZYeMwrSfj9W2GshkV8t73GVCPV7nb16GapouVHlKWEszzOA7ZyhPT1Ap6JSmm1OHkmZPsBibeT/JHDvF/G1tdAwvM+Q7/AOiyvquPY7iE/J87uf8AdkbHgWJoojAx6gbJUZDTbRoUTbtGwmrUU1oLLbqdhFSK1hxaZHQCmZlw8Rz6oiHcN7vVU85StUVS0hc0+YULkmYlq8IEhnnCmnNHIfkFy1yizL+VcEs+NaGdFEDjfDbCit8RqaDCojWChgxgFuOM22Aqo9Ty9NZMhXu1VNNO9U447iElDXI5fCG/atI/R1ZcU+H6jkB3uHCZMSHGauIccBBzqyLYy2DWa6fLgOWI3ukHHC7ZJUbSDRNznpqj0VNF6Ez508MpxLFA0NU3Fj7xFGeU6WNHhx6PGUddTbJXPt7ilfHlV0OtC5Vex00TlZCU70dvRyp6dK7VRl6qUt15KMcj/Exx3tW0TFlS20ypbunEpE+J5CMroJMnZIG+oinQ0EvvGjAWOZkxiKIRYjVEZyHV0VXO2p66arpp1K23WlB4VDSxqQsDQMSrIzGP3RTfdCPM1HUUgp44S7Y3D8L0xmzZTHyr3tckWZWzJsTtwhvsIaB1NBjkkzobgS3PciJt1d6fh1bDbdVdHraA2hxt0IEiUKzkOrhFatz0tKhwGocStsrOGoCXaTGsV1V8VWmBU9HUArRTIuX2djYZTJynE6QhqOSecVKm6gyJjJM1YbpAmr7dwi/oom5yqutffU3ae0L2+tG57pb6S+nFKn3WW1CWJGlxYUSQJZ8Yst6H7s9S9upb/wDjbddK20TkoMU9Q+jE5zaSU4HGOFj2Y0FY+TTz+PMmNawwI6uvaQlXdhlwGQ4yESE6bdx68pjGV6aHGitGm5qqunVRb3bLLaq001K9TVTIVILbcbUDjn3Soe2L3M7j9Ur1akqfo6yjJSNQdpn0EAyn4gmOzl3IYregyOD+wMrpiExa7ZT2FpRBZHYyT22xzHfIsNzWRVY1SsCnZT5dr9HadZpX7e34WgrqI+Uc6QbrZ8S3F/6VfOOTS2ODpVTnWeOZEUU6miU8Z9jYVwwQK6vl2hoNhHESyE9rL2zc9CncP9JyNahBr8nT7QvUZVNxvu8pgfCGi8HdS0n6gqDc8JpUPfED8h77FanjbkTI+J8EjnyIz66iPTJbwrargVpJtQsy3s6KHkVgaWxADmCky0KrU7iPUbGMR7p69O7xuOnAZpqSqXYhPyyEOFGfeksJ0nvapyyPSDFZvVazbdqKYVNTVUjd+zWkuNhcwDpmkq1ZaZT6Idm2/wAf9f8A8umaAWP/2Q=="

/***/ }),

/***/ 146:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/jq03.jpg ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkExNEY3OTM1MUEyNjExRUE4NDEwODBBOUZDODk1RTQxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkExNEY3OTM2MUEyNjExRUE4NDEwODBBOUZDODk1RTQxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTE0Rjc5MzMxQTI2MTFFQTg0MTA4MEE5RkM4OTVFNDEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QTE0Rjc5MzQxQTI2MTFFQTg0MTA4MEE5RkM4OTVFNDEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABXAIEDAREAAhEBAxEB/8QAqQAAAgMBAAMBAAAAAAAAAAAABgcEBQgJAAMKAgEAAgMBAQEAAAAAAAAAAAAAAAQBAwUCBgcQAAEDAgQEAgYHBgUFAQAAAAECAwQRBQAhEgYxQRMHUSJhcZEyFAihsdFCUiMVgcFikiQWclOTJQnhQ2OUVhcRAAIBAwEGAwYFAwUAAAAAAAABAhEDBCExQVFxEhNhMgWRscEiFAbwgeFiM3JzJEKSIzQV/9oADAMBAAIRAxEAPwD5y7ZcnW9KtRTWgJrx505Y0TNGZaNwSIyCoPqIKU+XhnXMg8sABrD3U+4pJU+UHRkCTVXM8xiVQbx/K+YWwt1SEhGqQRVR4kjPwGfhijI8q5l4XQNzOuOeZzJVKq1eApz4cMKAM6wXkuOpPU6nuio/ZUUzGVcAGnNlPxAttx+TEZQBqUqRJYaS3oBWSsrWAAUA4AOofy+bPh3B+2OShGUw860tlxbiei+2KUU0cw+haTUFFQRgA+iv5dOw/amftaNdJFot9znBKUqZSEBpBVmVEoIkE+OenABpK5di+1NyjNxZOzLPpRrCFpjlKmwWyAQoK0o0jxywAcdvnb7UbB2XIl/ob9udq31DHadZUtBUlSekpKVGpRWtK5UwAfOv3tuoiyp7DDTDbf5ignUMy2mlaJSR5gr6MAHOjd1+lLfcCY7Sq6yTXh6aUwAJq4zHkudVWSic0JNEgq8MuAGAAUmSZCyQAdIGrI+vy0pSgwAXFhtDr/QnXBspSHEqYZP3q5hS6+BHCmABp+b/AC2v9Nv7MAGAYjmjSlalcRzJHPGkR0x4IMYj+lACSaqHlBNE/d4+HA4KInpXBBNGnlCQFaSRQVHmPAVz5HAFEthetXhTZ4AilANNQPUDUDA0ntJJzO55jS0hmiaHKoFK0zqOeI6Y8EQFFu3LuBagBLbYaJ+7kqoyJyp4D2YOmPBEh3brbN3D0YkzcFw0yHEJ0JcUErVRZ0upCkktLQlSVUz8wODpjwQHU/Y3fKf202X2+s8u7mQuBs6xa7olZ/qdLFEoRpoA4wjS0skCqkHCNzzvmQbT7Vf8l249nuR27fuJ9pht1gqCXhpOlSdWrUqtPRi+wk4areK321PR7jqM7/zBdqO49ksOwrjZb7CuFxYSxuPcNqvphMQY0GM49dbo8uM0681bPh2lLfWBVtupGeOb8VFKm9k2JNtp8D59O6Hz67i3qyuTOmPuPzAZDyTJK1odkOB9xoalEhLJUUgckimK7STuJMZMI7y71yr4t5TxUorJzcOquonKoI9mHHGNHoiRDz9wpmOPOlXmWBkRlz93PKtcZ5AMKeakOqSo1KlFRyrTnWpIwAFVk2qmc0bg+EmE2T00E6FSFgip4V0JPDABbNw23FrUipQl0gDTQJA4AAVAA/fgAs/hmvFf8p+3ABzyaBCUKXkrUAScuR/ZjTJCSKtZpzSB5SPCg5jjniNRS5dnGbS2FvHU5pVpqRXOgry9RwItszlNNy4l0y7rJqsGg8U/uxJcS281p8wGfvch44AGDt2yyrxJbjxX2S6oZBa0tpJqTQEkcScAG4tm/J/3ruFni7ihWm2uW6QErguC829Mh0t+dZaSqQlSCQBkRUjLCXfuEaj52zs6D3C7Bbj7Wqs8eH337U7qnSbQlpClXDdNsuxVOetTjhXpeT0j/TAeXqJVp4jFTdW29rA5zzb7dIst2C01KFxakuRDCbQ8p9M1t9UYxFspBc+I+JT09AGsnIDDVn5bfU/LUVv6zUV52My1yNy7Isu/Iff6LvH5dt9o2/Jjbesm6Nv3RVg33ty/RVxX7pabz+RKhkMSG23Ihq9+cVDypUMT3cbIfbh5o67TuxYvW253fI1pzM5q3UmY11o76HEvpS4y62ToU0qikLQKmgW3glb6Y9Vrz7hSWTd+oUE106lNIvCyoJU9/FmpVaknhXMjLFX+U9uzkX965xPV+rf+RHtVi3sW/Ejv3OJYWZL9xnJWVqMRCgpzTwKa5pKuPs5YVmlGTS2JjUG3FN7R82u8MzrTNiRGjHRbXmYxWpBShzqgkONkgVQBkeWOTolRkNMtLLdAtatRFdRrzoD6sAHmuT+E/wAowAYRVDaRm4wkpOQ5Z/Tyxpkk+Nb460FadTZSKgA1SOApTIc8G4XlY6pOVT9OMOspq0oqTXMe7nT0HkBii5d7bpStUWW4dtU21PUhanMgemQKkjM+rlljj6h8DslNrkApQKqFclVIrz4VI44j6l8ACCDOuDTiOgSyps6gtClBXDgKEAAD6cT9Q6bANZ9rPmY7ldv9hdw+3kJm0Xa09x4NlhSrneIjs+/bbTZZnxsd/atwW9qs777ilJeUjNxCgDwGFgAS595N37ARP3jbrncYlxmQbTEfu/xjqXoEmFNaXbZpdK6+TqrQpS+AywFdufXddqlKPadA7HvrtH3B7b9u+7O79qwtpd54O5YE7am6tsWm6Tdv9wdxxJTchyLuyzW+M9K1vBsPLltNaGqFZIFcE8zHt2JYs3S/LVF0/T8l5Cv211WEtX4mHv8Ako378wnf7uG1tTuvd2n95XRhmY3bwL3Mtu1NgW9p+S1MtMuYww6zerk4QtpxtLjKmtRUrIDGb6bYvY2RcvzfVbnGi9qZqZ2Xau48LMIqNyL19jRm5i3qt8RmGw0GW4bLUZpAVq0NsJS0kavvK0pzPM1xv2sj/kVY1X6Hm543TLvdWq+OhGdD+oakajQZk+k+jDjvxap0I4dWfhCXlOIRoHnNK1OX0YXIHDtOGy202iqNVApxNQVLUn8QrkQOWEbnnfMft+Rch2yNMW1NONobJo2o1bQNYp7iwAAUjljg7KuK9CjMuzJ77cZhhAWtx9SW20pA8ylLcUlIoPbiy3b666lVy526aVqV/wD+m9tf/qrL/wC0j7cW/T+Jx9QuBkZKQttSiAU0yqAaGooaerDJ337fiQ47biCo5aFcADy5Zcs8FQ78PEl5ZAioqDQ54UyPMuR3GamqrYQmQkvvGgIUrSnIe9x4cq4oOi1YZWlZBAofcFeBy9mYwAW8SNqdqolJpQaT7PrwAG1pQpuhrSvgfAE+zAAWs2iBf5EazXSOmVAuciLFmx1JSvrsLfQpbYSogKWo8M+JwbjnHszlkNrfI0Xdd1w9iQH7Ba/hJFkjJ6ymGn3EOW2DBSnW246lceXanE6A0QAddacDXGH6vjRj/nyfzRVPE+heiQlch9E0vmddTmxvP5kbptbvXtXuBtWZ8fd7s9edrXovBMuPbLFcFdJUZu3XF24xnpcRh4tsrUdBb8pTnXB6Hl5WdKcJ07MY1XGtUhH7t9MxPSrFq5b/AJpXGn/tbHlueXarvPevdhchO2m4JZmpFuQW4rDz6Pz2SwtVYrqX1nU2krSngPHHprVifcWz8I8HdvQdtrUGQjqAkAHiM/V/1w6rM3wE+7DxBbclymWW0XCXDjsvzI8Z9+Ol93pMh2O2pxPVcodLahWtaYLtmdnzkd6Hien5ZLxuS+2ncF+3bKjuzbjfYb0KJE6qmLdCct4X8My86dL4KhUqRkThDJtSsqN2fluKq/MdsZFudLca9Rt6Qnr20BNCA22fNUAaQSfVxxRKLjt3qo21TQ599xpN+u11uYlXye7EjzZjMaGt0CK2206oJSGW0tpUE8tYJpzxfj7WK5G4T3wsv/Ma/kH2YZFjRTIGaPuUrT0gimfHEnfRPg/YQ3ippakpqED3agcKDgSKnPAHbnwZ6C85QnVl4kACvrIGFMjzLkM2E4xaapqCatwotUqS/e5cK2Wr4qHGhy3+ooPzZjgZbjqbYbdWFrdUEpzSnOpOKC4Y8VZXRa6AZ0OQGWWVFKBFRxqQcAFzFcQF1KgB48sABNAlhQRSifWa1qDTjzwAEn942nZi41/vkCXdITMiIxGZhoIQzOkPNhmXPBBcct0Mgl0NjUoe7gLcZqN2stFUA949zrEv+9oMZlu9SrneHVXTdNvC4NlchymkgNQoUgRrhHiNBQT+aQStOo1x5312WRNKxZjKSaWxVPeejXrVtu/1RotNpi25dm7Z3CvVuRYt2/pr9v61weltxnJDjC2g4UxkMfERusaoQoOV05c642PQcGePjdUotXGtUeY+6c+eZfUXrbjKq50aNk7cssXb+3odlVcVXBcZAU/KeCGnH5C9BfXobohAW4nUNPDhwrj0NuMlNNrQ8hd8jLltpgghuihxNFE+vn6MaNq1cnOPTFtVXvFKpagRveOhe3b+mhSpVmuWleoJKaxlkqSpZ0DgMzjY9Qw7VNKVoVxuQm/kaZSfK06tGzZCC4FrNxtoWpfTWo0toAIKB001/goMeX9cszhatNRfR0rUZwaXL7jDVp603G8QU/phKTUBgVz+8BmPRTGXd2r+lG7JUeu0wFuxIVdLqCPKbhMPOnmcJVn6ycWY+1imRuAD4Rj8J/mV9uGRYbrHvn/CfrGOjTPzNbGlKqmtK0xAFU5kws+k/VhXI8y5ECP7uLS3tu3AoCw/vHa4z+7/ALowRTC4GiIzusaAgJCEJAp6h9dcAFox7h/xH6hgAIYPlWE8gD6+BwAD/dO3y7vs+6x4UoMqiwXZBbK1IW66w1UNtqbKXEqUeCgagjABy0mb93ruBi4bfamw1F1xEMvusMxrxVrMxnbhH6cqUyNNCHlLJwrdp341NTFynYxJQ4s158suzrpZ7Dc7zfXnxdlTXre2w5RXTgvJbkpcS6vU64l1SCASTQDHo7Gz8jDyMlXpdFatamqUsLKdRB0Gp1U8anw8Ti6WwSu/xv8AL3nmpTC/KagoBoeHEj92N3A3CFzyPk/cDu/Wg9tm8pKSpTlmmENjgvTGdJyqkHSDwJAOGs/RGZ6ft04/EE/lpAi7WfCklCVXC2f9tCU0/TBTSlpxaWwPDGH9wf8ARsf20bv28n9fkf3GbU/VAuE80k6QEcicyU+PDljzNzav6UbtxNXHXiYb3E8v9RuZJ1Vny6A1y/NVizH2sUyNiBDrn8I9pwyKjQYJ1nM+6efpGJJq+JZkamKcSUGlcQFXxB+QyshxII1FZyrkOXHBRPaFWIfvUtTe27QkAlI3jtnqEA5JFzZAP8wwvfSUVTiX4/mfIf0NYaKVkqKCARzNCkEVFfHCw0XCJKFCqSoCtOFPDwOAC9t8geWqlHlXM/d+r7MAEu8SAYchtxtDzTsSW2tCswUuoUAFnIjQkk1wAc3hsmHAYvncC3NBU+27+Vb330FwQ02hxgtNvNgnQvTINa5ebLBRBupuNe9qZE0qvcebN+LLjFlnJo4hQT8UxJKQEppoISDqHKow9gt9ck+HxF76VFTbUcwCyoICqetRCeFcasPOhK7/ABv8byOpxKVBJOrOhIz50Psx6bDS6G/ASex14FJupTS9v3hCljU5bJzaEmqgrVGcCk0/ERw/bijqm9uqEVpsAf5fWhBsamEtIjNrk249JuM7ESP9tATRpxSyNRzHmNcT91xivS8bRJ9lF32828y+669xmuVS+lCUgFNdKSTXzVIoR/Flj59LdyPUQdU34sxpux9P6pcqEj+tkZcPecJB/aDi6xtZRkbgG66fxK+nDIsNxt5wLHm45cB6/D0YAL1txXlBI00zFBSlPbgArn6hazwBJoTwOJRRcnKNyMVsYhe8KwvbkdCClSzuWwaE5FRcaubCxQcyk4XyPKuZrQhBRUoqjaHY0XKN8dGhFch+AV5V44VOyxaUlKSCQDqP1DABa2+QdKTrGXHIcCnLl6cAFJv3cj9lssp1htp1crUw244NRa/KK1OJbFOogBJJA4jAAi+38I3bsvuVlbhuTlxl3y4J1RQwFOxZBkttpYrUUUjy+jB4i85yV5RT+Ui9i90fqO6bjKMVEVm5WGO0Y3Xq6mTDLUjUuOKpaCWUKTlzOHsH+SXGnxGcyEY2oyjtb+Bq9x9ZDiwsAVVQilACaAVpXKtMa1pxU05eUybv8bKBU5aVK/OTSpPL1+GNBZvbg+iVNGJUrpvKx+QxdC5brgl962TGno8ppiQIz7jTqC2SxIpVp1CVGnjjF9P+4rH/AKCs5ceq1wI+llwJOxdhMbA+JRab9IvO3ro+xJtUGU+pyZZjGa6LsSUlyryVAe4KlNOGPSfdOfh5GNZt46pa6FRcFuLcC3DHyJO2qTk9eY6JElaoCi2aKCU1SOISn7xHpNMeQljwVKrcjfpT3mT9yva7jcipQJM6QeXAaKH2k4ts2rab0FsilEgK6qPxD6cMdqAtQakR9fWGrMUOXpqKYUIJ133RZrE0F3KWGlnopajNNuSZTq3iA2ER2ErcOoej6MAE6TJSthtxKVDWhDoCwUqSlxIWhKhyWEnzDkcSiqdtTmpcBB9z1NP22A2VlI/ua1uV51TJQuntTTC+RrFczRtXXNdD3IcTcvW2lQXpqKBORGWVKlVeXhhUuJzD6tBqATqOf7BgAtbfIbSVIzySDWqa8acK+nAAve7kltvaq3HX0NJTPYQ2pZ0qDjyRRKQK1ToqScAFH2XL72z76h2P0USLxdER2K1HQWyEgpOSSl7jllQ8cBVK2pTU67BbdvnY0TuJt6HDZaiKZdnMTmEApLoT1W9DgrUBOjDeJPok34HWVccrcY00T+Bqe6TnGkMqaAUmieqAo5FVAa0rwURhm9fkrbotf1ElBXX0N0qBz14AU8Eq1BBUkkqA8wFSM8jxwmsifUm9Tu3gR618z2or4d5Q/KYTr0K1eSikq1E8agHLFN2NmeT9QoJSW79TZybEMfg/yGpbZK0cVEBKtRz8cqV5YnIuSyJxnJ0Udxkxxkrrvp6ydaUDBE3U0U6lEOJArUVFK55nOtcPq870VNqlFT2DbdTMu63OncriilaTJAB/0j+/F9naxbI/0gN1z+Ee04vFhrRvilyWHS4tDSBpcY6RBdXx6iqDyECuVeeECDyc5GTeo0nSpEhpGkNpQH3ygJUQtmONUl5RzA0pOOHOjpR+wmhUXfc25JclUCx7emtxwkJ/WbtBmxGUBQJcLUSRHaU8sA8TpAwdz9sqcjqMOpVqlzYtL5tCXcm4qb3vQwFtTkvsKlOW+M25JK6paQVrQFOUroSQaek4pvS6ktGtd5fZj0yeqem5j52hcds2m2pg7n2vufc1yb6lLu1fLHBjvorVDcWK8Y0dCQgAanDrrWhIOFZy6IuVG6blqxgtkbu7bzpL7LOx+5ML4FdC8ty3tsvghZpCLqUGakacyjVxGC3LuKtHHnoBZ2u+9uFyW2X7F3GjKc8gkIixpERio99/QtTykA8aJOeOO7+2fsAgbpm9r2LPchczdrh8UhUa1i7WyRDtkOa5HcQ1MluKhkLLYOpQNBQZYYjDqipVSqtjeoAx20t207Ht4QV70nXxciVLkfGWbal0dt0Y1SRFQ9CjPNaRwSahR5gYiUel7nyZDdOIG7Zh7EjdxriuTd4c2a9JfdsL8CHcWrrDlrqqS1KgONlb6w2FJOkFQUc04tsPVum4VyblEtJPXgMqRcdpSkaZFx3JDKq6kr2ffgAQvT5Xv0ss0KhzUMs8c3sh9Lj0XOdNAsx+ZSquW8Wl6h7MeWptO8tywkqOpSoez71I0q1UUHHEwSyDpUMqE0HHFdtdaroueg5F0kn4oGrbau3Ue+wJ6e4G/wCVJiLWDbVbKvwbmaqJFdFuCyhOmoKa8c6ZYj3Dudd7iS6ZR5qhoGFuTaraNTLW9ZWkpAbGyr2y4rIUJMyG06aeknAILYWjN3VNSX4sG7NMLdcQhNxtr8GSnRSpXHeSlYbNfKqlFZ04YatT6YUo3ruRJn/crj67lcA6hxNJb6qqQsVroyzANR4csN2Lmr+WXsF8jYgNqr+P2K+zF/c/bL2Cx//Z"

/***/ }),

/***/ 147:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/jq04.jpg ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkIwQTgyNzY1MUEyNjExRUE4OUYxQzFGNUY1RkJBRjlEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkIwQTgyNzY2MUEyNjExRUE4OUYxQzFGNUY1RkJBRjlEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjBBODI3NjMxQTI2MTFFQTg5RjFDMUY1RjVGQkFGOUQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjBBODI3NjQxQTI2MTFFQTg5RjFDMUY1RjVGQkFGOUQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABXAIEDAREAAhEBAxEB/8QAvAAAAAcBAQEBAAAAAAAAAAAAAwQFBgcICQIBCgABAAEEAwEBAAAAAAAAAAAAAAQDBQYHAAECCAkQAAEDAwQABAIGBgYIBwAAAAECAwQRBQYAIRIHMUETCCIUUWFxMhUJgZFCUiMWocHhYpJTsdFyY5OVFxiCstIk1FVXEQABAgQEAwUDCQYFBQAAAAABAgMAEQQFITESBkFRE2FxIhQHgZGx8KHB4TJSIzMV0UKSUyQWcoJDNAjxYmODJf/aAAwDAQACEQMRAD8ApO2t5BSkpISFeaCN6HzI+jXoiKZhyArcYSkK+IhNCdwkhQOw+waOaSrpgyMo5OceXKzKubBK20uvNtFTayB8Jp4Dy/Ro+kB14842fyzEVyoC4yy242rkNyripI+inhQ6kYKemCcojy0qS4SoEA9kFPlio/CpO/gKpr/5q60NByMcxwuATVR8QPIjy/8AFrrSO33RkFDHA8UqFPrNP101kh2+6MlBNxn1DQJ8NkrANKfbShrreg8Ar3RkEH4igTVClUTWoCqeexoCNbDSzklfuMZCSW0E0IAI8gd/0+eiqVC0kzSod4jRIGcEX0KAUQUKTXZOyjSv1Gu32aODSlnSRhGAg5ETgJCEKR8aAK1B2I2+nfw10aMDESmI6xjn0Io/d/x1/wBJ1xoVyMaj0NpWoo4Ao8qJ+jwoRrNKuRjkqTxInH4wUE1Hwgfskiu2/gSDvrNKuRjYIOUdGClX3UUp41/tI0FVpUdOCvYI3H78O+pOhNB+6r3RvCLku29h9BQ21xVuQqtaUB3pTcb6r2Y4RJO6EH0X4y0FRqgH6CPMin1aeaf8lPdHJwh0RHqtEcfEH9VNFM4OicZhKGjeLap1a1Jb8vp3APnt9R08PYMp5TMA132Ey5mGiza5Mh96NGhSH347K5D6WGy4GWEEVedXTi2g/wB4jSTIOsaYayQM4KBkndFFpqRUAjwNCKEVBB0WSQZE4xrugJ9hIRT0fvVFSrw1mo843MwmKh8UlQWAEivEA7AeVa6dGvy090cwnk1CqjbcU+mg/r0az9j2xuEJ+HzVyaAFTv4/oGlhCL+QgJcNKU8qBRFNvDfwO9NZCbQmuATHBBHpgVFCeVaA+JpxHgNaguATBT5AH+j+rWRkGxAPEcU0NBQ/o+zWxlAK/tHvjpMFVPiSCryPh9nlXWQSz9j2wOxE4cvVANaU/RWvjod/hChgx6DX7g/p/wBeh41FrUzEOGnECgrUIA+qmx866rbSnkIkcJUvi47UCoSomhFPBJrQfbpxo2BXgsk6ENjEjCFKdaaRSnHsUrynj7oWLLjV+vTcmbardIk2+3oC7lNSEtwrc2KK9SbJdUhlkBJrQkmnlqH7i3/sjYU6mufceuoHhbHiBTjjLnMSnDra9ubmuyVuWhDaqQrxKswqWQ7JSiH753b0zAyQYcrtDE7nkKn0w1xbJcWrk0zcHFBpu2vyY/wi4OuqCUtJCiVbaZbV632C4zuVK24HnhpKFpISkAzCkg88jKHd70uvyWi/dFoCT9kJVkeM5dkTJfMjyXp+1NKtVkx+5XGfIiv3lHzrUxyTbGEtzI0O4NvAMsj5riFtVCVKqhW+q6356g3N58VFsLiDnJJI4z4dkSPa+yaNllXm9KpfekfjEMzu54vdMm4dlv2y0Y+7c75Ox2fa8ftH4VYYl/x9CGptutkSMhyMwS1RdOQLiiSNWD6d+qTO4mkWqrEqxMpzz4DOIdvzaVXaP6ukRNknhlL2QOUjzT+gj/Xr0JoQeA90V3CfJYC1DYBFDzpsfOm3no5tKdAwGUCLUdRkYQVwOKjTfzFK8T+jwGlJCOJnmYDMBAJXuFK8U/sim21PGusjCSc4JmIrkqiEnc7mgJ38TXffWRk5ZRz8iFn4hQnag+6f1bayM1K5x6balHlSv7u/66DWZZRmpXOB2rcahVTxINKkk/q8tZGseMDGBsaVrQ08Rv8Ab5ayNgkZRw3bKV5ivhT4ir6fo0O/whdkkzmYH/D2/wDLT/hVoeFonRgVWafun/SNVvEjjt9hKigArSV8SSFAEqB5BKeVACpSQBX6dOVvQApbLkw24gnDAzkYTqSl19hmp/JJ4YH3xnX7gvcbmfd/a2H+z/q6/TMUwG3SWn+yLpbH5MSVenAtr8WQ5KhqSt2Q6FekKFQbZTsCrXz+3HbFOX64X28hTtYxUFqnBPgDWCpqT+8QqZB45Sj0/teno6Flm3WsrTSOoC1zMzrywMsMJYRZ3t32mo9q2LWaBYrDg1syrKGY2QdYZ5MaxOz2jGxj0OLLXOiT8iel3nLJt7kPpMpaf4jBTy+Hcab7DXu1twL9VIKkB4fCJT4Cch7Ie9yW9VIx/RFZUokHUdQlKeGAihR7s9yWVSO0sK7nwS/9rXeZY4d2TkvUV1tduei22Q+uPGNwt8AuCRDmyWwfXYT6qkAmm9dXE1abpdXWzZwgqKSPEkqGXZFcorqS30ziboVapg4HTxitdi93V16ogxMCgYhk+PT38kayTJoV6m3O1QLneGZsd1hxFtkR5JcdWlKkBRQ0aVDiiCNM1ns9z2belXe4hXXEuEhIGYwPKHy43Gl3Da00NBpKZSxxOUuEbM4JnFi7Hxu25BbimOqayHJFsU4n5mC/4LacAJ5IJ8CNex/Tvflm3tbdGoJuxAliAJ4/uifKPO26NtVlhqNSUk0gOOGPDjDnchklafi4kmnh4V2FdWOGnGB0XSC4nAkZTiGqUFkqTgkwVdhLQmrY8xUkVUB5+Y8NbjUAKhKdpUH4f3QE+P6fq1qNRwIABpxNfDcDy/TpRsJKvH9mO20hSpHKPUQObikECnEkClDWnkfLQdTUIaVpQRjzxggsplgMYHatA+IqSrypyp9fhXQ/WeH76PdA3RfPCDIt4SnlxBQmo8Bx+yv1aRN4KDpNK4qXEHPtjJywKDOOxbwoJISAF/d2HxCniPPy12mvL4mgdJX3VYnvjnrNJMlJVPv+qOEwQd00I+kU8iR9P0jXFQ7UNS6pBnlIQohanP8AbgjnOP3yC/q/UND+bVCn9Ry+aJUbbQ2SpIoSKHw+nw2A89Qlts+WNRUfhgDLOJXWdNioFPTq6s+OX7YG4pcWyhsJW8PjIBKlcE8lKISkElYSCaeQFfDQtNuKzVDwbYfBrGElKkSzPfw+eE7pb7kG2KgslLIxCp55cJRib2fimM9H+6TK75Cn5lJ7DvWQzcjhYlbbHKnWm7dXXdC7hIkIuUZCkR2oTbCVOPIdCmnUenx3J15a9SrTUM1boIAQ+dYywmSJZ4xf3p5XJqWUurmFtnTLOeAMSH3r+bB19mfTd36Wd62vueWaayIVv/nWWptWPAMCNcG7TMebmXZpqQBVKA822CBzQsbagNm2s+wlTqnglyQIwB+cKwiwLrfWS+Gi1qSM8ezuh/8A5ZGG3STh+W9kyk3KPDvTFrxOzRpMtiU1Gt9r9adHt6eKA8h63skIqQgGvgNeu/R+gbFrXU1ACnEKwMs8Mc8o88eoFYkXJLjX5MsUT7eJ+qLIe7D2s2T3IYK9ADMe3ZzYEuS8NyEtBx9qelpbn4dcHQPVNquDiQhVSeC6LFCN5turatt3FTqTINOBJxAnPA4ZjOIfZtyVdrqQtM1NEjwzlLHuMY69V9g9jdGXq6RJt0koyrFbkuw33E7ohxbTS4y+L0VslxTMmHJYotp2qSQa68bl+/bD3QhykStloK+yFSHzd/KL3Wmj3LaykBLrhGf/AF7o266l7Ax3t7C7dl2PrRV1DbF0t5LfrW24oRSXFcSOWyF7pPmD9WvdmztzU25LWw9rHnVMpKhOfikJichzjzbua2O2ascQEzR1CAMpYnDM5RJSrcNwGzuKfs+Y+walknEmTg0q+iGNKtSQSILfhCh92oP+yn+ojWEySTGzHKrSpA5KQAN6KVRIJ3IFQlRqabaaKy4qZSdCJ88ZSHE5cBHba0JWNZkCZe04Ae2J0w/oti64jOzjI8ug4rbm0IdtlvkRg9c7mwoEB0NLdjNsIUsUFFLNPLXgX13/AOaFL6ZboVtbb9nTeK9IOo+bVT6SMMvLuz98ejdhehNTuukTX3euNupFyKVdEO6gcv8AUREDZFPwSA6pdqzSFNjsLeZm/iCmYDkGTHSVvtqbQ9IW8OKfgCOTiztx04env/Jav3nahcbjQeQcIno65d+fpI+Ed3j0act91/TqKsNQictfRCPm6h+MVK7F94XTeGvfgke8XG43ll1aJdum2GRb0JTwC232VPOiWtCyocVLabBrUV09j113Q4+tLDY8uFSSdXDh+7D2v0jslM2Ev1o64ElDojPj/qRaDp73M+zDuCJjEW+I7F6vuLNlXDv1zhBrJ7dd74nkpFzeQpMa520A8Un0Y7rYR5+eiqP1hvLlwSuua8Jl4tZA92j6YYK/0qtZbVU0lTrUB9npSnLt1n4RImO9cZpmzKrhgeO3rMcbdnSY1rv9itEyRCuLLKw03JQ4hpSAp1KeTiSQUKqCNeh7bvC2VlE28p4KWRkRl7eM4p96019PVOU5pNCEESVqnqn2Swl3nOHb/wBvvdH/AOdZR/yeT/6dHf3Pav5g+XsjP06u/kn3wfxPHId2ySDbri44zFFZUsgKT6cdlSeaVg0IJ5eFN6H6NUL6y7/q7DbHKWkUPMFJlLEfZPEGLT9K9kM3urRcKsTpEkEknGQI4S5TjNH8xvs/E8Pfh2zHr/Lssu0XJl2PPtc5xmYzNS7xjJaLS0qSlbwSFAihSTUEV15W2Lf7/V3R66IUs1q1alAz0g45eyPQG8rFt4W1qmYCfLpEhlq4cIZXtQyjH+4O7Ope0e3cyxkymcXm4K+bnGRa3rvBZW9Pi2+5PVfiSpdycAQhxLTRUo0IJodSDdm7NxXdX9QlHUbTpGPAEnnzho2hYrFQpMyoJUueXYBFa/zZcG6mwbN8TvuEQYFryO+T3LxccejWxUa0NWhLwEPglmO0Uy1qRV9TgJdRuPp0b6f1tbcahVLVaQlCRPGWJPzxzvijoqKnFTRaip0kDDkJ48ovp0L7h/aT0p09gmMv954jLu9xtrV/usaDHuVynNXS9NNuzYbkWyWmS4x8o8PSSlwFzin4textu36w7doU0dQ6ErUBPKWXEzjzPXWW9XN9bZQC4o+HHD2mLF3/ANx3S9gxv+bLzliYFofQDBTPst7ts+6F1tt5lm1W+6W2FKuDz7TiFJS2knioE008N+o20VuFCKlBWntHD/NAB2FuOX5aP4vqjBD32Zjg2Z9qQexsMs+R45GyaytM3x672tduan3S3H0rdcm1odeDS3oRCFBXFVU115+9Qr1SbovHnKAfhauUjLDhM8jFqbLobjYaTovjxgc5jjxiSvy8e8p2M9oRevbitX8tZ65HtqElS1KZvfP04Mth0pcSl55wLSsE7trT5JGjfS+/qsd7KHFrLa1eEATlMiQ4SwgL1GsgeoRXyQFrGpWIzMzG/iLQ8oCqVeOyjX4gT8KiAKJPGlR5HXssXMVaEvPYKKRwlhHnVTiCSEzkMIENndRuqn6yf6tdB5txCgk4xtIK8o6egKKUshpbi3KICRUCqgRUny41rqMXWrp6Vhx18nplCk4CZ8QllC7NAqtfbZ5OJV/CQYoj7q/cV2B1/dF4Y5BucaEWWW7DclNvuWuTFSkJEdp9tJbbkocO6CRQ76+RfqB6ONM+rFdvR0Fy3vKcV4jMic/3ZYe+Pd+3t4O3Pa1PZVSQWG0gECUyBhjOMw8/7PznCmcaucebS+Tcim5FKcmFC2oscMlpbgZVUVdfXwB/a1OdmWO2OU0zqTTSkJYfN3Ri7hcKJzroSlTw5yiJpnYjOY5jZbzmk5q4sC+RZdzU8hl+U/bw5H9WBQgqcY9GPxoSCQo0IOpmxbkU7xCcWQcOcuERO5OLqFKfdUeqsknsJzjeDvTpb2a5L7aesPcf7fbZbLN2DdcnipyPFI0qbANiat1lkz7w1Jsjd8lRpNvbloZ4PIYHJC+KhWtVrg9RJaTTKVJ2fxhKzKeZ1OJmprHmY369itovHVvtS6Yxy8FcC6OYVa75cYyB8u6zIyJs3z0ZDbgDiZDLFwQhaVboUCny1Kba07S0qVMOEhXbyhoqqxFfVrAbAKJZiWcWu/nT/fq/xtaP69Z98wj0U/cTGDmc3PI7vfJsnE7e0u9lhm1tMxlqbbDiW1LfdWtYBr6z5BJ8Kj6BquPUq4OeZHmV9UTynP4RZXphZ10diWttCpaTwMYTe9b2ce6O/wB3/myXiMe4reu8e5JtUS+tP3WfGbf9QKZYksMxwaCvEukkeem7ZF+tiOowtAaXKU1Yc+YgbcdiubyhWNqUptSp6RwxHD6opVfUZFgrkew5DjuTYK7AlxHgi5QZEAI8OZamJK4ynUKSSlSHVEEV21tynbqluuoUlc1mUjOE6ddRThKC2tOGMwR8RFlbTmPRXdcy6t+4CHkWcy42MWW24zeLTOkm9QU2lDoYgH0JLJpNKkoceUfUCU+J8NNHVuVnX1qMLSSeWB94xh2efpKpgN1ElBPPhwilGf4XD6wyFqdgEefccUua1PW5+8pjO3eyXAK5pgSyyfTWGCB6b3CqgN99WDa6ty/W8i6OdLCRBOkntAmDjxlhEXebomHeo0jVLiMYkW7e6bL8kZx649ky5GUZBjFvNss7MwMot9uCW0RxKDMdCGROXHaQlThTzIQN9tNtFte3JrHFtvLCSZjFWPLjCZuNKg+NPvH7RC1ij/f3ubmtYdiHW93yuFcXkRDMk24ox+1tLIBlzLvIYESK2yPiqkrO33Fal1m2tc6xYwcJ56TDLddz2+lB6Wg9xBjZ72h/ly2LoWfFz/PbpEyzP4iedpgwELRjOKPvIRzdtyHAl+4XIJqkPujigVKEprq/tkenbFA4LnWqT1AJyUceByJ+iKZ3Bu9y6KXTk/hTIHdGl67YDWiWypYqdgaqIpXw8TTVrVV0D6wWkybSAn3RAmqNlU5kapnjAEfHpUpfEoQEpI5KApwrsDTzO2g13UtJOg5wcxQtTIh1MYXELQLyg4/QBB3QlJIoCqlBX7dRK5XtRQoOglHL5CHi20KG6pLksIqn7j+qY+b9f5PZGrem4Xy3ehdrVGBCFvybctTpixHAUlEic2VJqSBqjt4U9NcWHwlolam1AYGc5cO2LYsl2VQrbQoyQVAY4YR86jnXd97avPal1v8AfoOBSMGtci5WnGsj4xG7ixbXDGXZo0mS+llEiLxKlJKStxX3dVPaKJq3UIpxhUAfZ/e92cWFcbiVJ1gjSOMVuhzsTxePKlTZka53d5QRETH9OQz8Q3LDQClD01ePImmnlNPUFI8CgSOIlDXUvtOMAhSZlPONO/ZtkuNWjqC7yrtjU7sXKrnLvc1uK7fDAtWOWeCI02La0QvVCZD9zksAFSU8+IKagaj1zs71ZVpdSSEyA90PFjuFLT0Cm3QNWpRn7o+k7rv3PXPsfB8ayluKi3RLpZ7cY8KJIDiYnoRGosiE65UfxokhhSCAKUANTU6tSy2VxqhTq8Z+EQCsuLaq1zoSAmJw8P8Aqrdf3n/+L/bp3/Sz9w+4wj+oL5iMG+5PzIF+3TO876uuWMCNkkHIZ02yZO+zLecftc8h+E+2ghJdS40U/c5AKCgdxqiL5tK73GtACTpmOMW9Yt3ixW9VCTIqBER9jf521zgvuM37G8ayGClKG1s3uwourJ+NFVBEzk9FSr6UnYnTNUbGvDTqmwMB2xg3iOcSZJ/ND9oXZK0NZv7fsdPzjYTcF2l2bCYbc+8p9mLJW7bzyrQ1Tt4Dw0Avbe4qM66cHTxkeMKJ3NRVQ0VB8f0Q7MHxP2D+56dJxjqPp64t9h3iQ03BukXKYtjetrPouSZzSzBQXpLiW6FiiTUmhI0JVu7kaam+D0mzqxg+hbtLr0lEanZAfGM4809gPaV890uJdQ47f7lLwfKH5zrGST3i45YbZa1qXe41ycFG5Fxt7aSGhT+KBU6szYNZbtwthD4AqQZHDly+mIPvCjrrLWBNMD0FCcbcdIflUe1rp2Sxf7rEndkZG0zRq6ZuoXKFDe8ecKxkCDHVUVCuJNDq8Lft+2YZYd0VhcbjdFTGMXOi4bZMf5W/H7dbbbBQhsIYtkCPCbHEFND6LSC5X+9XVmW6rtdvIJIwiG12RnA6baeRQtuhqQn+8ADuP0acFXNLii4j7CjMd3CIuqh6itc84GTaCVJ+EpNRSo8Nxv8AVTRAr50x7zG2bfpewxiXOt8YxJ595WXynWYxB4piu+mtaviopawa0oNtQDctxuSUo/TplWo6voiZ2y3U8/6j2RKL+OdJx23HUSr+ENpJcrMWpBT5qRvUqHlqrLhX7pemhIVqmOJiX+StbNNrwlMfGKO5v8l/O12FrTIds6VBcBcpZU8tsHijmTvvp3tPmlACvH4h584NuZtqKNC0kTAn7RGI/vY9u/4nn91FskMWy25zZ1XZtsx2w3Hu6VlqUY5I+F3kOah4itdRDcFqNvuv6mcgYf7f/wDRtYI5RjLbuqX5GcQevIDTkW83LIoeMrlPpLj7TsmW3GdkMFQPHk2orqPEaOoav9RIHP6YaSjpnR93CNxML9k2J4FebN17bsszJqC/i0zIL7JaWkPyZZVFgtN2+5cUllpx6RzdQKlLYJ1Ft+VQ262ipynL4xKtpW43qtXQ5hKZ/NF2/ZZb5eJ492F1mqVJuNi67zufa8adualP3Bm3Ti7PdiPurq44hEtSlIKjXiungNTXZN9/ULX1wc5fTET3Lav0i5rY+Xyxi7XzQ/yWv8P9mpl+oD70R6Pm37+9xnXPdfbUjJn7Lbn8Lw9TVktNylQogmSGnUFE+bLkLaWQ1/DCyKn0UgftFWqveL9IdbilFc+cP9EpTjn4pKj24xXvuv2idu32Qi9W7qyw4fZrtjV4yjG2rrPaau15x2ywV3edcmXI5Syh1u3NF0Mq8QRTcjTfT7jYeq1U6zNxJxPbDwqyVKkisSSGl4jPKM27LYLze7vHxy3x5Uu5zpnyNthM8lPSpalEMsNJBopTriClIHidShKmNIfdISzLLtjulo1vsqYSPxtWB4gSyjUj8rPs2++3f3FxLlk+JXNmzT7/AGPDbxdDB5z7FkNylOx7XFKJYSSq4L9RtSWyFIoFK2pqBbttrd4pwqmcKUp1ZHAj2colFhBonktVGLgOE8xF9Pcd7pl4R2Llc7GcQyeLLwjsfKI+P5FCkNuOMXqHbXL5IYcCilpyM1DqmSjdISrfUL2pYXLU/wCcbdICVTkCcZ4RKNwvIrEJaVJSynDsjXH2893t979Kdd9rtMCEvMMfjzZkMOBSY1xQpTE9pP8AuzJQogfQdekqKuCaFM1eMYH2R59r6V79QUkk6OUTGbglf3lhNPAp3J8qHz0caxBzM4bDbir7SZiHBb7hYYiEuyLeq5ySAUqfeLbbfIUWlKUHw4k0r4a7FyIASFqA74LbtSNI8CZS5QmSp0ZUt2S00iJFXX02UrU4hGxNFFX7W/2a6/VHJSDipcpwO7bQleCQO4QcF5iCP99tNR94/CDTc0PnwrT7dcfqH/cffHPknOE8IbtxvSltL4LWElPGlVAHlSmx2G+kXK1ATqnjBDFOsLAcmUcjlEZ3dC5D8eSeanF8WlBO6RTwKiNvHfQgr20rC1EYGcOJbQsaVAFPbFXPdzDsdp6yj5TeIapczGcgsr8WUyOUiKxc57MC4E0+F1tyM9uhWxI0Buqrar7YS2BOD9vLWLgGgSGuU8PdGWd/9v6sM96vWeRwbhEyHAMly+1qTMjKaYdtV9WwiSbbc4JWv0Hnmy2pBSSKJNaahNluDa9LLWDiQAZRJ91UGgoUyAmfLCNdOybctbOL3i3S/RXbC9bVFBpzZuEIAtrUnxSXkFO+22m/1Mo11FkaWvxHWM+8QVsupXRXVaJyc0Az4yIMJftUfedxrMcxlIQiTmGeX2S4SsKSW7QpFnQW11PqoL8d0hXga11INjoRR2RAWAAr6Jw07tf81cyVYke3OUWk/Ex/mN/qGpl5pjkIivTTyEYF/loe0q7Zd3F0re+2sEt956qE6fn0y1Xu4LjtXix47dLOyJyre0ytyXZl3p0D+J8EgoKd0qFaT3tvalaZPlcHPqMTvbe3S84POJMvdxER979O/MqtfZ+RdORbm81DxW4XK02R22rMdiHiV0u9xk/h1Ww2tSPwGSzFKBQJ9Eg1A0z7UpUXFlu9PT1PDUeAn3RKbmtVIg29oAU7eABxMu+MiZd6vmF5W2qCtyJdLFeo13gzkJLb6lx3kyoTwc2WhHAjiQfA11bjrFLVUXTkSJSz7IilNUOUrxWyZLn3xc7rXvS8XTK8SuVisETKux2Xxk9i/E7pLYseP5U2txheQ3aFHaUi6zGGXUemXSACnckahn6c7ShbZUfKZcyfl80HM1hdqS/U/mkCXATh/wCfXD3Fs2wR8569yS6CXbOxG7llNvjsyLLkuXdnS4zTmWGXG5RoNvgw0/KNODlsop213RqspX5NsEPHEAqM8DllieMObzN1eIrAtOhOWA4jvjUf26Zt3N0B0vhHWc/qydKcsDUhMj5NMqYthcpaHVtBLLQQUoUCqoJHxaG/v1q31CqGoQS0DhKc8e2UR9ywredLqpdQxZ60e4TPZi4yp3V2RssvkBZjo/8ActJ5PJD64b/prQw5xSQSagb+GjP7+a4JME0ux6l0jWUy9n7Ye0jubKmeaWsSlAteioof+cKy3w5OEOxor8cmu1eQFdN6/VKibWUFCpgy+WEOX9lqR4dScIbGRd95Cu2vuN4HNnWsx3lS2Wrt6UgqQ2srDS0iO62E03O5HkD4aKZ3751HXp/C3OWI4j2Q11m1wy9oJBMohuP7nJ13OOxYePuMrTcm2n/mr9JS00wlyTKUwpxxhtC0c1pSST5Aa09vtdMAXMZ9kJM7SVUkhsgECJhc9yan3U238GhQpB2U7KvCgwlwfcCHzF9FaVnbdWgKr1JQ2wVFJKR2fVBrGxnVuhLihp+XbHK/cD6bkZ+Xb4MSIFrS+9+LsrbbCE7kJSC4UrV+1SmmpHqaw4sNKbMlGXHj7IPXsFpCCrVgBPOGN2H2hgva2G3zGsiXbmrTco/pPIVc325DpiKMmM5Hjsx1LC/WQn01KIBI1IVb0o1M9AoV04aqbbDdI712JhzvnGWS+y8p6w7Pw/KMhtTFwtbGQrXBiSkcoMxUgMW1d8kym/m3m7s3HaaHqEJCKV4gE6SoaqhpnfNUqVBavFiZw4XWkrqttJcUnwjDD64uJ2Z7vIsazyrO/jqrdcIodkygzc4UuKhhUVaosppSHmHuZUtVUhB4gg1Ol7vdn70wKSokaVJwAEjPvhlpGFUdQaqf9URInhIZYQ5em/czhmN9a4NYRcbHFRFtEJc19y6MoKZVwfVMkJJHNbz7ipNVcqEH7dcsX16gYTTGXSTlh7409ak3F0vH7fHGJY/7o8R/+/t3/E/s0p/dSufzQn/bI5n3/XBjp7I75/0h6MuTGPZSx2NYcau8FFus0SdJnXXB4b14hWq4KaQhT90xl/IZkB1PoAw3VstK5EhOqQvCQtzovBIbOaioAfPFv0LtF0i62k6x+7LEx873uQlZTevcPms7Lok223l28yBLi3tJt1wSw2paUfMx5ZZda9RAJHIAGurn2tT07W3qVplxCkBvAggg9xBiv75WvOV7v4LgTq4gj6IrL2guQvObsox1AJjwGUBx1hClNNRWUtuhK3En0nEpBSaUINQdTu3p008iZicRUvuJWZIUZ/LlEj+2SY5C7Cee/CF3Oc7apjLDLE6DHeisK4+vLj+sXVuPNJpxQlKgr9rbSF2abeZSApLYCySScDhgOAjkVLs/y1aj2H9kXou0XuOfa7gxcpWQQsXnW9xue3McdjQmLOpxIjqmXJ1pUOCrmEFCmwlCV0oPLTdRM2VmoSoFtdUJykpM8sZDOFUv34HUkO+W4jQuXvyjq/497n7TBihjLM6usYNRURod1RdpH8D0x6fzV3bifhjjSR9xTrLfIedd9DljagqlOVvQLh4FxAIPcYV85d/5Tk/8KoU8WyPuC8CPbZ2M9nY9fogUi7XgsOSrdKaYecCVtM3hcWUuJIrUfKqWnbY020I/QbYbHhdp1f8AsR+2HCgqty6vsvfwLiTrW92jPmrZfkZC/JYcebY/E7T2BiTChz29KXLF1s8koA3cbCUq8ttNC6fZ2s6zT6p4/iNx06dxKcJ1OgTy0LhHuMbum2vxlyXst2ccSiNjt1iZIh5SeTrNHEYxBLSFn4auPJ+LatdEs0lgKJ0i2UtTy1oOPsgdbt5QqTwcUv8Awqj1N77CXalsOW3O4txYeJkzb5Y7xIaXyXyUlhiy3RyQ8RuCWgsg+VdY7RWFQHmHWJcJrQPjHTVTexPoJd7ZIUfhBV57shUNxdtauVxlOoSttE6VdrAht5SvgbQnKcafcbcp90OLNPM6QVRbVQNTq6dSOXUbghFRuNSpSeH+RcFC72PbY3zzzd6kqbFJVqhLZu0/5obroqDBhMSG1K2/gupP1jXAZ2Zq8Ip9U8PxW4XLm4s1F3T/AIFwiTczzFxLa5OHZXwT6bzTc3G8oYC3uQAaUtjNlOJWDvx4KqNF9Lb3/h/jTGuvcfuOfwqg/c73Kk2eSq82UNPMvMSuMpu/W9DQcH8ePblTYV7XPc58fVQyhxaD94jx0q2Leo6QEhAyxGUIVLtxWnSQtKe1JhEjo/EJqm7ktUX0YhfS/c7tDjrdZKEKbjFi84HbZikJSVAfGagbHXFRTU3UPScRpkOI/bDeHHmxpWlSzzkYPO4+huEZNgveOiO04hxllqZhkxx2UI6g2hlmbIjRy98sDxDiTXYgeeuW6ShP+4dbnwmQPiY7S/Wf6CFjnIE/CGT89fv8u+f8n61/+fpTydo/mtfxJjfWuX3XP4VR/9k="

/***/ }),

/***/ 148:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/jq05.jpg ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkJCOTg1RUREMUEyNjExRUFBRkREQkFCOTRCNDU3RTIyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkJCOTg1RURFMUEyNjExRUFBRkREQkFCOTRCNDU3RTIyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QkI5ODVFREIxQTI2MTFFQUFGRERCQUI5NEI0NTdFMjIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QkI5ODVFREMxQTI2MTFFQUFGRERCQUI5NEI0NTdFMjIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABXAIEDAREAAhEBAxEB/8QAvQAAAgIDAAMBAAAAAAAAAAAABwgGCQQFCgECAwABAAICAwEBAQAAAAAAAAAAAAQFAwYBAgcIAAkQAAEEAQMCBQICBAoHCQAAAAIBAwQFBgARByESMUETFAgiFTIjUWFCCXGR0VKS0jNTk1WBoWJDlBYXseGjJFTUJSZWEQABAwIDBQUEBggEBwAAAAABAAIDEQQhMQVBURITBmFxIjIUgaGxB5HB0VIjFfDh8UJyMzQ1YlMkJZKi0kMWFwj/2gAMAwEAAhEDEQA/AKw/i5yNmHyP5MxrNss42cevMFqLQYNvAjN1EFwXZ7brqKDzDbIEjRCoo2K7lsm+hOmb+TW9SZeSA8YIzG4qhX+IqMlezSy31jMuTKuQ044y2QNq83IcFSRPpcHYDEkTx6eOvQTHmVgecyFV3+c0W5WXHExckMvxRHZO5Yzhou6/7G/nrINJR3FYf/IduqFEORgF+prXoiKi+qQk8qbELYKR9yD29xbkidOi65vrP91P8B+Ktmh09GP02BLrk1bkN1VIcTZs/ckwG8gnlTdV/NTuTcSVPJPDfQCeM8w71OuG4axByEJna5PisxGpC9UbbcQHCE0RdvrIU676wjahML8bmvQ5hyQXNm2rOgqXkEOgbKzHAjEvxCpCq/UidNtbYoE5nvSL8OZfCseYMY45tiqa6fxNyhyjZXqMVj8GHFpI0Z44djZ38ok99KdiuIRIqIgeSrptaECAY0z+KQ3gPqXGpphs7EYYvO9TCuM15mfiLIO//wDqfFtdKeVnvxGslKMnKJEZ1FMWryWKjHHtT1ATdF211v5Z/LWXrzUSXVFvE9oJIIBr21AK5p1713b9J2TbYkOknaSMRhTDLFffOfkFh+Q4NJp8vN+4j5JWvwLmprHSgMMtON/gUY3b6hubb7pso/p169tPkXoFvIzTXtDnFlSRXZ7e1efW/NnVYQ64cXcgHAfDYpRw/wDIbgynxnDsIK0HG3QZYqccrzmjJSbHaHYSeemusnGCMS7POn3AikIjuS64N84PlVB0lJ6iwewRDxEBw3muHEV135a9b3XVjHc5rwG1zBGwdgTezp0aQyKoidgIPYaEmyh2oviKqK7b7oqdCTXn4eLy49y6vtptUdcYOYQix1Pf6UFN1JPIU23+pdHhwoKnYl7mmpwOa3cDHBabN+2MVHpsyC7Ohv5Guy7dNAXF3ypSwHIBMLe2EkQcaDFZsWBGjuk7Hitq2qp6RuKRKqJ47oq7eelN9OZ+E50Wz4eVuxXknmFMm5HYpKe/YibDtuq+Hj/r0AK7ltD59yx9oX/pw/w1/l1tR25G4b/cub/4pWNri/OPHNQeRXePVd/jc2XZw7CsOJS2krvbB6JWtmCIDiIAKK+CEK6rekwfl+qsdSgqPiEW/wDtrj2H4K9hHKpp4Xo8oRAi7iJwXEUhROpEfaoiq7+eya687UQ81A2Knx4sqtl7mO4P0PNmiLsaIQ/SvRURd180XfUlvPz5eLcFvJ/TnvWoyRhX65pVaVY7frqRjsqb7gnRE38EXXPtZ/up3cB+Ktuh/wBGP02BC46uOwaI66MKpYZWwlTvMBFSIwX+LroFO1i40VWsm/frJYyaye3EmrKb27pjCd7Pb/spuvVF1hYRl4RBmJy1HNpFbZk4XCVsXTU02BuUQoRL+zuCJ+pNHtpQdy1VRvMdriOdfOvMaZLPJqHEZuX1GEZnmWRv/bcePImjjuyaOnajIw2UKxZaCMZOKpGPmu+sWgM4umD/ALZB/wCUH61G6PntfEBjw/Usf5+2szhfnCqZZTux67xmrdx+Iz+VXRItdESExXQGg/LaYYTYhRP07698f/PGrWOqdOw6TMWtLTQnI4nevHPzi6ffHqEd3iXtDqJCc7+R7GB0T95kk+C/b3EllnDcZjTfWkW8s99idAF3jQox7eqZdP2fFddI+Z/zX6a+V1lJb2MjTqxHDQnGhz7ciq58uvl7rfV2qxtkYTY51phXZ3pJ8u+UXNF9Irajl/DqqHjV7kFdNsspxuC9XZIzjMeYy59vxazZVGInt2A7hFP7QxTu66/Mzqz5r3fWPURMz3G3klo7xGlC4129q98W3ywm6R0iIRMDXvY2mABJIAXavxRc4lk/GWA3VNbXlxj1lidG9W2s6OpS5MMK9hthyc4C7OTO0dnS8zRdHW1+LT+3+IfSqM6wMWp/7l4BXFFtmXjVYwj8Nxz1wROxHe/ciVPHZR2TrqF8kr3F7j4ia+1FvwcQynLrh3LTFkMeY+bbcpsnnzQiHfb0+vXv7tvJd9Y4nHdVBS148VkPWjbfY0UtgVEvx+oAgiJtuu/d576xU9iizzWFZzxIBOOAmQqGzjWxKS7777Iq9F/7NfVdlgpoSRJhmtf90sv5pf0NfcR7EZxv7Fy01PyWyHHsdr6KfXBfZDRRr3syq3d/+SrK2c7DegFCJps3YLUUoZI4RHsZOpt01yI9QzufzHCr96am1eY+VX8PcpxmXz25R5FpMYxmkyyVhLBSose2mRRYF6QTbaenGBOwpMtmZ2psXiqrre66j1ghj4p3tDtmH2KGTTbSKEARitFbv8SM8znkrAoxZficyDMjSHo42tjAZakSwaZjtsv2TSyQmMPSAbQxT0VTtVeuup9L3t3PEHSvJcQN24pFdRRNidwtAxTN5C/IqYLtc8rSMOiStlH7l2f3Q0TZz60EjFELp4LoLVjXUTXPhPxVo0OOP0IwGf1IAyL37skehmu+0CymGxLNPrBpANRBlR/u3F6fwaCTfls3BZWPtsw7PIqqI3vEiQmPbnHb9MUaF3Y17d1QvqHptr5Z5bNymnxr5Sx3kHk4XscSa7CxludgkuTOhPV7cu4qIrjth7JHxRZjERySjauj9Kl4eGt+Y/KqDIFStXzr8SMM5j4/5jwK1kxoFxyNmE3O6XKgkNQ7LHsvjemWOSYp7CQRICxhFwUXckJV0VbExNe6M0dL5u2mGPswSee4khuHCMkZfBJ9yjg1VzhwHg2HcwQnbH5KcGzIeHyo2OSklvZkQqlfVOkKkjjVdcNIBnJ3IWtl3XXWPlZ1rN0rqjoQ8iJzKgbiCqT1R0/D1DE3mxNfKDSveuePkLj+/ick8j49yNFhhkmD5E7VRmW2+6NXBESKbcKCqincyBSR32/tDHuVdebfmr15rGv9ZXeo3sss1uJCxocTQAgU+C9QfKf5eaNp3RkcsbI2aix7XVwrSrqro3/dtfus6j5FcOYJy78h0jUfFU2vj30XDXGkdyLIYwuEUd+O+6np0UCWKd3XcyFfpTVL0yxu7u4bfRPLIA4OcMMRtG9Wrq/qa1dCLHha+eNtGmnlIwBHtXTHUcc/GSrweiwLDsQOLS1MRigqWa2VHhTaqBGaEUOK8O4yDi/iNCRFPquuiQau+1/p3OaPp+NVxe8sLS/kMt4wPl35fCiTfmzi2z4ouY0lixW8w23QnaG9QkUCQF7Shyk6thLZXx6/Vt010XS9R0+9a0FgL6Cpxz/aqHc2c1tK4kkRcRoOyqBXuI7xOONtsk8SKKuIIKSr4L9XnprOyzZJQRtpQe8IAuLjVfaNTjOVRchiYFsin6e6Ci/rT9O3hoZ7LdxHA0BfL9JYpq0CbbVxHUXZS9w6CDsqoo+K9v6ETbWYYIXPo5opRZFRktV9yY/vnf8Ai3P6uivSW/3As8Tt5XOSXDd9Tu27mTUawcusIk3CZllkG8CI9bEUU61+jceb9R6KbDigamBDsSr3JtrjtxpE0F0IXxtDDtxV3trx8zqOA9lftSE5JiFVT5zZM1mUUcOHSy40qyYuZqMvRn2VFuczWPsqQWMRtxF9NRUHl8k0HrOm2MZaxjn89pxApQe6qi1B0eNTirhOI/l3Oqsei4f8e+D+SORZNJVNy8lzbMp9pi/GsU2offKshurh6W9IrwJn6REkVU328tXDRr30UDeQK4Y1/VRVh7I7g8uvhJVg3B3I3JHMXA0fkHkjGa7D7i0tLZKaFUk87FmUkaU5HrbRh2WTsg2ZyxzICL0+9pQMR7TTX15cPml9Q4DiOFBkrRpsYgg5TcW9q9odQwdOtq+1JdsXt/UVs2AH1nt/R9ESLvJR3Ty30MJXE0IFEw71p6zITxm3sYUqITz71bHGI888BErilv8AhARVFQt90LfU6+W3+JlpxFb8qXEPh56XItq7IpP/AFMiqVzLZi5pMaP33px7BXXYouNAK/kdjBr+FNfVSd0s3EaAUqd6fd+zaPIcioVi2kewopDDdjEsaucwIMzYrMpmUwMlsBdiOtPJ+a2hghboqoSLptbC1MIMjiH7qjelVw24fMXhuaQL5vUGXYfRU/yK4bdiJnfGU+BKy2oYjxiHOOP2Hx+7VUlrsRHpNfGUnAPdDFE8dGWVvEb5txA6oDS05bcVLbXhtGugmaOOTKtfcpSPwj+OXzWt+K+WpkCFj/8AzFEpMhD7LKqqdzLWnRZtJlRbMuuxyslI2VaeeRPUEPpRdUXqTTNFY+W1iLn3EkgealuFN1Ar501qmt2Epkaf9IWEUNduXt9qtRzrLa/BMSrcZxeC3W18SBHpoESLFZgV1RDiAkaLAgxgABkALYoIn9X8Oq7FCLaIsiJDaIx7eZK6aQlznVz2V3JbcT5MtIN3JsK69IYLl5NroQgDTjURYUcVuJHa8BoSIS+mu/Tu30NwdpUXIbvKfHhxpeRcOssNzyLEuMZt0lPUyPI0TsE5Heay45dwqw6yZoQqm222m+mXMumv5sR4jWtHZe6iVX1lFejgfVoyw+jakr5C4hncaZNOx+X7KU20pSYFg/GNAn1pF+S6zKYlCKuABohdo7oqeGr5Hf32o2rbuNrOc7DhFaYGm+qpN5pjLOd0TC4xjGpzxx2BCW3tHY4LFggDTqEgEYSrAQPr4qbveg7atFpYSxQNfc1Ejtn7UBIyNvkNd6iRM2jru5rIQiX/AHVg08qqX6BdZbFU/wBO+vrpxs4uezF1QMe3uUJEhwjFXLI+1XH86Z/jwP6+gvzWb7rPf9q35M+73FUzfIfAvm5lL1FKuWMP+7uzX3auBiswraGBxYrbD7x/c2g9OQ20+pgqAnb27+Oqv1LHfyXjeS5rTUbE6sdRj5ZnDTwhav4lfu566zz9/N+dRkZO7VMn6GJT4zUisnTpRo8Fi8KN+nIcjPfsLuC610nQ2suTeaqWycWNB+tJ7zW23k7mxNc0E7U//JuH0eVcmUXx4w95cZx46iPlHNUSgBmCymJVBAVFi5sxAaZhzMlfbVnYdiVnv33306k0yKabhs6MYRtWlsX2zXTS4iqaq8zDjyphxsfHJMNxYauHHhwqSaUgAjVsWMMSvi+ygQ5KRwFhtFEdkXoqqu+kl9A61fyHmrhu+hXHTpRPBzW4BL5aZzx5XC4sjlnCFdiS23yh1sO+cFGHD3VJHdWNK0jQ9RLfz0HHi8d6YOPCCTsUDzvOeL7GMt3j/JeETpsRpG32nVt4brTC7kKqpRU7/qXxXR/B3KEXLfulCPAPnZI+GnDVpc8GYzh/I+Y5/wAq387kvIEBiTZ1sdv0GawGZaxQlSIcKvRwAZUkVCVPNdDCcEkUIoUw9HHwh1WmoB+tWrfCL5a1fKthkeYZ5MjBkOaRI9xZv5Ri9zQNKwxEYgxayC7Zg/EkMsgK7tA8il47ddtK7y4mjkJYfB+pFRWcJjBIG1HAk4m+QETO8flUdPFhUlbYwn59EwcZZNZJB5uXNIT721L0lXfoqLoG3128glrGSI9o39yguNHtZ3tkI8bclrOFOP8Ahj47YBXYjjkWfkrFRjDoVOU2shmwtqRZJPzoNcIqrqQ4ksZKdnYgH2tCK6EuJGXl96rEPoc+1HwMmiHKJHKSkcg8mU2BY1keQzrwm4FRDk3SxJcx2UkXtR44kdlpwnD/APMSRQUQeqa3c0FpAKIQA+IEnK8urrDkXIXrB0MtsvZYlQzEcRiBWuySlSZCRFXY37F8u909t189C8l3YvleLxpki0c+qoYzqsyChdsz02/UbM+1ScBsNu2KLQfiJempxkl7vMe9bz5PYxYZJxc5dw1RLTEGmrFgib73ptZLNGpbCqKofY2qi4m67bJqwdKXd03V/Tlw9MKUHeDX3pVqdnzYDI2gcQqqI1kfYpG/FNxw+qdhAvX+El7tlTXZC6S54iSA1n1rnPLlhnLHni4jhTYpxSQZbcCdkU2NFcrY5Kx3mpihuGO309u5bNp1321W7+6ZOPStqHVrXZgrPp1j6dwvLijoaEUGdTks/Zf7/H/+JkaX8kbwmfqLL7hQWuXmJeWYUStNuxZJ3geoKiTR+tDYQ07hVUT1A322Xro/Wo7WS+AaQTUKn2gMdkY3iklMkQr2zHjvA8hzSuq/SpsarZM2ZMNFKUiMNEjAMsNib6sq8qCi9u3XQV5JDZu/HPC05VWLDTJXESuYeEqsfhH5KcW4OV7c8phmeOZny1lLtzeciZLjFkOHTO10Y+PUEPIfTWKzDqY59vVRFXDVdGWT2hwmyjIzRupW/BG2OMeI7E+1rExY6AMhiVkC1fs3AltWbbLEgJgyxT2stiUgmjrCsEpCm+38Wq9rDXSXZkZiz9afaO0ss+B+Em72JUs3w1yXZOo1GherLVEOS2y2wYgKp6aE2oojzSdN0XShzZGtLgMQmzQ0kB2DUs3MOF00U7GBaMg1KKkbddeggiARtmPpOEMZEQAcLqqL5poQTXdcW4qYw2w/eQn5M+L+NYhXcQ55gMq8rY2dv06ZnRLOefpJNoLMOX92ahOp2xXZiqfeg9CVOunGqWcsNs18bTxFgP0gFJNNup33LmS1DA4gdoqaK7rha+w1/iebhWefbFr2BecdOROjwrKC0w0DjUqIm4vp6KtqQ9vjtpVbRiezHM/n41G7HD3UVwbLEAA01CEcJ3J+Obdu6wDMo+W4e/BlRhSPPZiS59ISnKSDYILnZIdTqKH+Lbx1W7swxS8DT4hmpY6y1LMQEFmfkVybktm/KxrD7uorlslGaAGSw3WIBg3HiuC4uxhHVpCQ/P8Ag0DJNwtrGfEpOVIdiAPIecWvJnJzWC2NZNh42ybUjI1/bspnuUOLVxxTcQjxyXvI/DddtaRzyGRvFlVZ5cgxIwVrXDdWw4FNHis/ZKHFIjaRUc2bff2BBcJDTZDU16Jt12015jN60T200tcNxiXkhKP3fJTWrx5h8VKSEVP7WQ2JbmQL3eO3XW6BMUhOARup5lte4Jc19yUpx23Z9ksia2TSuq8yu7bbZoiq0Cqm2ybdNF2V1FaTNe1w55KGvfDbcLsCqfouOTJuWyaCfKSuaqpk6Oc9tdgNYzhiiOefepp112d90xunw8tw5sjclR7W0fczvmaKxsOJ3bkeJBwWcG9lXG1PkxGnBntkqqy/HYPf1lTfbvdNevn01T+C6/MazMcGUPwVvuGQflR5RBeCPihN7i2/yT/w/wDv0fQbvekHLO4qojB/3h+HyI9aNphdhXuYaovVdHWiDDdlBdY9qUeHJfnTEFYjw/URmS65bffMi09e2udRs/WsegG7FQhz5rf8v8uw+QbW5y9aZtJhycMdtTKpyGvnsOhGopzSetEJiGTiF/Zl3EOgbzrA6vfyBh/DaQRX9qJjj5TBHsCFGQ/PfJMsxy9wN3G6KzjWsqTV0eI5AolRyqh5HnpQyGUpZcuZYJ64+n6HpKm3QkVNbab1nJLI62JPgcB+mKjfbNuJG12Kzn4Mzc5tfhzhoZ5XOVdtFv8AKIlfUuRZUZuDQV9xLbpGmFnmc51hqGSI2R9e1V8OiavkN0bqEPO2hTptsIYQ/LGnuRNvaSRYX0aM2asmINmrpL9BNmieo2vRfq3Xpsqay/yFa1CAXM2GNBcSWPbK9IPGe/3ISCbFtmPI2QDRU2Ivq6fq0GAVhbzL6uPOxPh+IJiqwIlbMOGDZHHNpoYMdHzePZfWBxwkTZNtv9dr1avpGfwN+ASq0pzj/EfrUN5l4j5UyrlWKnH0+vqKYMBkpYP2EtBVyxfR72TDFcptk8j5L2E9uggnj4ap9lpV1cvfLEfAa09isbNTtLdgilHjGar1yIPkpxIDNVmmK5RSwqeUktLKIw7OrZQtmrhOt2EUpENIijsqL6m3XqmqVe2NxZXbxNU1OCbWF5BeNcYMmnFerXyNy8zNwbSQyE1CKwZcQ2xQyYVon46tE2Dgmi9dk6EmhkxTi/GE0z+xlWk4/cOUtqMY5jir6pNvtNuNG6qISmiNiZLsi9U1szzjvWj/ACHuVy3FGWcdjZWEC8yGutLegRgK7EmZMeMkie62hxIzk0u2KEo0VCJoi7+1dl20eUvGSPFPlGcS8mS7zjj26kwR9NinlVzaG1RRAX6Rix2BOK8Hbt+Bd10a3IdyymxqbuNPZgPR35slpx4W2ks4zkd2O4io8fay5+IhDdOn4fDU7rYcplxtr9a1u7NstoX7T9qrd5EppOJZXyXHdYa91KymWUR4urgRJa+s0QAKoqkoubrtro1lcm4ZbtP7oKpDT6KGcj94hRCqtIEPGDblFJcB21FiWrA7Puj27oK7fUIC51VE8k0ddH/WD+E/Ba2N0ZrNzTnX61JPseP/AP6cv8ST/W0PVqKp3LiXrcoqoE5+B99nOPR48gWxXuSEThP+p3ttbILJOsiPcKKv1Drzje5Y4mi1RewqxqcinvVtrEN1+49B+PN70NUaBRRfatOer2uubdRERVdKbaP1D+UhpPOU3fFciBgkW8l3vaxUQbWSkGzmtMtygjRWmX5098yaFIseO28gopbbrqx2ejutniSmeO1YaLp0o5AJbtwrirgvjJyRjPJPx7oMrw+ZInU1hkOTwYMuRFeiLLWutZMF11kJAgpx3XmVUHETtMeqdFTV+0/ULeJvKuCAwD34J2YrgwAXkD5YqjwgGtaYHD2/Stisi3h5P3PQ3Fa7xETJxn6EBUU0DZzfsPbdd+um0WpaYZWhsrGEnzEig7cSomwQcX4dhLx7PC7NQvl9Vs7x847rDIljiNCLSgTKuesBqHeap+YqJqHUbq3Dv7jBSv32/aiPTXFcdOuP+B/2LdZFY4u9R8exnJtRXSajHmG5slyfFBSfMYZqhqrvajoe3LdP16KutZtRC0fmNv5R++zcO1FDSbkiv5dPj/gd9iHXKed0mP8AJvGvIdLnVRb1NfFcwvMKSLLalyW6a1YWdAvRRpCNQqLT1G3UFVVBdRdvHQtj1N0/aP47u4jklBxcHCh+g7Aiz0l1Dc2ontbeRkBrRpaajYcxvqUSy594tOudYk5pVyW392nIseBMeN2M7uLvqp7M2n2uxdlEk66eal8zOjWRNhlaJzT901p2YICy6C66mm4rQmGIHxVFKn2hZDGV/EDNnqfDKvjXE8tuvYTHzku4SxVBBYhtlMnPvSHIsd6X+UJFshJv16bapceudLa3ecqyhMUxBNTUYDMe1WzUunNf0PSheXk7ZH8TWlooTjtok9+SPyC+O3xvsa+VxlS4xVpZJCtDgDZvU9LeyShlIgsTmybkzzr0kmncrXb5oJJ4oe6z09rS5jm8YGGO36VV23OoSkRPa7hcaHDZ9CDXx5/eOSuR8yyHCvkBwHwxEociM4GJZVCorishU9oezmzJOX/u5Iviok3KjutSCXqpr+HVXv8AerFYWBIwyVqGG/L7kbgKCw1jeEXc2kL8+BMqMzsrvEpbIJuPfT3LVs+iMht3Ng+Dm3RF30AEJIOGRw3EpxOAv3gNZyDkW3L4wqtbIhj1NhCbdhxqtHURXI7sRd9kV5V7lLck0dZ+cKOX+SV9PlPdqGXS5tbOZdiPsRnhdjIBev60dBYIFBVN5HBRCRdk2Rddc6V/pp+9vwVB1b+eynaoLiUODDp2mJjDJzHKN2UzMcQGXvevl3IJCBEm6AXTfroi+qHAdqb6Tmf4Cod6d3/NT+kmoKIWq4foTbwOtMFAnkpgaqbjHacmQ3+Y2f5ydDExRdi27vDz15+dNC8Uc0FEc62+6E3vxlh5S/yxx3kEqnFmvTKqurkXT0YErqxt1HFeeunO4GmWAb6bKqIi+eidM02M3PqmkNjca02BATOa6U8A8KJfyn4G+ROe5BYtU+e4BD46uM2tKvHMfqbmKMydDbFkp1rkTMBo24cCQZgLKvOohIK77baus9mbqNotn04RQ0O3YUz0mf07nSvPgBGG/NWucHtZsvFWF4ZjGO00qvxWkrsSZeo37B6uKxqo4sy3RdiuxYhyXHwI3XG1MdyT6lXVdl6V1qV5dzn8G6uC6DB19olrEIhaR84fvUxIWwy3j75EUwrKqK6iR7cy2mS7IlE1TvUTX3rpeC/r1A/ozVZGlj5HlhzxREfzL09jg+C0Y6YHAUzO5J1m2afJaDNGlvMbp0NkDlIVcfqvORBNBVR9d5EIVVeiKilpc/5bPfi8V9qN/wDbl/s0yP6AmTwrh+2yOjqbzJZN1UpcUJW7UVqTVmTkhI5e4ZNtILT8NGEUSRFU90JNl1PB8rQP6poc3tUmo/NaQxD0ruXLQVA2Hd7Ml55w+OubVtLjrnAeKpmE2XF93lJ5Jci2kOM6HbGeii4rYIpPb+G/TUt50BbtjbbWULWludB7frW2j/Ma+fAJb64e6N1aAnLFQDE+IfldCZhpfcK8MRIwx2w97fWdmTspV/320M/rdLfxTZNONI6M5DQLfTo5wPOSBgdmaW6l1vI57ubqMtux3lAJFUUrabmfHdaZy8H49h5pJr5ceukcftTXXawZUdYj5WL1k482qusuKiASKJb6tl7Y2Ftp4a3TYLW6Dh+I1oDqbRXtVUZqj724P+4zXIIrwOJI76Hcquc84qmZ3y/AtcqwLNMhWrrIkeP75iO1XvzEN1HSkuKRVrTbTaiiKuwgG2yeWkIa0GozRfG4HPFE9/AsOYbWPeQcZYWInqDAo0GY3XOCKKy49ZimyzGSRNxZ2RFTWr4o5POKqZl3cx+R7gmF4V+adPxTNh4lyTWMWWJp6cVjIWxcnx0jIvR6zrV7haJgU2N5rtcUU810CYo6kUCHcSSScSVbPx/kvHGc1UDIOM6Ljm7hWRMPJb1MgZpRSkKidzsIyV6O+SFvs6iKK6kY1rXsDcKnFaCruJpxapnllyzkOaz/ALg9EZh0bLcFkBbMSfSAyDQCSdqiRuKC/q122wghtrGMwNDC9viptpvXM7yaR148Pq4NOHZ3KLRJMt0X5Yk+MYzJBBSXtZFCVBDdV8x2Tp0TUphikxe0FaC/uohWJzmnJbf7vA/nB/ij/LqHkQ/dCm9RP94rlVxsW+Rsydxe4qAbftHlr4bjFcao67GRCCS6sUe5l7fqop5pryRcOfDdNgaatJQ89zJFcCFvlJW2pOGLybZ3mJ0eSTLG3Ycckzseq/dsS48Fp3tNHm0dAHlbREUunciatFppd5cMaIHnh2JnSlAVZ5Sfu5KvMeNcVj3vNRYnYT6mRbPQGHgQ+9WmlbgS3GHUdfeJDX6VVfBd9XHR9A1Kza50jyS4givYFsJHNeGjynNWccBcMxeC+EeP+Oa/IwyNaFLJwL0B2cmpYWsl8iLoql6Djotn4+A6tkTbxo4Jw3l7xnX9ib/gthDzG0uJCkeXWjbtzEoURbKc9u29Hr+3uabMe05DxL1Ds28fPUtDvK0bIyo4GNDt42dqSHnDHapzIlRxoY7tfWIqp3dr6Kr6ACuOD13VdfeLeUQXTffKNFYqzqrDozwg37PFJUQDbH8wwKtBC7y6d5J6KbKvkunerN5VqxzSa8A+AVbidzJncWPiPxRso4qu0USQHX0qCG8907hIEUthcRN+4R2/j0N03MHSu5rQa1zRWrxltjHynODxu7yoNyZzXHo6FqLKbi+6OMDEdsBF6QLTafU4DICpCoj4InjrTVNT/LJ3x2T+ES54ZHsRml6czUrdr9RbV0fl7s1Xbn3yU47qItgb9g990dUkCsdjmNjMPuXtVWiRT9LdN18O1F1SLqSad5lkne8E+U5d6s9vZWdrjBGGv37afrSR5fy/lvIrytLJl1mNiakNa2RQIDqkq7K+y2QvSnE/WSJ+rQjjwtJRSFN9b+zhuNN39dHTZRJpZCdw9VT6WR+rfx389Qc525YqhW5k8CISJJcWcBD+M292nXE32GOwX1ERL5r066Xm4dU4BYRq+LmPct5JzRSzeJM2tuNXK6axd2jkF196ndqozwOPx7SsEiiPhKQFbRFHdFXpproltJqV8IZBwwg5jPIoLU7uPT7MztIMx2HLcuk+gq7PInFs3zcMZNgKSZyiIskb6o64q+Cp3kW6D5JrshkZbwNgJ8jcO3vXP4IpL6/YXYRSE8RGzuRMtcciLY1GJ0hvzZhKZO9jXZG73R7uxxzp9JKu266Uyaw+3fWZrRDvxrimV7oj3ag3T9KcZHlvF4twFSs3/pjkP+QV/wDSb/l1p+dwfpVTf+Oaz9xq5IuJZuUV+W1MushyJl9Gm7BFqW3pVrNkopA77VuO2ZNzDaRSJV2RE269deW9Rfy79vhc7EZCqRXMHHciTjYMcicUWuCcjyLH+cchmnQXt5lMu8V5xuLDmTryBXiS+4jy69hlJGxCS9+6Gmyau+iX1/DK2RkMz4tjWsJKZ4p3pvKXIcM8QeawjkWYjWbZEpx4OD5Wbq1DsaSMJIyJVI2+2y4W/wCL8O+2+r5edRajII+TYXzQGmtYjicMRiVin4gJTV4t8ms0pON+MMdZ4J58upK1tq5bZNX8VZ5IhRnRyC3U2mi+xkssi72vwKSb/wAC7Qwa3qD3EXFndiOn+WRjsz9qZGasIbwP2Y0wyRP4vu8jyS/ubazgSaW6R4RjV1vU2NTanBdHvbOXCnxY8kHW1VUVEFdl6LptaXfq2Fxilip/mN4Se5aMf4wOF2agfMRtfd7JXWITkn7YyE4XnRaeMgdUm3HFX8xpsjXYen1an5n+F30JgjDW1MJrFMHsEt652ZLq5jJ1zLolKiCVOjhOyB27gjtmSh3L1VUXppzqVwZ7drTHIyjRi4UGQSSG1DZHHmRElxOB7VIFubmnwppynrLGzedx1lmQ1BgTJmzPc4rbjntWjJlnv8SLt/VpZYzG2icI2Pkfj5BxI+eJsvA17mNYNpNAcVV3zbbZ68E1x2LyCjitmRRaCqkxGhFd+0HZLEVy0Jsf2tkEdvPVO1B11NcufJVm4OBBVsibA23jbDTAYkUoe6irichwJdw67aP+2skMi9K5lq1Y77r6goNh2Si6+Ph10LEx7XVc4HuW2CyciikUURblvtxf21ZUSaRU3QfTMSXdVT/TqZ/lKygTevVNaL5xYzlnY9BEHCQ19RfwkfqGioKL47ddBLVR7G6OdfXkP73a1NS9LebZbbtJ8auixAMkFHFdmPRmwZDfciIhRETQrYOObl8bG1OZOA7ysuHDGZag0GQz9gV+/wAO+KuJON6G2TGuScM5BzS3jQpOSv47klHd/agRR9vDYjwLCQ9GiA4Rd6mgoZp011TpXT7ayha8z280hJwjfxEZ7MFzjXrm6u5i1sckUQp52lvv3K1TDoNAONx3XXEmeiwXs4n3H2UNywLdQccd722HHGy/C33KpaM1V8xuW8sO4TXZ8U90NkA0+Rzi0ztpTHPDGm9ecctswHJgcCqORIb7h7H4/oGTSj2r6KGqOuiA+aIu6dU31rdCy9BxXOOIwBFc+0hY0Qal+dc+3LfUhjgGmtaUxwAJy2orfeMt/wAkL+lY/wDtNJuPTvuu932q18evf4fod/0r/9k="

/***/ }),

/***/ 149:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/jq06.jpg ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQzMTkyM0Q1MUEyNjExRUFCRTZBQkUwRkY2MTY0Q0U5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQzMTkyM0Q2MUEyNjExRUFCRTZBQkUwRkY2MTY0Q0U5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RDMxOTIzRDMxQTI2MTFFQUJFNkFCRTBGRjYxNjRDRTkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RDMxOTIzRDQxQTI2MTFFQUJFNkFCRTBGRjYxNjRDRTkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCABXAIEDAREAAhEBAxEB/8QAvAAAAgIDAQEBAAAAAAAAAAAACAkHCgQFBgMAAgEAAgIDAQEBAAAAAAAAAAAABgcEBQIDCAEJABAAAAYBBAEDAgQBBwcNAAAAAQIDBAUGBwAREggTIRQJMSJBIxUWF1Eyk9RVlRnRQlQl1VZYceGSotIzoySUZpZXChEAAQIEAwUEBggEBAcBAAAAAQIDABEEBSESBjFBURMHYXEiFJGhsTJSFYHB0UIjUxYI8PGSM+FicpPCY3MkVBcJVf/aAAwDAQACEQMRAD8AzMmY6kLr28PlZ3aUpZ1Z77lerzKVgFnGiVdjiaGYtniAprABXM1I3ZUqaJduAJEAQHkO746eaMb0nVjTTYk2nD0A8AB6o96q1Vuqp/LUhPcJbxBJ9m10WbnDziAk44knXV6Qgo6jjFSF6SQxJk1mYHCaKwqGb+ZQftPvyOQB39PVmU9YWahTW5KiPQYUFsoJ0a1HbjAr9mHLoEY+nJoRRou0YKj7BJKmQMDgzyFr0A4alBYDgUoG+7YBKYBE3rvrXe3S6hLnGPLFQDzi92MGj0jwWwx/gEWS18Wu0neq5V8suAfmQXQrrabyUlFoQcYo3InvGshb8RKIcym3DfUB+pVTuMJ4o+uM7rZUPP8AOO1OEDtnaETLkqxQaZI4qiuYMnAm5KVQCeMap7tQoj5N+ZTJgKX1AB+u+jNupL9IlJ3GKpoctwtcIVz3uiqlTZ/GGQJSuBLw6U5dwsUMg2O5POtItOuP3CbtIo8fZSJyjuXbYQ5/XcNhTWtrr73pK42i1OKauVRSrQ2tJkUqUMCDuIi/sHlDeabz4zUXOTnHFM8YnnL/AF6yV3ixLg+ONgGt0FhORUqtD5WpjQGZKbi25/xDk0Iq8MVeILtFZ2EUBnxbEFNtHENyNz3HmTpVoLUmm78pOoLnUOKE5zcJ3j7DDO1c9p5qgHytCU4iUgINiC+E/JGO+v8AR8q9TrvF5DsScBEklcWyEc6jJOQiq9JPmxp6BfK2E8c8nJBePF6sgskhz9yJScBEoaNqX9z3TP8AUKukV6qlLuzJmgkzIJ8W0nu3QuLvoKtrqhNxJM0gH1DsgQOrPZiax52moWJOzvXiSuNEdzFQx1fH65pVN9iaQk2a0UrYJuGbkWcoM2twkkGy6jo4oNUUlDnMcFkuFT+5zWty0n0ypNWdPLm6boacrW2XCEnKtScokU45QCBPGUhuiy0Roy31l8dTWJCqjNI4DgOwwxH5KEcRYcr1uS6xJNS2l5C02x1uoNGEvYIeNr0QwfxtsiXz9io4Sjnzzik9aGdCUPEgcpdtIz9iXX3qN1t1m/cdQoLNjoFimdSMwDi5BwqM1EE5VgT4CUF/Ubp3Z7fQSQhIfUgkSA7ZboryR3YTsS5SeHa06HORw4TKKibKQUBMyqjhQwkTOI8jbHN/nBtsHrr7GUVLRvvOrtyZUeY4DjHLjGmahp5fMxYmZemOZs+eOwaxTplosAZQpVG4GUSWTD7A+w4im9KBTiJhEdtgDVl5LlDmS2RIc04haZd3tjVdD6tabT8lfXm1Wur12FTc2hZ3JqqGKLJc5YlyUh3QKqLKmAwB9QMBgH6CGlJq0EvfTBY5T/L7SG0k+7gN0XTsidcLrlut0g2NqlXLAnVMx4xtNgSjjtObGCgbI2kJV+nzcg4OVuxIY3HiIm+mk5Va7pdPXdNvuEilXH+cD1tsFwqEG4kmYjx6yQjdtilbgNURBPIOWycHAlKog2TynYREDjuQpD+P7hDb0EdS7xeWqx1uqppCmWZp7v5iKe5FZr0lz354wsr4xoJpPzvfWRhnldKwf9yL8q1WeJkcuHEazYJp8EQMsmIkXHfgYA+pR1d01eeQme0xPuqgpxrsZSPWYbH+0mH/ALe/pE/8msvPdsV84CMvevot0chK9ieVo0febHNLy10JKPK9GXy5xzV8ZkMpfZpJ89ZqMwfuEEkmwIlP4ypc/BsQ2/yltOrf3A9SetFztaH12vThdUAtxCUge+RipKeA374+h9/sx1c6bk0Ac3Ace7CI8pXR62dw2s5nrAuXccp1C6uY2brcOpKyUQsydxIPIp9EIspByo7ZrsUU10PQpkQ5HIQQDcNd1WXUC7KhFPUOByobSEqVtCimQKt+BInD5unVHpcbBT6eulKnnIYQ2o4DFKcpJlLfwhe/Yig5nxJlqcx3l9+/mZqGpdwYRkkmDxpFEr/7eryjFCKO6TKLpm2In4+aQHJ9o/TRQ/f7ddlIccUM6jx4Ydkck6xRYEXd79MpCbQQkoG3EpBXiZn3p74kn4r+0NryFOX3HttstbQgsZwiVDoia5UI189TNkZGePGLrrmAJFwkZQwpgBSm4Bo7doLcm3NPAj3eP+MKW6IqlKnTiaQDPvnBS5opUi67BWKWkFmDCBj8t2uVeKKKMvEZnJY2eOGyux3BQBBysgKaRh3DyD6hq5buLP6ecYt7yGqkpImSNktmMDlvtVdc7hyXkkIT4tnCB1yFfcJ9qqpjyGRpd5x9Y8ZW6XK8yBa2sO5pq0WDBk3scbYSMRZyfJViqQzFMSqpnWMH3/hpXaCuGqbPqPLWVSV2lx6TgOX3MZ4/ZHQuk+id01zUU9Pa21KrStIQACZqOzZDhesdTrLWlXBGi5WiLoat46TbRsMpHmhztIphBZQVTesSJuFmkj7+xXoiZUPtOkmjt9R1Q69p3U3tVwoBNClHZjtJ+r0Rd6//AG66s6f0orLyy4mZ3pUNxO/uMJ86x5r+XvrkFcx/arbQV30ZPv51g9nbdDZMyFkCHK+bEYVaexU/sRpWrwKcOYySAQTJFz5QATOBHiOuMOqP7ctEVd6q9cWekWrUTLJWkJLmYqCRuzS2jhEnp4zpjUFKWr66hCBhMqlxHEdkCD8gmYULV25nsi4wzRHxc7lqBr8PlSuVpBVk0p8jao2Hh7VBkjpY7t84MdMFFgUWExwUIAkOIhyFxdC7BbNe9I/k/UqhdaFMFpTnC07FmXwnfvMD9a/btFa+y2BYcopnIrA5hLE7xtgMs8suz1qpDnLWGVrrYI6uSOPmF8vdcVcOGrCQmKhFBAjPkVcruRZ2ArBylwUIqTcTFMJQ2DTL6Zr0d0+rW7JYkJYo3FZ+/wAUp4kz2SgH1PVXHVN5qayuB8CgkA4YZQd0t5jyruLLHY821KkWuTsLSJnWDOZsCtfkUGaTKRLEN5J+xbPF2irVoKyiqgHAyJgRH0DXUWruoFdZLC3U6ZeSiseORR8JmmU5Yz3xXaQ0i3c7s7S1DiW2y3vwnjsGPsh//Sf4msa9tkcoNj26brNDqYRFPUWaqw81OKuHbYkkSXbWFCLSIaXTO64GUEgiJA4j6a40uXXfq7Y7+G6l9S7arNmASjEEGX3eMdQ+Z6baT0JV6OTbFK1FXNhKKjxSQZhRVM4bBLbvhUGXsHQ/x4/KrXMRw9snLdW8f3eot281YlWyC6jKwty8zySrdFJMEWqZx3+0AEPqOukunnUS86r0M+5WghZIxIG87dgjkXWNlt6krlvScJ9mzbEy9gvlR7ZdfO8+cqvhKCRVoV/f41q2PLkjIvRg4wHCrWHVma+1IuMVOqmfyB/Mkn5CpnIHk2DcNcxdcegrvWTW9nu79wSlFrdSsyVIeEgyOVQ9c4uen+paHTWiXaFygcKid4VP1gw/Lr9kiBpWCkGNquNQsk1GuL8+l39gXio+Zk5kbPIyU46kG7RdFIyaz7ymTBJP6CGuiEUDaHzbqZYcZaklKhiFAAYgj0Rz/dki5XJ+vQgoQ65mCTtGAEvVHMYNm8ZY0jL+djMY1YtrfcpO4LrMFGzRRVafEzlwZZUz0DrqNFFftMPqIDsP01f09pNM3kUDMme+KOpNYyfKtAlBE/T/ACiW/wCP2Iv9/aP/AErX+u6z8kfhMaPJ3L4FeiKIedFZzKk+SWja9L0+6jYXtdZTsUz9uJYmQtBoMsWum0IRT2UORI6vjA2xSOhEQADb6U7uhLdqfXi7GhLbSXFqCngACraZkyx2euO+rf1D+V2AUzbQRVSGIEjGqjO4HbrpQXHpKTlSxVIkXF2jHJpWHdE9rDyNWscmg9M0KnyIlIHXEFziYQMYXQiYRAR3jXmy2uwOLpcjboaUUYgGYTMT9XrhTrq3rpVqqKxRcJWSMxnKZnh3RZ3m+w1O+SH45qhkJe0KWPtjhnH6alttZIdSIbT6U/HoN7nAMXpBTB1IAhDCsIlKmRRZNTgAgIbj9tdpF1Zqi0hNMZZUywBGBIHacY3GqDdTyCfAAMN2yK9XUnJ0ov2zvcWLGqx9fpUigtNWJtHo12TkI1s5VIVI60b7ZvJzolIUAXVL5gIAiYd9F7tbcBRrPOc8vMZRMyA4DHZEldOapaWKMZVKGOXCcPJpk6t2MyAvhXHdesz+83OeqykSJLUm/TYsU4F2MjJ2h2YHicVCC2VAeRwEwBsGwGENbG7pSqtBQ4Apye07QeyNtIhNiqVuV4E+WcTxjj/l6xbF/HLSuu9dhJh5cYy8XOZlcrTTsiSTdcJBrGJOYyEZAkDVRCLMzBw3cOfIr+SYfQDDtead00ms0jdL248pvkUq1pUD4kyBMwdxHZDa6FdeV6W6k2gNOuIpEXBoqkSBlCsZxGfX/shj3GOGMw5rie1r8ldq2KbVbmuLj1Sww1tdrsncfFsY1tKH8kC1bHm5tk2KZJUxlTOUxAogIlFQ0tYll7mV92rHKX4VLUR6PVH0h/dN+4vpvrnpum226iolXzKJuhA5k8pGKpTxmd8VzcRfI/2GovYibzbJ3dc8tfZoqtuUcMk5kjKIcvPMqwrzN6Y7dg3jWqx0myZOBQAhd9xDVtZblzKlTq1lTihIk4kjgeMfHxtttkEMAISdwwhya2Aq5n7KNT7Bz2YVFq5KfsM0pWp0JODnpW1wK1bcMpBNtA82TxjNUVwxKv8A93tJmciBgE/LRQ5VPchVC2oihUcUDBJ7xs2xKt9Mt+oLigVKb9wn7o7OGJMSLhm/ZRxerlHF/XixPI6nzOUnis3cEJQzRxX6TRDv5OHsblpOsnzd7FxsOmCagOz8ypmAOQmNtqkapbFS3FqqrKNh0oTJJUgEpEyZA7sce8xNpy8tL6XlFTyl4EmZ2ACRgvlewrPsPYsYlHFkG2uVyYTkC8zBKtsWJVOfRgjO4Q1kmTwblo9jE7GUoikLYin3F+8CiGry+3dipp2i0yk0aVzCZYDDcN0bLBpa+VN0U+0XFhKZjH3TPaOENb6d9m6T0uoV/p8axiVQVtTCTduMcN4WzQTx+4YItkRWeKyziTNzMluooknxTH7OPppVam0zVagWKu3KKMhnJOEMxbFa9lRe33HMokAtU5YShePZjE0f3O7P2vNEdTWNjsuQ0WywM5lS8VyQRCJQK1ZKN3rFunGN0igAgJQAvkEQ30Y6e1PUaYtXyZKTlIkobj3iBGq0Q1U1RqHgldOdgVIiNXhTomtCZ3xdW+yWA30ZUqHIO8uVa0PbFMyEJEzdadF9vRZGPkEgZvoO3ulEzqByEQUIU4AJg1zt+6DrDW9K9AVGorClxmqIE1tEpUZneUkGLiyaeRWOfK1EKn90ylhD2svdQurHafDtsRpFCisaZWhIh3K1t3SW6UAtIOBQXOpGOhbKIs5Jm/VOcCguHJMxg9dg1yT+0j/6Ci93Fi1auqHn33pBKXlrXlJXLDMTA9rHo41QqdqGmUJmSZgAborj1GfxuNvXpF6x3Zog8WZZCJb2htaJNhKhs4jXEZYzVh0SQbu0nqPkBRMgl5f8mvsZbOojd3ZaXSt5+cgOJPBKtgHZhCe/QKGVGsqFANhRTIylh/ONx+1w/wCHTFP96ZN/rurT53XflKib8htX5ifV9kcBAdLOz1Mz3ao2uWnDMiljXJuUqjYHuQb7XIWMWcqHqB3kmRs7RWckTWhn5DkUEPy1gLt6jpR0lfcHGeWDlXL3x7/pjoeo06K481pCQNuz6oUL2x6h3CI7Hp9bZ7JWOJoMt267ZHokvE22Lm4GNuU42dtk4iUlGZUkWRp+aizNEin2J+aisI7bhqFVIUjxKUpat+bGcLTU2l62wVjfJkW3JHHbjDTPg4XdUe1Y66/3jHM0EIe2vIvMDe7T8jW2MVZoaSk27aMYQxE/DKmXcKKogi5EUlSqDsADtuqtRdQG7fdE2ClSPNNkFWGzOAoeqGvQft5u126cK6ou1DTVK8FhhClSJUyotrmk7RmGHZEGd0OmzXpX8m3ZSjyB3LbHGSIWRzziB+Vms5SfRk41WOSBbxxEzkWdQLsx24bAJvs2APTT6tbzF60gainI82jKkjtKZ7ITduW5ZaumU6405U5Fc2WKUkKkB2TGMWK/j0wrCdNutmOrYWNdOMndgnFds94n35UEH8JGSqCisFAsUVEiqtUlmIgouQggInMHINRbJYH3KJTtbMATnLDD6YDNcX6svt1XRoCAyETBSJGf2QU+ecKY07c1ShS+faSMrQcSzFsydJSjyPO7aRSVHrz2RL+qbJmI7iJUpzMlGxtwOC2/rsOtzOrrZaaSosNqqWnBUILa0rUCSDhKU4ptG2+spK1NQ+iRSuYMjPDfFEztyxiD9SLNZ8Tqqjj+Qsz5i5dw7EIiHGLsWYWlzrlZI0IcTJR4QScaJUy/lpqRoE+ol3oL4wpNqC3mGQxxSMdnGDdusq37iQ64pSOBOG2EeQbFOWn4hi483jfSrFksDcSkW4LukUVCpmOAkIt9+xdw9DCHpvtoWtLdU+oC0pJX/mEW0qVB/FJH2RZJ6gsnGYeuWJLYnMTy2bOvWUI+v2mp1lp5XUjiqxOqcnEWB22TKqpKOoJ1HFKvunxRauSKb8QHa9ob87T3By3XxIQtoy8IluB398M2w6Mrq2zi724pWw4mYmZwV1HxZnvNtIyfRcIRcgtLW11Qnk7EPHrCsTr1jDIOW6zKRJKuotU67yaZiLhuUSmU4iU5dvTV2/dbYukWRMpCsDhwECtpst2q706zlTlaTMyGMxBWR/VXI+GMk4TxRkGbn29qyDXHU/NV2VqcC2Uw+k3SMVkUHUYHt7CxfSr0wt26IKH2IYxjDvvqbS3q3rtBZUAQNnGOiOlF8tVAmqauKB8ycRykAjDMDPEGMNjhXIdQycoSLy/ktBxXUZdZ5Zqp1aGQax7ts6UM6BQGrt0m8TcJB+WoKZDfiAarLNcqJkrdZJLmMgcRAhrfR10RcDU3khtxxU2w2ZDiJiMiGmLHcnl3Sn+6WRsZtVGzYsZLXPBsvX1pSTIvxaxkfJIRZEYYz8xOIAobYDmDWxDlOqpLxSkrWZHhjhhFAtm4ooywJZEpMjvwE4Xr23+afsDQK3G4hxdkO3zsJWLag9s57uwYuF7K3gHYNFYh3IgmZZeEkH7Q4iCfjHgIbCIlENBHWjQWkdY6Kc0xXha6d0CciMw7QeyE9pvV16pNVpeAb5SVEYjgYPDBHz548JjWVtOOMeTq2e4WoNpdSoXacMWhMJdqdswcLpu0CkdqxoqH8jcqypNxMVMDCOwa+RWlf/n/AHizdURqCiqC3ppqpStiSiFcsFJ8ZlImYVPZhHSGtdcWqusiVFQFWpHiAlgZHYPRE15Y7Pzmf5/qZJ4kZ16KgeyUraJ7JMsWnFi7HBuKRFHm7HWCWNUVHzirvZBM5k3xVDEO3MmKg/cXf74dANPWWmS1a7kvmv0lI2kFJBGCVDHicMY451hdfNWtxLSylPMMpYHaIa5/DVH+zq//AHW1/wCxp4eaoPyU+iEt5+4fmL9cI+metGWOyPZzsLYGufZahRjzMGWSKVmluVfeMzQU3GwYSEq0KioPt5MGiAmEdwEGgiH01y7SJZZPilHeOrdQ1FtuJtlLNlc/eJw9AhRnfjrRfaDWa/Iv7pKWKXr9ysMVKTMq7IlLlioCRWcMZyHOUiOyrpRRHcoDy8yu34+kF2ierkLdZEkgnDsgJut9q6+qaqLg5maRLDjKCR+KvuXNXq4KYZzE6/SLFKSFRslRu75cIqUulqhHb9gi0sloAqi0W8VKt5mhzAZNU5VPMIcSjpaDSFq/UL97uTJcqHAgACUxkTlG3sg01L1Mu1/0hatIWd1ylstCt4rSVTDnNXnISAMMZ7Ysa58jh7Z9h+piLvFTyLuvV/PoUp9eW800sri1U+WpSVkkoywtgZqeOIRUUBwiqqodMV1jAUR0T2IVdpNRyVjybjgUEYzAAlI7oUd2o6NDynLeVIU6JnMZ4jDCUFT8hzVxhrDv8apJKKbs6ZPwbgKmjNJslpI4rEJFs2JeHhRUVIIEKJQ4B6gO3polc1WtVuq6FtKkrcpVpSokSSojAy24emKGw6cqam+Nh1aVF1QTsO87Y2vx4/IrjDsZGK42koqBx5k9Ejwr7DkrMtnzCzVpQ5SpuYB09HxSoKIKbOGyoiqJ+ewGLsGvjh1Equs3TvXruqqe5i42Zmp5vlWkrQ4tCSCWwpRKAo7jKUdM6n6PXrT1rYuSUJXRrQCpaUkBA4qxiqX8r8bgHGPcvtt1+p9XkI3rxZKia32eh1bnFwEFf2itetEsjAqIlUTjJhCQmRdJGS2RQOJyGJxAQD6a9MNfVeu+nDVdd6N+keUkHK6pKiPCk7QBxhAvUiqO6HMoKx3QuPrH1/6YN5teXrDu25VnX6a7GGgLeVsm5i3S6CpVWbqQi0EI5skVLkCj/h+WTfiPPjpz6SqLKy15mnElJ3GU98UN+duBdFK0hSQr727dDlOub7p9hFpPZbTutxb5Eq+REHWRHdPqCbfGq0HYS1uFstbTfCr4zphW2xyM0VS8UHiJFN9gHQ9qyzO3ivcubDSm0OKnmMjOWG7uhiaF6tO6Lo02O4qNSgJkEpITlnxzQRtLGsRmbMz8MkU2u065rtLFR8j3KbjSklK5O2y5mq0xCy0eqqzdyztioRY6aY80i78ygJR1RKRStUhpiCpQ2kQytF36kbvrt2eIVT1CP7f3hPDE7I32cfjYydY44ubcQ9sqTbbuXkZlLOcsRK8s+jF1kAOyjkI5ytIHFqQ/FNuVMgCbYPQR1XqvdHRUpZTTuOHZgoD2wXUCba9eleWSpLxcLiTuTM7402Pur/yq45eyltcdoa8zYRceoEhFkr9zdAq1YgCgJSigpezORNAu6i3ISiG++qOk1WzTrNOmieDi9hzJkO8Sgj1LR3S71DIrbiwSCAk5VeHvxiFOxvcDtxVa/I1rKUZXbfiyVRIccn15jCta0g/TbKGaJot3m0wg5UdkDiKBhOYQ3DbRVZa1d3cyAFpQ+LHt3QD64tFVo61effrGqlDiTghKknES+9OKvNunEcuNr1LW6cOVywdOVoOLhmoKGlp1ddydswDzbGEr9RwAnEREyYgI7bjrdV0FRV1QeddSWfhkf5RzQy42youAfiEzn3xE2F2kwyreTyt11F56ahi0r9rlakVlXQs3yL1IiRDmBcfazDVHiJAMYeADsOtlDStuKfbeKgwDJIG4YRtudwDFMH7io8pWzfMcYsBYDyHfkfjOk7NT7NAK9i+qN6cWyuQRHpXlzqeIpdQtbyQnJw7syRpqLVQj2Kvt2pFDoptTlVAAUDZm9HbrbNLXOoq6oLWw4SnKSAoEAyMzhLHZAZeqehuaUItb4XTrbBUcpGVRJmkz2kYGezGNN/jMdkf9LsH/AMNS04/1ppv8s/1CB39Gvfmt+gx6Y1+SDJPRrs9l7GeXMaxNokJu+2WbLPOJxg1m4Q9ttcjaHx1VhcLsJRF5GP0AFuqoJm5CgBQ3EwByxqa7N2xovlf4XZjw4R09ekp1BqooqFISok7++JH7e9g8P9k8Js3GHcr0+xXeYn5iWsFKLGM389WCPWUg9UCSTIXxokcumhSJCkY26m2wfXbLRWoa65NLQhAKcZT+iW2LLUtgslPSjlOnmSE++UQF8RNbTXydkh1kaGi5M1BpJLBDtpFq1XeO5075dCuxiPn8fsHcvNHAE1wESpplPuAlA2r6rpV8wu1CQlW+WMKxLbjI5dOczQOEXBejTsKLGwdcyzkGG/eDhN/fcgz8jMMGbdirIPFDNoNs+W9un+368w4oJiYwgPEeAiXYNU1W7TsNFcyEjbhEVymuVXWtsU6CskbuM4Wx/wDo4zlWIHH9Tb1+SXssVPNUW1ZcxUg7ewErKybhEkYeBMyP7aUdqiHjFIeYpiPoADoNr69CqdblOSSoEQ1dOWGtttS1W17XLCFBU1YCYPEwH9B+ObMbbpdGdzMo2Jth65sX7Www1abPDwNnqUU+QSZ16SscqzVF3Du/drprNW4l+0xgMoHrtoJsGgbNeKxVRqNtKitXhGCp98PnVXWytd04vT/KZNAWChapgqSneQNpPYIU7kWou07HbEZi9P72vP1u7zdye2ywEl7FM3iSLbJRtJFXclROEc+r0ex5mKPHzlMP8wxdNn5KLTReTpW0tW4bJS2dw7I5Hr1pqaw1LOLJO/A+iI/kGWWaTQ6XEV6txmM2OSXEe0iHscm3d5AvTFwCCkiiwUZonQbnZIgBjGE5SoJKgZT0N6W+i9O1FRVhxszoZ4nNunwis1NdG6WzOXF7lpKEkzJG6G+dh+hdpw18U1usGKsQWC9Ztnq/Xml0l2S66p6xWZBAkzfLTNR6j1NooWqwDE7MVwIdVIih1R9CCOj3V92aoaX5PToBQgAAyxPf9Mc46BXdNV6xqrg4SbYXSUGeBAAGH0zhaHwcL9p5fIFihWlLreUuv9GkGLK5wWRYFC4MqtNSSqyDGWpDdcfI1lYBIFXJypmFAzY+xygCm+lA3abi20t2oSAlxUx4hs2fRHW1mqLZTXBtDr+VtIE+zGLl7T+D9Jkoti8yHjWAaSqSbBIE8NVmtOGUwu7buSvmzxA+7cy5kA/zylExdw9da2dL310KqadlC2iPjGyGurUembetLtJWsmpX4ZFQEt/GNtmrLWW8XyPuXVvzFb8Wz50iIuMW43jrZBfpTpIU3Ue5dt3CjpoDtMdxKombiAjt6aC7tS3umdLTVOfNH3eHE47sJw+tCaZ09q+nCV11GLoqWQLfQgA7pqJlLthBvylZQouQcCMqnXWGVwFpPSEtLxF7o7WitlYRBodGPdQaJGjZV45hHg8jCX8Prp79DtGVOpqZ+4ViUoQ0gzmoAzkZYQkf3e2yr0PaqW3IqaRx9ZGDVQhwbRPFJiqbUbAbFd8hbpCHYvixkgq+PDT6SDxis4FioRCXTZOCmKo68R+SQCHHyF2+oapKijdZqVMLCc+Yj3hxMc2Ip3TSNVRyBp2QBKgJkgQ5vDOA+tPYjGNG7GMZJ3iDKlfbq1y2V72btNC5zrNdY0JdK3HyCbQDKmKsdRy5aKKh7lMiXH11UVrdQiqTT0w/GJkrGQ9OwwW9MXLJbOqVOrXRaf0ogJC0zSspVmE0gDEjLjHr1zsWIeu/elSkyTWZttGzJXP4c3MtuQfVkhByZEv4abduFHbRV83QfNZjnsIJh7kRUMb8suipzT1bS2/zASecEhaspngdhw34HCLLrq9pV7X/AD9IUzdHp9xhIaSkjxHMuayB7pIkJHhDvP8ABLwn/wARP/gx3+TQL+oqX4qn/bVAL8lrf+V/WIzXXWbpEkJn3+DfPS0oLxd6s8ky2OWerO3fo5UVdjbDHXMPH1Ewj9R2+o6pHLNX/LMlbzHFy4R9Bh0G6cJrPP8A/svTOf8A6eP8YwIPcrrrhZDErmyYZ+Nyd6lBWHSj265RTMuzjEINwm4j2T2VXfTkqumLabdIJoEST8gC6E2+xRASHp20ulUtDwKDMyCsOEKPqx0yslht5qrZeqGuwJm04CFYE5gNoBwMu2K9uRYXNPXCmv8AJEFlE8VJSU3AOo1pFybZwtOpRzp4rGOGKqfkWfNY5yssLhstsIkW5gH8jAutzRRzacQThtlxjlqhWlDORahnClb+3+JQ2DFfYGn9xOuBYDJVqkKpkN07i1bdAJTZ4ifkJGEIJ2j1IxjpC4rUgY3k+oIk/mATfbQNc6tNbROLbSQlJl6ovbRX3WiurT1vyFmU1ZuM8PVBSdT5lnhlrZLVc7XAdgqBi+cKlQ+tWRGiN0lLFcFmxTISlBLIldrQb2MWOAplIdLzGDcNhANA5bcXRjKknxQ37rrP59SJtN+QlumRJYUnCagMBPhDro1KAyB09yjcmmFp2vGucTa71aKctK2Cxs4VlDkYTEWeRhp9dV4JXVhifblRJyIgmQBDiBgDV9Z0LS61mBBCh7YB/K6Xfr22Q6QVqABUrD6eyEjUjGs9kpoXIFqx3jk8q2/YwV1tKOmMdFSVPrVcYv144rzmsC7h4i7WjVhOfkoKJkzFAwaP7vJygCEGaoEanks3o0iVJ8sCcZ4bePdHN5k7HSFkaYXyzf8AA7WPxljWz3pCPlKykRs0SiZi1pNP0EkeVu2ZQpiw7JwyZpLj7hymCyxwIVAoDY6fV+kaMrqFpKiJyJkd/wBsQupdi0TeLWq20LihmSQZKniQOEYHykdosydi+vVQT6/Fn65Rm8bPQOXApticr22MbzzJhFr0y81WLN7poxeSknIFLKEKdo8ZqggCgByDUJy4M6hqnKt4hKFGYB/jvhfaPsFv0namqG3iZSD4t5monGCj+OfD2QOsLSD699ZaRUL/AJOqWInTnOMjkCUCCaK5csUpH2K6LJtnHEzoKQeXYQLcg+iiDMwgIAYdCd5FY8nl0yiUowwx7frjoDpLYtCXO8L/APYSj5Ra0mYOUBEgJd+B+nEwx41C7/ykS8jLVjTAMz78xje3mJKBfxyIqFETIpomJyQSSMf8oQNuT8B31Nt9RfqC3pNC4kOkyIVwl29sPq+dPv2PKr+W2K414E1EOHL2ykeMcO7wd3sVVB2hGYroD9NmmyO5qGQrE35IIJgg3L7Irl3EpCYgAIFKkBgH8dTKS917lSlF2SyWCCFHDgZYxizpX9p1MgIpam4ttD4XTMSxwgH+28VkayQt3oudZVtcsk0+oLI1ZkDdV+3iYqWaGWW8kyoRMi7yQXAOIjsbfYNtNrSiKi16Pqqm3TQtzcNpBjjvrZSdLNQ66Yt+nqmsXbWTgXFkiQxlPtlFffsn8TGbuu/WLG+cLSnGuH9syTXY63EavF1piqxd8XI3q7J+Q6hkQSbi5S5lKUBTOt/IGlzb7DVXK6hKlkkqM5d84NtRsaHPT6lrKOaalCgDM7BIDZFt7rn1Qw7Z/jlwPjC4VaoyriOVbKnk3pU1JiJkC+6kfO3m+JHcW58wkMchDAXgIlHf0HWu9JbbuFRbwJVNMvLPiZA/XCHtFotFxvArmXHltKWDMEkDYNsJm78UPJNI7CsbhaKYybvoGIrbal3OmLxsrMSbCuSaLyCdyKCZHanlbuWoJ7qJCoRMdjcgNo06e0Or7pY66qpUlykQooE0zmQJyHpENTVNLoYN0TD786zAHxiYGGBxw3wdX+M3H/8A0ddf/VJf7B0k/La+/wDz3P8AaP2QyP0v0/8A/OR/uCHbu/4mq81l7CgzPumKRyuvOImTARKkAlhCAmQTCO/oIm39fpq/Krl2wvRV2zdKI9vmPrrmCmWHG9ukoydrFuhnkDOxzqQkfCu0coLESFRNGMKAqtnByOCmEAKB0CjvuAa9pM3zFGfbMe2I1bWWoIlhOKUnbHr8yw5XY/FLusyNjsFGulrjGqky9USRiPO4Ic53KzlZErtTxiRECmAQSAAEB+7bRTq/CjTL4fqEDCXGnqouM/2sPZjC1WlKyxZaPYrbXYJ/D0PH800rdmtMcLgz6nPJ9VRb9I/UG7hNdcFVSgoI7mImUwb7fTQGwvmUvktufH6oJGPAxzk/3QZCHUfCglD1zLCNcybB3q5kyrZ2KNWuzZ2ecLW/0BqJDvZJFwCz1jEOVR2dPUdwRDYT/b66pVu/LnTSkyG2GYxpC8P6c/UFUlXkpyHDNFvHHHZeDzXcZiotX6dGmsUWytUktcjXkY5komLWaWgoS3u2YkQm4a0rsm5hTV5pAokcphAdbGbinnJAO0wtTT+dS87+SCfRAH5D6p42z0zmbp1rr8XUszV6w2NlbsKTDqdbVKbtLqQcITEvV4FaURgGNpB0czszIygtTLGE7cxTbhq8TXy+9vim2xzOGcHzdqiaNh7KlJgAqkdcMvSCice+mFpi5T9fsknVrBK3mtSpXMd+sSrxMqyq6Cq3kFYxRENib199uwr65CNu71CCJNutshs9EQpgnGOE8ZLZCiYb9mV3NMXkBrXaY0m216g7LJ0sj2sMZ6JSj5+LTrtiYCg3fKKIrlUTExxOmUDFKYZTacrYT2RWv29IdIaly90S71zxRLzPdLsSxn46WbN31qytKJvUBvMczkGq1prbpuszm4wrR4+QOVbYgCoJDEIIB9pQDW9NTyPDOU4yaarKdJTTzyk4y4wzL+AVKkU12DkJgqBWp2iZErJk8THMdXynA6n6yQ5RRUKHEwDyD6ayFQxUeGoM0DEd8bvN3VnxCZjLW6+RqajUzaJsTgTyTOQIuExf1m5ZNAiLVB4kCsycgiCJCgZI4CmPHkIb7jrYintlQtLRlJSh7Yzaut1SrNjKR39kVtUJaw5p785Z6/Vu42RRC65gLEzcFHODzD6Jq1aUIeacMhlFnLxsRIG+yheRQIBxEoBtrp6iNuodKJQiWblEDvlhCUvibnX3oqM8uce2Gx9m+oLi1YVyVW4inSMgVWDcTccwfu13or2yKZkLES+zuT8qLtsVqUURSTEQUABKXfSv0GlVDeuZc/dzHb3wxbtUeZtHys7SB7Izuh1hpfbLrTV8bZFk1o2NxenJnc2CIhnkq2lVwfJxa9dICR0XzmThDv1wWEwkVQVbiUwcSaldUdPt22sXfGB+FWLKwcNmWX/DC40hdLjZri/aGyQ2lWUeo/XECfKv0qvBapBZS6qZysydrqz+DibNjmdx2WErVgrDx24bDJSM7Is3KpXjbwFSIiUglVDcwmAwAOs+mOtq+zt/JQMtGtZc2zxUANg/0wU6g0255VV4qRNxRwPAfwYUR+0vkT/03G//AFf9jadXz5jiPX9kAnzZriYsjp/JusT2KjjoflFo5OcrkSjQL+6SWTQUDk4BA0WQCpGNuUm5TFOYDAH80dcinlT/AL6v6ocE35f2B/TGI++U6xScfKSrvo7ldk3jI107CaYY/tqcfHex3Mm5lnicezZMGpNw8plx4lKA7+oamUNuYU6HC4c08McYi1oeyT5In/pipp3uyLac2STp+i1M8W/U4xw+ee3Wfqv5i1vVTuYopGiAqshFPimU7kS8FChuO4ba81Q/lpU0gWVETxnPbI+rZH62J/CGdASuZwlLfDv/AI0cR46p3V1n1Ey/jZ1IQGZHc7N5YuB6bNpN2jSRK0AIOMtj1snHr2lFqqRcjkivFJMoiUuwaWtzu9XYEtKS2VocRPNKeXGUp7uMH1vsnzpsUzKimoQsL8JkSlO3Zujs+lk1Q5XuH3h6x4crUDHYcwxjRfE+C5tCEI8tLWLZpJp5BnSSbNX/AFkaRdLAC7tbcB4+glLvrfRtKr2xeHcULOWR/gwV6w1/VV1E3YqFxbdDTJAUhKiEFYEpkAyn34x2GPsN5E6MWC8SGCcPw3YCZtryvOhyNb24F9vUwTcTSTatPXDdSCRSiJGTWUM4arG3OcSicBEdFNNS0hRNSECQ2yGHbC5tlwpUUVSqrUQxkOcgyOXfjBB9kexXeCRv8dEdYOrdFuGL7xBVC6McimeVGtou5NzGtz3CMWmQk2LktqgrY1dJbmEVDmIBtxAd9bhTUEpFScO7+DDEstu6c3K1h2pqChyU55wN2+C3p+WuxtnwyeNmsd0DF2f0k3Slac2hzSLLBWuUVFNSQiJyzV92/FmNi2IVN4qPIFzfnAP1CvNOxmzZE5+MhAK1caSkuaUvBJZzYTA2fT2QvjKvcb5XsXSMTXbv1bqM7ASE+6ZyLansYG+HiHiMUD9OfnYyKbSBotociIJEdmSADmEAAfTUp8U9PTB5JBcliOGOyGvb7x09WE+dCBUy8QGUCfZHyPb/AOX6YbtH9ZxtUY6PWRfe0bz0myrDpFiCgFQat2rhqk5TKqO3ARKVMdtxDWi03BqtbWt9pGZK5CaRskIm3DUXTekWlDSAQUzwCftjlHub/nknm8pJ1yNwjEopKqNyQthyPDwL4iqZx8rxo/lEWzGQQKAbGOiYU/vD11OqHKQJEkI2/CIq3dYdOmwCtAlPeE/bEUZTu3zir1QJnIeZeq2OouKbKyDhR1mKGT/TUuIHI7euI9FZBymmYOIj5djD6AO+raidom5OpZaKxxSJ+yNB1t0+AJp2mi9IyCgmWyFH9IGuVMg5B7UZDwVdKfX+4szbG8RI5QyBJrxlFkahJhytjvH0sm1UK5fSsg3AE1QAP/KjvvqRWaorESbkrlAjw7vRCmVcbYu8rqw21yVEyEky9EHzZcV/MpOM3KI9j+rLZIpDlXBtkh9+rnbikAmVbvUmZ01RSKAm2IUTB+O311qpdUvNXpFW6mdONoIw+yIlc/SDxZED6BHTdCen/buOx33MxnA5LokPlJGmPJKAuiBkH0IlbrFJpSa8wtPo+4l0mSykw5Oqsihuqb0Nv9NNbqdre13jTVuabQ2F8rYAkS96FtR21+q1E5VszCA7PD6IXz2jgfmUw9cVqPb8zZszY2t0dHig/wAa0ebuGOVXCjwjdtGJWA0e2jhCMXEvuDqJJCkb1AAD10pqOqTSMpfT4VS2jA+mGrcbg3UMIo3JKSGwCDiJ93GOO/a/yq/7hXb/AKdd/ret/wCrHPzV+n/GBr5PRf8Ajs/0J+yHcMXVbSPEuf1dFp7VBwEQ+XkLu3dxTNUXZxjxZuZbxODqKODFHlyUIXYdw9NKzytefvKlDlrBamR4EAR75myviiG615tg6mxkFb24xtIPAXbIZAcsW7losggd+rMLJqoMjpFdGHcXiJTCPDfcQASSzprgsc9RywPX9+kbpwaQDNLvgQvis6PIHeRnaPN027eObW+fyFTq05FO14y0VVwxQFOQmGcnCSLciPvVBWbqKmBUVAEwHENWNzSl2oKFTygDfjsgKpXHHEc5cuYSd0thwwh6eQ6lhS2IxKEzTW0o3i0jNGjmCjZZj7ZosTxjGKuqsyYSXgEgbkTFUiWwAO2oFQ01XBNBUJCmyMOMu+L23aicslT55syqwgpThMZTtmNhM98Iw6s4dtNT+QDsQOM7lKUXDmEceXe4P3wVNd7arbX5KIefqcC9S9kSQdJtpB2BCpoEcLrlKAmKc3rohf0wbPYkOIP/AGZcEk4Tme2c4Hm7lT1r7zTYOZ1RWrbiezDAQ1u79n47FHS0Mo4oxPkXPlyp+OIiCiKW3wtd0JSftSrRhGJRc+K9ZZIxqKizhFwoiZEVFW4iZMTCGwCFXVOoQWEmSDgcIk07LTYUwR+CvBQOMxCX+incj5UmueC1rKPS3KDDr3fskrz1nhlcR3JM+J21jMVN8amS9rdkbRkAg+AXBGYImETHPwH146xpaZt0TUVekxapTRJZ5KEgN8BFoaMr7+ejuAtFYhs8AVTs/wBvskFEWLkUQbkkRSYkFtIAQvIwFEDkEgiIb/TfznNk8Ip3mW33Q65PMNmMo6mahpGMjzV22v4udVRTBnUb4dIXZK+7P402FfyCqVgLtGCcuzlArgvkOgQRHf01kKVtz8Rcyo9uHoiC4/VtPHlFMhsmkGIyJiLMKYyIupOkwMnYUnLJos0i4OTSbrGTeezlmL2UUbuHaIrimHiSbJgZuBlBER21NpmqZpBBG+eBjM3C4qIz8vD/ACCOpjMUz8NVkmbqYq1sn2cWo0TavK0VBB9IEKYxlHU4VlJqMmz5QClBFZH0BMxgN9NYVqWC2OWDOfGNDlRWOiSwgic/cEc9O4NiMoUSLquVsSVnIkdLOmStnqraWCKjyOUAHjH7Jowqrli2XTKch3BvGPIQEm+tLNY+wQWyMOInEdaFOpyOhOQ7ZAA/Qdojxxt1DxHjJxNN8ade4zHSsovwfewMk3cLIoD9iTd4pBySSTNIDCKYldkMYQ9fTbWblfUO+/l9Aj1LDCQEjNLvgkYPDkC7ZOmHsW7Z62USbIyL9pWX7yMTFIpxQFyeKauXSapRHkUDqh+AjrUat0oyYS7hFu5TtOiS5n6Y8aB1xrNUyJerhHWeSTfXuCbQE9CO2kSWonj2pwBBRrCJskUSOEjFAplBUEqgB6h6761Vbrla221UElDXujZKPKOmaoVKXTiSl7Z4xIEXgqpQEi6mI4au/nDJsW5VmkC2jDgzT/NWZFbMnJB/1wqQVVhNsKZihwD66mJeWuk5aj4U4DuiQmlacXzlA558YkP9Ir/9lMP6dx/XdU3Ib7fTE3liF2K9dOnqLUoSOaay8kURMCjhtlJq3YLqmKoCahmqkrw3BET7FH6B676J+ej8sTiJVXC4ue+w6kdqVfWICzu1jOmweD7sTrBM4ut1omq2yhLYa45crURXqhRwlIs83POTNX7qQmCrOATLyVEzchjfdscCbXdP5d5krVJChuO0+mUViquteEn2loT2hQ9oEMK6tVCuHwliJvN3zGxV4uiwLMxIxuQsQ/OyQUTQdQkm5UBo7jFWw7CfiYq4hzHkProarHXPMK8JlhuMS2AA0AnEYwV0FWMckUTVjp+HXKmvzMnETDNJkqqZMqbUp04xMgCCZg5FDkAiYfT09NamlZnUlQIc3R48lhQHNkDLeQPbCu8Hmysb5RO2DhpCUBDCw0OAgzOQsMSrdnftW+7mXSho5+eWYxjlwcQWWfpokNx9DaILo7c0WxJdDiqfOJDKoCffsiJRU1EipK2XEFyRmAQT6jDXRjKa5coKSb2DbrJDFCmiq/i0zlFJu4CMKoBuKjryJmOKXkMYTFAwFHbfQNUErVNQymLYtp3KE43ElD43Veokn3cOvLFKZYecmMbIih7pNUCAq3kUnotCuPH9oiKYiAFEBKPHUincWhMm0kxiGuC468UYZT3SfFgm4EinlOs/DyFQOH1MdECKimQd9wMPENZyjPlo+IRrjM6KRKZRVXq52SqSwSyqzgDNUjmTH3YvzisVMN0TABgV/mgACG2sw86kBKUEgcP5RgW2Z4qTPviEmlgksQzNfYtYqRyfiK1uytIKTqkRJ22WoT9yZUqC6qUekoq+qXA481UxN7YobjuXW0Fx1JUZoI3cfZEd0tocCUAEHfwiYZ9CsLKvveu60nFuW7gVjcEyKoODnVMuUU1ESgkgKZjeTc3MogIemoa1vBUlAlPcY3hDBTMkA94jyQLVCRaSUK6h1WiZESo/pbVoBTmA4GObzoJ8zK7+n3HHfWOdXwx4tphSSApI9EbRQ8aooYFSCQoEKbc4cTcgAPQPT1Lr3Ov4TEbyrU/7o9X2xzztKtpi1I4cg7OCwqNhVRFBXcxgP4ynKCRhSIcNgANxHXmdXwmJvLT8QjPTFITiRIyaX480E1DnAwmHhzFUi5jbD+Ajr9nV8Jj3lo+IR+2wzSZFSi4IciySygrg15kaiQqqaKR25GpCnSVExzG8pjfcA7beupbTi+SQUmU43IEkyBmJx97N/wD260/uj/n1H8MZ48fZH//Z"

/***/ }),

/***/ 182:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/gj01.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAHrUlEQVR4Xs1b7VXbSBS9Y/NvceJUsKSCdSpYUwHWNrByGoBUQFJBSAORqUCmgpAKAhUsVBATsf8Wvz1vNCNG8kh6QmODzsk5SaSR5t15H/d9WOGZrnGcHjwo+hPAakjq+2oRrZ5jK+o5PvpbnE4GCj+cb6+yJHrzHHvZKQB86mvgiAaYgjBzBSbCmVJ0NSR1sUtt2AkA+3E6VQqnAKaiUyYshsCn1SK6ET3f46GtApDbORKx4BVBWCv2ciC25h+2BsAoTmfIhR/3OCAo4OqBMP93EV31eU/d2q0AMIrT2Agfas+rIeHdNkwiOADGw3/bOHnCPFtEizZExnE6flA4A/C3+yxrwq8kete2vuv94ACM5ikLX3V2t1kSHUg3Z3zHP9XnSeHT/dfoo/Q9kueCAuCJ73YP37MkkkUAs2I0T8kjQHC+EBSA/ffpR0U63FWvUABgTXgX0iEGBWA0Ty8BML3tBYDxAz99KhzaDEIDwJv2hr0uJ/fqfXpChM81NnyRJVGJRUpsfSdhsMZu7beZzLTGcgWMCZg0CNXZnJoACq0BPsfV54B8a180AHzKr0NLXHnfiwagzgkGw+SlO8GuANwaZH6XIvRiAWASNFRIWhwYy3kOwjJbREtXaJM8sXcvUeANYAKnykGcoCf5ufP4gu9DQtyW0BgazDmDj08UEWVNOAxBiHoDYEgL8/Y7IsRK4RsBX1SeD/zBO1YKH359jTjBEV8VVnltFyqFheEIQfhAbwBMteebFXI0T69IYbm3xtl6gHiwxrLt1OtQYbNSA8z4XQ8KS6WwZCD5GwxulkS999/7BRYAECK2a2Zxa2AcOmsbzdOftiZgKfeLAsB6Z20SwMzN/ffj9LNSmBFheb+IPtSduF47QALChAjz+0XEUUVf+/P07D6JTvjvLwoAJ3Hx2mQ1sWk6tUoO4CU8zvuusyRqoswif9PbBMyJcEg7GhLe+AqY1mYBNG7aRAC279d18d5GnFB8IAwAeQE0hcIy+xpFPujZV7gq3WQGnFH6HKcBiBsqakiYPNW5ut8OAoDRAo7dTGJWRIgkwop01PEBCjjW/xTWFyXvDwaABuGxGnyeJVEs2YD0GY4ClmuEBDcoAEYTtA1nSfRWKlzbc7bWyATLRoK2NdL7wQEoPLnhBdKNND03mqfavLpUlaTfDQ6ACVPc0/uRJdGhdCMNTpHba0y1S2FRawUwkfQamvYQHABLWrTDCqAFo/dpyp1kIhxa23eaL8w9Sr6G/dAQmn6L+olbAcDRAhoS3ko3Uz0px6kWJMsJhWNfkmWc5ZVU+7YCgIkImhtwS2tAOOwKgnPKpZjvdp5creBvuh0lqb/YGgDaFEyjxIAQSYmL21l2Bal2nqoAmHWppgrCNtpWAeCNOPyeCRL3+7/UaYM+wQE+m+mRuzVh6hY9PJ2nG4BOsuSvC611xl8YUxIVT7cOgNaEfEKEQ5mt/V0Socj0FHAApQsotoF6MSScVDWmY9tdVDDZCQDWuRkBuO535AlNtwQsuV5Yx/SaWmYb7xPS5Z0CUN0kawYBqy61vYb+o/t6caq8FQDyk6IjKDUDYTwksAMUxWVXW5TC8ZpwScC5BamhA81L70A46UKOggFQEbrcvCQsskU0l7JCz5xB4dBG81TXHqrv4jxhj/CxK9C9AWDP/R9wrBSYkXFn2FcS18NOEj7wKk6PKR+RebyMPTsV6FIHWhryfAfwZAB0yAJOkQvO1zkRFnvAjeHuvu+t2MmtgS+u3VvtUVAnnsZKMV5To/53Q8JB15O3m3sSAPtxeqoUHmd1KpzfKYFJtb7+Ocebj+YpJ0WlWaM+p88f7QSAU7Utj7lWWBdXcIvqTT8IitOt4wAm17jR9wF0cYCdARjFaeKovCtaiXV1JCy1ELmn+2qe/vCYR1F5stpRpcdt+Is1oGiA+N+4mauXp8Hb9uG9b6vMdd92Tj8vyubXTZdqlByAFrWu1vtbxmUkgLin65s9dENj6b40E+xkAjUqWAjiAUD37ySS+p6xp1s7NGkKJD7t6NKMFWtA24l6AOg6LOH1KbYeWAGpCI2++12KpyIAGiZAazWgRyTgOYIZx/Xa5OeRGNl6YVWJRKmw2ARaHKD+eFUDWji7T+uviXDiZoI1UecuSyLNBGu0o5MjFGmAQJhiU1YywRoXhFIjpY5vmAWuc6wdzJS2zkMBsFF8MGbDnL5p1IVlujV9Pq3yJq/gNrh34tTlBk1+qa5RW1U9EQANqpa/T1h8qH6cPTz/n638tEUas94Nf7WOVkqIpAA0efROvwWoC4st88GlZSX6m/8sZ+PaHQABmh/GoVXzfB6MYjPYnCE0GtfknHcGgPRDbYSoUuoqHJ0hQgxOQaos0WmMTkKzFJmAPqE4nZHCVEH/4LE4FS50cF2vqPIq0tXePVK30j4AP+9GjSqoVUGtCVTMhqdOL0G43HprzHjrCQaYKtL5Of/heZ2mQWnvuDwRPnHsd8mWFbAIqXlZnfk+X0XE4ayTgBsuwnQB29VGsQa0qbC9r2f7TAjjej8NygUMfs4BjTs4l3akzpjBxBIdD6e4HhKmT63++GQIDoAUKN9zZsRuujFHnBdCp6HmgraqAX0AqFtrWt6XT1Xzpj39D+vWjH0iXhPLAAAAAElFTkSuQmCC"

/***/ }),

/***/ 183:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/xm01.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAGgElEQVR4Xu2aTVYbRxDH/9XiPZOVySKxsjKOnDX4BOAb4BMEnyB4H4OQszc+AcoJ7JzAygk8rGMMrCyShcUqznuoK6/6Q9MzGo1Go2EAy7OB99TT0/Wr6qrqqiYs+EMLLj9uPICTdnP1UvOaAq0zY5OI1wFaAaGrCPsP2v3TeZR4owCctFdWMFxe04RNMK9D/oqwec+cIK4VwMnz5oYWjTI2QbQOYDUl6xmBImb0dIOjn9r93vvdZo+ADWY8JUIbwH3zTkkQtQEYCSuCinZhBA6fCwL1GBxphd4SPkcP2oNBWvkeQKvTN2t//7y5PQ+IKwFw0v5ufagba7JfWdM6GVNOCstARECPtY4aSyoqupfTAPysZUHMDWDkpAibE4QFA39aU+ao0RhGD9r/RGUd1yQAZUHMBECc1KW+szHmkRPS0BGBI2aKVOOyN4+wWZCmAfDvHO82xT/sALib5yMmAhj3yMWcVFnNFn2vKACZT2TQelkgTAQxApB0UmbPpj1yISdVVJCy42YB4L+RB4KOd5tcdjG3/T3jmxyAC/HKaYEIkFBl9xDoiMFjYcm/I7HZ/Z85VzxOEhteKzI2XA9Zi7Qxf/RMXVO4/ldIrn9vBED+edjpj0KVMRlefgnGNoALC4Ejpf57nBWbj/eah26sWxrvtDrnr9JAJWJoza9dDuDmRbvV6e/nWdPx7r1fADrw0GKl4FSp4ZMsR+vC4mE8b3L9ovhMABLDtVaHdpF0pBRvDYeSg+MwC0IsPB1pxTtK441ZIKHb2u8/jfehmfetpLaSxUk41Hqpa61BFkdP0rmAUcTwziGItkQRrPU2lNoJMkFZ00Ap/TiEECjkQitsKS3w/HesEjMBJEnTq1bno3hP88REY5Kh8Er9uykTW4CNXgjBQR0J//BFv+u9NOvlAwZ+FkGY6Zn/7a92c1NZa1nxihBAoRMM1jSCEAqv1HBTwFgn+E0vhKD18qfAB5i4PXAZmyH98Le/RZOJJzCrUzBHVjNiJVb4pLYdBOY3/lAjmvcChhO7ecXE78KMJ/FHEsfF9yQV4c4CqVTYWAIYPW8tXvh4TUkIYuGhE/R7d8CgnQZj4hFTK2zH+31c+EwIRg50lYbRfNYzJKwS+CA4/WUqIisMpvb7RVr4bAiwGerCh0EhmueB07/F4Sg/BM0yZ8Y3TPgSDeXNQ8Y/mJB6xkhabd5v1iCxYc8oMz4ux97TCo/lfD7j64WGF832rKPEWwD7rU7f+Qz7ibzf5PdRFCi0omDQbQMgxRQiZCmqvSgWMFHHXzQAc3THcrrqlICxhOHgi/UBRbf2VwBFSflxt8UJFpXrqwUUJXUdFqCkX5DzmJ6CPSaP5QFF5brRFlBUiCsFYIsYcly1j+3PSScHXbhDEzPOsk55MwiQGOoON1IB2ot/oLGKjqTAALrSSCmblU61gCClzE0owopSWcHT7/lyXVZFyqey8363MAACfh+q8eOs5OLpklqVAMyBiNFNV6RqBzBpn1W1kCxo4dzpipSv6NRmAdcNQACFEHxFZ6EAuGOsHHuNc6xi6031Aa7I+S5dm5MFuI7LJwL98WPno1RuK33S2yssrtYGwFFnKV23OuePQgnf//r9Fin1ep44nEcsBJCuLItTrMUCZIEfdptdU7omPmjtnz+z2jflbxF+Vanho6q7wB68CNlQw52wpyA5R1XOd+oWsMJKMkRvXF1dyt+noxseTM9aLz76rk3lW8B+i6U3YBoqPuGqFUCw36VRIu2y+6IZVmiXzcCKkAor1umeQu0A/ILrOA4H3xp1rhcYAB25U8jatW6B67IA6wQ/b/nenodQyxY43v0hAvT9Vuf8Ww+gaM2+yB6fNmY8DNp+o7sjePVh0Ic/Tz1OinDW6vTTV2imyTPz79mJkGu61pEJulqAdGrlgsTANy6vsisUUsoyc++Ea8sEBQJrkh7+KgGnpC7bV5H0TDsNmsRo795LMEkoNrdLassEZ7bdil4ILSB98UHrxruFAUCEE39fyff+a4kCFSmy9DSpuwuJiw8LByDteBcOQPqG2sIAEEengFN3k2x0V3GhAEjdb1STcBc2ayuKlvZgFbyY1nIIobaiaAVylJ4iy8xjCDUVRUuvfoYXw/R2htdyhxZNkgqVxKpa1KR5YgDVXL3zV+CK9AxuFICqDlmzRIivAK7avIvMX3Wd8dZagGR7cmu9CLS8MXJ/4ZY6wXlFj98vCuB/W4rAHwg0Z/IAAAAASUVORK5CYII="

/***/ }),

/***/ 184:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/cl01.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAHW0lEQVR4Xt2bwVLcRhCGu7dSi2+xnyD4CRyOFgfgCYyPFgfgZjapCjxByBMYqhKRm8nB8jGbJ/D6YHHM+gmCn8Bw85KKOtVaSYxaM5oZSYvXVhUnpFnNN9093X+PEBZ4vTx8c3/473ADCLYRcBMArmbD2db+ydbVAn/Wa2j0utvh5tfPL1bTAT1Bgm1A4EnLa7pMEHoB8Gr07nsA+Clf5VUHTksDoTMAXnEa0D8Ok5a3XBHQ1k60Pm3xbG+PdAYQHyTngLBrfCOi94A4AYRLIHgh7vvsELoDGCUfAeB+fWJ4dDP8NFYD3qvRxR4CvVwmCJ0AvP7xYpNSeqNbfQLc34ken8v/cbxAwAkAfKv877NZQicA8Sg54eCXT+RaTAqA4CQ8C46WGUJXABz85lEf4YgIr6SJE9D5TrS+v6wQWgPITfnvYmKY4sNnvz++1Pn5MkNoDaBi/kTvw7N1zgWyqx8IeKSLIb3tf/lAXQBUzD/8LeB4UF4tIIwB4Dt1DFMg7RNCKwAm86/7eX3bM7kD1w0rs+EEEB/dJYRWAJrM/0uD0BZAo/l/SRC8AUjzB4Kn4VnA/tt4+cSEu3QHbwCa7M85i1tGCN4AeJnjUcKp7EabVNYXwr2blXMCeFINjHC4EwWnNqtz+X8rAAYTXYglZMA1FadpN3GZtHpPKwA8wNcCwQkAix4wgF0C2lN1va8BQiOA+OBiF5C2AYD/youAftmJ1o8/iyXU4w90yRi1AOKD5AUg8GpLoSODoAK4CwjCAnWa4zSMgjVf/+f7awBejd4dI+DPxsGI3s9WbjaltL0IdwAA1gt5EaxCKwGttdEXawDiUXKb5SkUEOAvIjhvSnoWBMFtYQn+CM8CtlqvqwKgluXlQ/n42MIhsMg6wHP8D8fpIN1TrPUqjIIHXrOXLiAkLnUs5z2+j5igSbQ+AMAYUzxh0aV4MSnJ+yxUMUbFAoT5S43vziCo7yEDrlzhCiyCSXgWbPlYQQlAmj8HFSQ8FJr/5Ww4W3Pt7bVxh/qqNgc3mVoX0pwrhBJA/ENyqDQuPoRRsGqYgFdbyxeCmNB1GAXarVidYDxKuNlayOynYRQc+gMYJSxwFrpeOYgJgu++q8nntS6lxiHeeZ5FQSUJ001MjH0ZRsFDLwC1/p6o8TMINyscfMpmRptixAVCrCyEzf+LSbbVKMpESJi/1ux0HZ2+IeSgudWWXTjArWe/PubSu/HKF5A7VPOEySMnyGKASr3pYW1bC+FIKsK2FzZZAhKuAsKfxfNhFGhT9WzCCBuE6aapJW96Vr4b2sxfPqAXNPR9wCYQOggAwKtd+PzbMAqyAxbz8we4gZAduOA4ZU2NnQG4mL8LBFdzrUTvptY6AbfUObrzpK07gXhH550AK+YPUFK3mnG1Mcq3eyVKxfjW8wW2F8l8Pj+DQDCZrcwmrnlKFmfiUULKb3hNQmfGmOKamq66vH8LCG8JaDIYDCafvvk09ZlwLQZo8m6vROf1KBkL0dLreQdLuEaACSFMEHFq2xXmecvKI0TcJKJNwCxeGE+noW6P5zrc9SRXH9liCWHuVnvFhIloYqvxi6N4mCJPtgiSNcMz5RTZNmM4teGsshggjMMoeOriAm3vMRy5MQ3HVvBQuosshipHV3wSnXw7ZQWnU7boA8Mk3pjG0JXLOkGkNQRDtlgKqE2Ty54l3GWfnQ1n+y6BTQTw2+GVXQGAjpWOc61O0GiC2pNczvuqTlUyCRUmsdOlCNK06E5xgGO5K9TKZZFea1PNrtme1jfzAmseL+49Ae4x6I/SZis5G84eNFmB+I3G/CUeJVzIzQ9fCNHE2BdYAATO6ti9nDI7m7wl1OtGCxXZLqiiSXNjpJ7teTUhGjTGejhgv81Ss/yEiKWiq+QvloKsttUrY1tbYy41vGfRo95eETuF1TWqvOoO4FKHyMUoXMwKgN+2M4Rqup3V66zyyh6DT2Wq7gC2eMFz0GiN2e7kBCCD0PJMgK/IGR+8myrblta35Q7gWvqKhcwszBlA25Q3Pkj4w4lC5LCKnCJgafU9nx1A9TcJjgOtMwAeqA0EEa2t5baLxahj2nIGIabIHWjqBaCEIATSpuJJdR1XkbOybwPU3KBpTBe5TLEKrjT9L0PxpC1+YvV7AscTZSJi14qyaucI9wHTj3k1yHKaVS5TZnzaCoCpgpTFkzRn166N7SSqsQZwW8tMTOFdiEvt1gAUCOWJ8XmmeXs8XgQdawBU37/iBkqiY+pgG+euFEa61n4nAHMI5vPAvgGwgJorwNzeKsy5DJ5NX6nkED5wvs8r7KIPdgZghoD7A6DtQi4zBUBLlC4XttjrNQpWJpmlgONBChNfPbIXACYIrMUVknZR3LhOWJh0xX3mH2dyUZVObZKZLSz0BsCQMqu/z2oRm7Svxn9NgIeL+niiVwAOEGwLwv8vVWAXUdRlwKZ7egfQAkIWtIg/ruzBpH2BLATAHEKlqFHfq5xwm6DlO0Hb/QsDkEdr/o5gtVjhZZiwBPI/cD9Loo+Q6tEAAAAASUVORK5CYII="

/***/ }),

/***/ 185:
/*!*************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/jx01.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAHbUlEQVR4Xu1ae6wU5RX/ndkLGl8x0aQB0/jgD3wQoynq3W+Fnb1EwQcmmEgAtaIYoA9t2irinVlZ3dkbn4nxGbX1lahR06S+ihBgZi/u7EUx6UNK01KlsbGJVRPFRwv3zjFzL7t3HzM733y7i2zw+3Pm/H7nnN+c7zkf4RBvdIjnj+8F6PUKmDeSO31zf+6vqnn0dAX4yY95oy8ltL7FqiL0rACV5AGcAWCHqgg9KUBD8pXqVxKh5wQISV5ZhJ4SICJ5JRF6RoDQ5Ak5MHINs4B0d+gJAVol7ySt2zOuOcTArSoiHPQCRCVfSTpdzt5HzL+KK0IsAQZK5owxsCBwwtO0bcNJa6fqAkQGJ5t8hUt3zYcA/CyOCFICZErGGia6GsCsenLeDWivOyJ/g0xCsjZp1/gtga4LtCfk/LIP49JLxhMgur7uPdFrTjJ/WRAmUgDdNd8CkGoZPOPDRKJvgepqrJZ7/1ffoZJ8BZNxzWcZ8D/Y/sa7HVE4ObYAumt8ANBJsl/uMPQdt0HkPpO1D7LTt5o6ErCb3kV8+Ub7tGu+SMDi8eeE+5ykdVMsATLl7BpmvqsWxMBnxFgPjfeBoQeI83tHWItqMeMJxWkJ+P3YX95OtpjJV4ADrnGCR2NTnOSdu8NCCOwCGfe2+QzvzXoQrQcSNzoit6taaiXzASbU9/+GYHXX/A2AFXE06ETysv4CBUiXjAIRDVZICPSyLfIT5dTQ0q55MwF3h9nqpcHlIO0p2YDaLf24fgIF0EvmMAhzqmTsXeukhp4OIs+MZH/EHm+vebfDEVZ1tkiXsucQ8dtxA6vaK5a/rL9gAVyTawkcYbWcLfQW9hduuOnIvUcf/qVsQAdHBbimPwpXBy8m7+Jicmh9YAW8Zc5kDX8Lq4C5ZfM0jaF8YoPvpAJKpgWCMdkF8JyTsq4KFKBhtmgcL3pyDMiUzcuZ8bvahAneAlsMbah9ppfXngTu+6Bh1L7LSVprqzNF2XyEGT/pqS7gB6uXsxvBfEFD4JsY/D4Yo0Q4EaBLGpL/BNR3ttOf+3fluV4218VKvnlruzOheUs29w/9ORaPpHHo4HbRSO6Yb7zRzyV5xs0YvLgoCi/HwTTapl3jCgK9VP+cdpGGJXZ//l1Z7kxp8BLWEqvAfDTGcLszx3KCsK1H99LgWSDNHxCPjXTcYqqMxNYYhC6FgX8x09JiKl+O4gsQ8V1HWLNjC1Dtx0Ervipb8woxKsBW73U7dzwOG/1vcLD0EbG3dEuqMBzGEVxBaE8A39nct40f0l4+lTSayUQEeLuIp/6jdmncTuK12MzW7Hnch7OZvU+bugPjE2hY6iStTXLdZ3wzFLqFDtkLZJfZIv+8SkJ+CVOCpqviJZP63POwbPh86w8V+5AvDyIy7GR+KCyX4DGAc1raHV2fABdalVst6Zztg9MS+7SVEweU9IYj8peqCBiECUqOga81TVtm99/xSljyDKwpCuueVnGEDoK6azwK0GoC7mH2nndSQ38MIpozPHhmYoq2iJhWMnj65NAwenKrbWhccYKT5H0MXNk8a/js9EtH5O+P8hMqwEDJmOsRFSsEBHzBwE4G/4mI9hBjOgPTapfMtc6I2LCThdDSiwpMthJCeH7uCOthGR8R06D5HqjhcEKGdcKmblcoD2ttGVbuNajVjrAek/UXJYABgiVL1mhHIN0W+WoVqfJIDowg0Apb5J+M46elAAPbzRneXlRPgOIQ+7YMerwo8qvi4mTsGyuBQNfYIv+sDLauq0YBdNd4E6D5UXbB7/krRxSOUsNGoyoiMHhZURReiEY0W0Qfi5eNa8AUeBok5ZB4uZMsPCNlq2Dki9DO/iNSgIXbc0fs2Tf6MRhHKsQHBjYWhaVYQSoe42EiBfDp2j3Z3ct9P3BTuY/jhXZgrKUESI8MXkCetlE1JAKvs0XhDlV8N3FSAuyvAv/cb6ZKMMz892KqoIRV8RcHIy+Af7LTfFoj7UsDn7dFFNSPx6U9xTOUF6A0OAuk/SUe/aQ1EZ6yk1bwH19V0g7gpAWY6AbZTQDPU/T7f0dYhytiuwaLJUBmJLuSPZZeZzdF7fES5/zCi13LRoE4lgDztt16nDeW+IiBqQq+AGbbSRUGlLBdAsUSwI8h7ZrPEPBj1Xim7vnfURvn3/uVKr7TuPgClG9bSOy9qhwIYa2TtOruHShzdQAYW4CJKjD+SaBTVPwz6P2iyM9QwXYDoySAXjLvBOEW1YCoj2bZ5+aD7wGpkiri1ATYZszGGL2j6NOHPeYIa3Ub+I5BlQTYvyYYBnjyEkWMkBjwisJKxIB0zbQNAcxfAIg8dW0R+UJHWK93LTNJYmUBxm9gAR8C/l8ilUYbHJFfoILsJEYx+IkQ6u7iKUQVdfVGgTI2JPiOkH/xAYl1YFoem7GDAGI8aKesGztI2UQVKEC6ZDxMRD/tpmNZ7k7cOWjlK6QC2tv7yyYnadfVwTJYANf0f2z+Ouy3l2Tg7ZsRvTY2ZWzV1tlD/2mfLJihrUGwW0EdSN7vBTiQah+Mvg75CvgWdS7nX92KLXYAAAAASUVORK5CYII="

/***/ }),

/***/ 2:
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2020 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
  Dep.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
  Dep.target = Dep.SharedObject.target;
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i++, i)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu'){//百度 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    if (currentValue != pre[key]) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);

  // vue-composition-api
  var compositionApiState = vm.__composition_api_state__ || vm.__secret_vfa_state__;
  var rawBindings = compositionApiState && compositionApiState.rawBindings;
  if (rawBindings) {
    Object.keys(rawBindings).forEach(function (key) {
      ret[key] = vm[key];
    });
  }

  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"VUE_APP_PLATFORM":"mp-weixin","NODE_ENV":"development","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      this.$scope['triggerEvent'](event, {
        __args__: toArray(arguments, 1)
      });
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    } else if (typeof val === 'number') {
      ret = new Array(val);
      for (i = 0, l = val; i < l; i++) {
        // 第一个参数暂时仍和小程序一致
        ret[i] = iteratee(i, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onPageNotFound',
    'onThemeChange',
    'onError',
    'onUnhandledRejection',
    //Page
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onAddToFavorites',
    'onShareTimeline',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 3)))

/***/ }),

/***/ 3:
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 4:
/*!*************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/pages.json ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ 42:
/*!****************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/xl-list.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDM4MkRDMTMyQUIxMTFFQUI4OUI4MkM2OEY4QjBDOTciIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDM4MkRDMTQyQUIxMTFFQUI4OUI4MkM2OEY4QjBDOTciPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEMzgyREMxMTJBQjExMUVBQjg5QjgyQzY4RjhCMEM5NyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpEMzgyREMxMjJBQjExMUVBQjg5QjgyQzY4RjhCMEM5NyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PmPG8ZQAAAHaSURBVHjaYvz//z/DQAImhgEGow4YdQBLWlqaGiMjYxeQ7UJPi4G5bwvQ3loWINEK5PvT2+dAe8OBmBkUBY4DFfzAUHBiAhLXBzAJPACFQAcQ/xgAy38Ao6AJFAJbgZzyAXBACdDujbBsOAmIW+iYAGuB1FRwOQAEMPEGIF5OB8tnMDMzt7GwsDCAMHJB9BcomQekT9Mw1R8A4jKgp/8B7QI5BqMkfAMUjAXStMgZZ4AYZPZnQkXxTSCOAjrkBRUtvwPEkUD8BKMuADVIsOALQAxy7TcqWP4W6vM7SFEBxyy/f//GpXEPMLHkAvFcChNdFtCiE1A2WAzZTkK14TwgbifT7n+gvA60dBWl1XE90AeLyHAAqIbtJVgdf/36FackMPhB+DfQARlAn4ixs7N7sLKyMuBryILKFaCaFUBmNcyMv3//4o6i/fv344s/sIEgC4Fs4UOHDu28c+eOMRsbG3bfAAuWFy9enLp+/bo7kPsBZgY+B7M4ODgQnZpv3rwZDDT8INAB8jh8f+fDhw+BQDUfaNIk+/Pnz0Ogj0D5+TWW0HoOpEKBjnhG6zbhcSBOA7kHSew3VOwCvRqlG4A4GohvgAoYYBwngtp4ZJUTo12zUQcMtAMAAgwAwHa41PL0cgEAAAAASUVORK5CYII="

/***/ }),

/***/ 43:
/*!**********************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/xl-list-hover.png ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QjhDMTdFOTMyQUIxMTFFQTg4Q0JFRDMwNkIwRUI1REEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QjhDMTdFOTQyQUIxMTFFQTg4Q0JFRDMwNkIwRUI1REEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCOEMxN0U5MTJBQjExMUVBODhDQkVEMzA2QjBFQjVEQSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCOEMxN0U5MjJBQjExMUVBODhDQkVEMzA2QjBFQjVEQSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlptOwEAAAIZSURBVHja7Fc9axRRFL33vtkBt/MjaU0lpM4fUBFiJ0sIQSWFzSKBWAUtJChiNATSBIQ0SZEmwUpBC0HQrRS1SBU3MMUGxA80YUVcIpt5N+dld0NkP9nN7DT74PKGmWHPuXfOPfctqyrFuYRiXj0CPQLeYCp9TkKaI6ZLXcZ+wczTnijNAPxKDMmPkaoRUroQV/nhQBcF2X+OUQI5YdVZXOzGAL6rah+4LniJb3Gn2+hMPIXteaUNF5T0YdfAmaaxPTnwAbXgoJCClfuYS6uRC4950UrikRWP1Pj/GVEoIrfA72N06PqWLN1WEgsigJIqJ/yF+oxjj6IzPimT++0/zax4U4mvYf9+jKkHaPqruPhSNQuYAVcJOox1hGNbOAb0bWQ5jgiknLFAbJXwEqZYq0fceh2qmQxVljpwOkqoTPik77WkwLLaii1OQ6VlCOdxm/iuv6ZY+al2OI7vIVbaIDAHs5lvOo6N/7dBDsaVrQiB3MR36bd7/mW1iXJx65iMwFM8bw3gd92gMaEhD/O+7vunB940aNuSJCuvnhzIvEqeCoZs6Ncup/GosPPjw05uYxgE8lyy3ANh163Adu58y2o+0ZcdSZ7JZij0z9bJPvhX+J3Kb2XzkRzJWMItVMT1888aj79Z1VGQ+Br1mfAdIo3YO3KviGGWRsnXu3UofQaFXMeeRQQAvuHOeG1Nxt5fsx6BuAnsCzAApwSxt8iX3tUAAAAASUVORK5CYII="

/***/ }),

/***/ 44:
/*!****************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/sj-list.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACAklEQVRYR+2XP4gTQRTGvzcQ7EQEEeQK/xSCpdqIjTYKisIVFoJ/ArfzpoqcImIheBYighY2HnmzwaCFciCIoIII2mtlIxyCWtmJlZBA5slKNqxrchvkJtVOt4+Z9/32m92dbwlTDGbeC+AqgD0AHojI3XHLnHNnQggLRPS43++vdLvdn1XtqWqCtfYoEaUA5vK5qrrbe79aXNtsNjc1Go1VItoyrM+LyLOq/msCWGvPEpEHsKHYKIRwOE3Td8VakiSHjDFvC7UlEbnx3wDOuUuqOtbq6ADMfGu452NvICqAcy5V1YWS8g8Am/NaFIBWq7Wx1+s9AnCyJP4ewAsAS9EAmHkXgEz8QEn8iYicZubrsQGeAzhREr8pItey2iwAvgDYXnjPF7339/LrWQD8sVhVPwK4473PtmM0ogNUfSxqgNqB2oHagdqB9XLAObcfwKKqHgTwVEQuV4bS9ToNrbXzRLQMYGv+6TfG7JgJADNfADA6XYcAX0UkPgAz3wZwpXTgfReRbVktqgOqupOIzhXFVfWl9/54XosJ8BrAkUkpaxYAf2mHEM6nafqwnD1iOjDSUtVj3vtX44JPbIBP2TPQbrc/TEpd0wKcArCSNxGRf9YNo/3ngtCbwWCQdDqdb2tFvqkAkiSZM8bcB7APgEz66WTmdpbiszkALorIr6q8+Rum/eyI0SLS4wAAAABJRU5ErkJggg=="

/***/ }),

/***/ 45:
/*!**********************************************************!*\
  !*** D:/gwj2/gwj/gwj3.0/static/images/sj-list-hover.png ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACLUlEQVRYR+2XT2jUQBjF35e1FS8iggjSg/9ANy0i6kW86EVRd7Ot4EHQKjSJJ6WKiAfBehAR9OBFaRJR7EEpqDXxD4igdxVEbFKKoJ68iSdRaPNkq1l3426zSKenzC2Pme/75c2QeRG0MYole5NoPANAB3gz9L0rzZbphnmQlAEB73QWpkbfjt36llVesiZ0V6xdJDwAXcncuIB1Ew/cyfq1G3uPLPk53TEpgmVVnZS+KHDGsurPClAsW4dE4AJYWF8ojrlj4pH3sl5bXzK3a5q8+KtxKPS98/8NoBvWSQBNrVYOoJfti5CZPW86lAJ0G5ZHYKChs+AriKW1M6BiC9buPra4s+PHCAAj9dqvAD4GZEgZQE+vvSYmRkBurW8uwN1x3z2gG+Y5pQB6xfZBlhuakxfGA+9sVVMPYFgfAaxMAAgMRr57NXmeB4DfFhN4B+JyFLjVs1AbygGyPhY5QO5A7kDuQO7AXDnQYxzdEiMeBLANkHuh75zKDKVzdRsWK1afENcBLK/lCq2wal4A9JJ5HJrUbtc/AJ9C31UPUDSsSwKcTl14X0LfXVHVlDpAyGoB+huaE0/CwN2baMoAKHgmxM5WKUs5QDpjUHg4eujdTuvKHEjZvicM3KfNgo9aACLSROt/7w+/bpW62gSw9wMcTYqEvvvPuploH/NDXaPnXBCb0f0bn2eLfG0BbNhndk1Na9dAbgbotPrp1A17GKANiLOIOPEmcL5n5c1fJgUSP2oqm2YAAAAASUVORK5CYII="

/***/ })

}]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map