//发起网络请求
const header = {
	'custom-header':'application/x-www-form-urlencoded',
	'openid':'',
	'sessionid':''
}
const request = (type,url,data) => {
	uni.showLoading({
	    title: '加载中'
	});
	var value = wx.getStorageSync('sessionid');
	if (value) {
		header.openid = value.openid
		header.sessionid = value.sessionid
	}
	let promise = new Promise((resolve, reject)=>{
		uni.request({
			url:'https://gwj.maohemao.com/' + url,
			method:type,
			data:data,
			header:header,
			success: (res) => {
				if (res.statusCode == 200) {
					resolve(res);
				}else {
					reject(res.data);
				}
			},
			fail: (res) => {
				uni.showToast({
					title: '网络出错',
					icon: 'none',
					duration: 1500
				})
				reject(res.data);
			},
			complete: (res) => {
				uni.hideLoading();
			}
		})
	})
	return promise
}

//POST接口调用
const gwj_post = (url,data) => {
	return request('POST',url,data)
}
//GET接口调用
const gwj_get = (url,data) => {
	return request('GET',url,data)
}

//小程序登录
const login = (url) => {
	uni.login({
		provider: 'weixin',
		success: (res) => {
			let login_post = gwj_post(url,res.authResult)
			wx.setStorageSync('sessionid', login_post.data);
		},
		fail: (res) => {
			console.log(res);
		}
	})
}
module.exports = {
	gwj_post: gwj_post,
	gwj_get:gwj_get,
	login:login
}